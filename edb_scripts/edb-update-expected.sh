#!/bin/sh

#for v16 and onwards
awk '$1 == "not"  { print $5 }' src/test/regress/regression.out |
while read test; do
	echo cp results/$test.out expected/$test.out
	cp `pwd`/src/test/regress/{results,expected}/$test.out
done

# for pre-v16 branches.
awk '$4 == "FAILED" { print $2 } $3 == "FAILED" { print $1 }' src/test/regress/regression.out |
while read test; do
	echo cp results/$test.out expected/$test.out
	cp `pwd`/src/test/regress/{results,expected}/$test.out
done
