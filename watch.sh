#!/bin/bash - 
#===============================================================================
#
#          FILE: watch.sh
# 
#         USAGE: ./watch.sh 
# 
#   DESCRIPTION:  
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 09/13/2016 23:46
#===============================================================================
RESULT="/home/amul/Public/pg_data/RM38238/rm38238_watchfile"
#================================================
echo '================================' > $RESULT
while [ ! -z `pgrep -f edb-postgres` ]
 do
     free -h 2>&1 >> $RESULT 
     sleep 1
 done
