#!/bin/sh
################################################################
# pgxc_stop.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

echo "****************************"
echo "*** Stop *******************"
echo "****************************"

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

### PARAMETER CHECK ###
stop_code=0
if [ $# -eq 0 ];then
  stop_code=1
elif [ $# -eq 1 ];then
  stop_code=2
  stop_node=$1
fi
if [ $stop_code -eq 0 ];then
  echo "Usage : ${0##*/} [ name ]"
  exit 1
fi
if [ $stop_code -eq 2 ];then
  no=-1
  cnt=0
  while [ $cnt -lt $allcnt ]
  do
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    if [ $name = $stop_node ];then
      if [ $cnt -le $ge ];then
        no=0
      elif [ $cnt -le $pe ];then
        no=1
      elif [ $cnt -le $ce ];then
        no=2
      else
        no=3
      fi
      stop_cnt=$cnt
      break
    fi
    cnt=`expr $cnt + 1`
  done
  if [ $no -eq -1 ];then
    echo "### ERROR : INVALID NAME"
    exit 1
  fi
fi

### COMPONENT NAME SPECIFIED ###
if [ $stop_code -eq 2 ];then
  x_ctl=("gtm_ctl" "gtm_ctl" "pg_ctl" "pg_ctl")
  component=("gtm" "gtm_proxy" "coordinator" "datanode")
  slan=`echo ${all[$stop_cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$stop_cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan(${component[$no]})  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "${x_ctl[$no]} stop -Z ${component[$no]} -D $newdata" > /dev/null || { echo "### ERROR : STOP (${component[$no]})"; }
  if [ $no -eq 0 ];then
    ssh $slan "rm $data/register.node" || { echo "### ERROR : RM REGISTER NODE"; }
  fi
  exit 0
fi

### Coordinator ###
echo "***** Coordinator **********"
cnt=$cs
while [ $cnt -le $ce ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "pg_ctl stop -D $newdata" > /dev/null || { echo "### ERROR : STOP (Coordinator)"; }
  cnt=`expr $cnt + 1`
done

### Datanode ###
echo "***** Datanode *************"
cnt=$ds
while [ $cnt -le $de ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "pg_ctl stop -D $newdata" > /dev/null || { echo "### ERROR : STOP (Datanode)"; }
  cnt=`expr $cnt + 1`
done

### GTM_proxy ###
echo "***** GTM_proxy ************"
cnt=$ps
while [ $cnt -le $pe ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "gtm_ctl stop -Z gtm_proxy -D $newdata" > /dev/null || { echo "### ERROR : STOP (GTM_proxy)"; }
  cnt=`expr $cnt + 1`
done

### GTM ###
echo "***** GTM ******************"
cnt=$ge
while [ $cnt -ge $gs ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "gtm_ctl stop -Z gtm -D $newdata" > /dev/null || { echo "### ERROR : STOP (GTM)"; }
  ssh $slan "rm $data/register.node" || { echo "### ERROR : RM REGISTER NODE"; }
  cnt=`expr $cnt - 1`
done

exit 0
