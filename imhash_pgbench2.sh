#!/bin/sh
#===============================================================================
#
#          FILE: imhash_pgbench.sh
#
#         USAGE: ./imhash_pgbench.sh
#
#   DESCRIPTION: Compare TPS & generated WAL size of btree with imhash index
#
#        AUTHOR: Amul Sul (sulamul@yahoo.com)
#       CREATED: 27/08/2016 15:01
#===============================================================================

#===============================================================================
#   CONFIGURATION
#===============================================================================
RESULT="/home/amul/Public/pg_data/RM38238/result.out"
NUM_OF_RUNS=1
DB="postgres"

# Small run
PGBENCH_INIT_OPT=" -i -s 300"
PGBENCH_TEST_OPT=" -M prepared -n -N -T 300 -c 16 -j 16"

#PGBENCH_INIT_OPT=" -i -s 300"
#PGBENCH_TEST_OPT=" -M prepared -n -N -t 10000 -c 64 -j 64"

BIN="/tmp/RM38238/pg_inst/bin"
DATA="/home/amul/Public/pg_data/RM38238/rm38238-data"
INITDB_OPT=""

LOG="/home/amul/Public/pg_data/RM38238/db.log"
LOG2="/home/amul/Public/pg_data/RM38238/imhash_pgbench.log"

#===============================================================================
#   FUNCTIONS
#===============================================================================
function change_pgconfig()
{
cat >> $DATA/postgresql.conf <<EOF
enable_bitmapscan = off
enable_seqscan = off
#max_connections = 200
#shared_buffers = 8GB
#maintenance_work_mem = 4GB
synchronous_commit = off
checkpoint_timeout = 15min
checkpoint_completion_target = 0.9
#log_line_prefix = '%t [%p] '
max_wal_size = 5GB
EOF
}

# Print output description
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}

function init_test_db()
{
	$BIN/pg_ctl -D $DATA stop -m i
	rm -rf $DATA

	#Drop server cached
	#drop_caches 3

	$BIN/initdb $INITDB_OPT -D $DATA 2>&1 > $LOG2
	change_pgconfig
	$BIN/pg_ctl -c -D $DATA start -w -l $LOG 2>&1 >> $LOG2
	$BIN/pgbench $PGBENCH_INIT_OPT -d $DB 2>&1 >> $LOG2
	#we are going to create second index on abalance
	$BIN/psql -e $DB -c 'alter table pgbench_accounts drop constraint pgbench_accounts_pkey;'
	$BIN/psql -d $DB -c 'UPDATE pgbench_accounts SET abalance = aid;' 2>&1 >> $LOG2
}

function reset_db()
{

	$BIN/psql -d $DB -c VACUUM 2>&1 >> $LOG2
	$BIN/psql -d $DB -c CHECKPOINT 2>&1 >> $LOG2
	$BIN/pg_ctl -D $DATA stop 2>&1 >> $LOG2

	#Drop server cached
	#drop_caches 3

	$BIN/pg_ctl -c -D $DATA start -w -l $LOG 2>&1 >> $LOG2
}

function init_test_imhash(){

	$BIN/psql -e $DB -c 'DROP INDEX IF EXISTS pgbench_accounts_abalance_indx;'
	$BIN/psql -e $DB -c 'DROP INDEX IF EXISTS pgbench_accounts_pkey;'
	echo "--------------------------------"
	echo " Reset DB"
	echo "--------------------------------"
	reset_db

	echo "--------------------------------"
	echo " create imhash extension"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'CREATE EXTENSION IF NOT EXISTS imhash'

	#start script to note down free -h output
	sh /home/amul/work/source/scripts/watch.sh &
	echo "--------------------------------"
	echo " drop & recreate PK"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'CREATE INDEX pgbench_accounts_pkey ON pgbench_accounts USING imhash(aid);'
	$BIN/psql -e $DB -c 'CREATE INDEX pgbench_accounts_abalance_indx ON pgbench_accounts USING imhash(abalance);'
}

function init_test_btree(){

	$BIN/psql -e $DB -c 'DROP INDEX IF EXISTS pgbench_accounts_abalance_indx;'
	$BIN/psql -e $DB -c 'DROP INDEX IF EXISTS pgbench_accounts_pkey;'
	echo "--------------------------------"
	echo " Reset DB"
	echo "--------------------------------"
	reset_db

	echo "--------------------------------"
	echo " drop & recreate PK"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'CREATE INDEX pgbench_accounts_pkey ON pgbench_accounts USING btree(aid);'
	$BIN/psql -e $DB -c 'CREATE INDEX pgbench_accounts_abalance_indx ON pgbench_accounts USING btree(abalance);'
}
function run_pgbench()
{
	echo ""
	printf "WAL lsn before test:  "
	$BIN/psql -t  $DB -c "SELECT pg_current_xlog_location()"

	$BIN/pgbench $DB $PGBENCH_TEST_OPT

	echo ""
	printf "WAL lsn After test:  "
	$BIN/psql -t  $DB -c "SELECT pg_current_xlog_location()"
}

function run_test()
{
	#--------------------------------
	label "--- init DB  ---"
	#--------------------------------
	init_test_db 2>&1 >> $LOG2

	#--------------------------------
	label "--- IMHASH ---"
	#--------------------------------
	i=1

	while [ $i -le $NUM_OF_RUNS ];
	do
		#-----------------------------------
		label "IMHASH PGBENCH-TEST RUN : $i"
		#-----------------------------------
		init_test_imhash 2>&1 >> $LOG2
		XLOG_B=\'$($BIN/psql $DB -t -c "SELECT pg_current_xlog_location()")\'

		# Run pgbench
		run_pgbench

		# Calculated WAL size
		printf "Generated WAL Size:  "
		$BIN/psql -t $DB -c "SELECT
		pg_size_pretty(pg_xlog_location_diff(pg_current_xlog_location(),
											 trim($XLOG_B)::pg_lsn))"
		((i++))
	done

	#--------------------------------
	label "--- BTREE ---"
	#--------------------------------
	i=1

	while [ $i -le $NUM_OF_RUNS ];
	do
    	#--------------------------------
 		label "BTREE PGBENCH-TEST RUN : $i"
		#--------------------------------
		init_test_btree 2>&1 >> $LOG2
		XLOG_B=\'$($BIN/psql $DB -t -c "SELECT pg_current_xlog_location()")\'

		# Run pgbench
		run_pgbench

		# Calculated WAL size
		printf "Generated WAL Size:  "
		$BIN/psql -t $DB -c "SELECT
		pg_size_pretty(pg_xlog_location_diff(pg_current_xlog_location(),
											 trim($XLOG_B)::pg_lsn))"
		((i++))
	done
	#---------------------------------------
	label "Server Configuration"
	#---------------------------------------
	$BIN/psql -d $DB -c "SELECT name, current_setting(name), SOURCE FROM pg_settings
WHERE SOURCE NOT IN ('default', 'override');"
	$BIN/pg_ctl -D $DATA stop -m i 2>&1 >> $LOG2
}

run_test 2>&1 > $RESULT

#tail -15 $DATA/postgresql.conf >> $RESULT
#rm /tmp/tmp_result.out
#run_test 2>&1 | tee /tmp/tmp_result.out
#cat /tmp/tmp_result.out | grep -v '^$\|client\|NOTICE\|aid\|bid\|delta\|tid'  > $RESULT
#---------------------------------------
label "RESULT STORED IN \"$RESULT\""
#---------------------------------------

