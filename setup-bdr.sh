# function to print label
function header(){
echo "--------------------------------"
echo $1
echo "--------------------------------"
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------
BIN="/home/amul/Public/ppas14/inst/bin"
BDRDIR="/tmp/bdr"
BDRSRC="/home/amul/work/source/bdr"

DB="postgres"

PORT0="5598"
PORT1="5599"

DATA0="$BDRDIR/bdr0"
DATA1="$BDRDIR/bdr1"

$BIN/pg_ctl -m i -D $DATA0 stop 
$BIN/pg_ctl -m i -D $DATA1 stop 

rm -rf $BDRDIR
mkdir $BDRDIR

#--------------------------------
 header "   INIT CLUSTER 0  "
#--------------------------------
$BIN/initdb $DATA0 > /dev/null
cat >> $DATA0/postgresql.conf <<EOF
port=$PORT0
shared_preload_libraries = 'bdr'
wal_level = 'logical'
track_commit_timestamp = on
max_wal_senders = 20
max_replication_slots = 20
wal_log_hints = on
wal_receiver_status_interval = 1
listen_addresses='*'
log_min_messages=debug1
wal_sender_timeout = 0
logical_decoding_work_mem=64MB
max_worker_processes = 100
bdr.ddl_replication = on
bdr.default_replica_identity= full
bdr.trace_enable= off
track_functions = all
maintenance_work_mem = 256MB
max_parallel_maintenance_workers = 4
EOF
cat >> $DATA0/pg_hba.conf <<EOF
local   all   all                  trust
host    all   all     127.0.0.1/32 trust
host    all   all     ::1/128      trust
EOF


#--------------------------------
 header "   INIT CLUSTER 1  "
#--------------------------------
$BIN/initdb $DATA1 > /dev/null

cat >> $DATA1/postgresql.conf <<EOF
port=$PORT1
shared_preload_libraries = 'bdr'
wal_level = 'logical'
track_commit_timestamp = on
max_wal_senders = 20
max_replication_slots = 20
wal_log_hints = on
wal_receiver_status_interval = 1
listen_addresses='*'
log_min_messages=debug1
wal_sender_timeout = 0
logical_decoding_work_mem=64MB
max_worker_processes = 100
bdr.ddl_replication = on
bdr.default_replica_identity= full
bdr.trace_enable= off
track_functions = all
maintenance_work_mem = 256MB
max_parallel_maintenance_workers = 4
EOF
cp -f $DATA0/pg_hba.conf $DATA1/pg_hba.conf


#--------------------------------
 header "   START CLUSTER 0 Port: $PORT0 "
#--------------------------------
$BIN/pg_ctl -l $BDRDIR/bdr$PORT0.log -D $DATA0 -w start

#--------------------------------
 header "   START CLUSTER 1  Port: $PORT1"
#--------------------------------
$BIN/pg_ctl -l $BDRDIR/bdr$PORT1.log -D $DATA1 -w start


#--------------------------------
 header "   BDR SETTING FOR CLUSTER 0"
#--------------------------------
$BIN/psql -e -p $PORT0 $DB -c 'CREATE EXTENSION bdr CASCADE;'

$BIN/psql -e -p $PORT0 $DB \
	-c "SELECT bdr.create_node(node_name := 'node1', local_dsn := 'port=$PORT0 dbname=$DB host=localhost')";

$BIN/psql -e -p $PORT0 $DB -c "SELECT bdr.create_node_group(node_group_name := 'bdrgroup')";

#--------------------------------
 header "   BDR SETTING FOR CLUSTER 1"
#--------------------------------
$BIN/psql -p $PORT1 -d $DB -c 'CREATE EXTENSION bdr CASCADE;'

$BIN/psql -e -p $PORT1 $DB \
	-c "SELECT bdr.create_node(node_name := 'node2', local_dsn := 'port=$PORT1 dbname=$DB host=localhost')";

$BIN/psql -e -p $PORT1 $DB -c "SELECT bdr.join_node_group(join_target_dsn := 'port=$PORT0 dbname=$DB host=localhost');";

