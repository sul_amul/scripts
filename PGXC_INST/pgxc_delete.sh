#!/bin/sh
################################################################
# pgxc_delete.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

echo "****************************"
echo "*** Delete *****************"
echo "****************************"

### USER CHECK ###
uname=`id -u -n`
if [ $uname = "root" ];then
  echo "### ERROR : ROOT USER CAN NOT RUN"
  exit 1
fi

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

### PARAMETER CHECK ###
delete_code=0
if [ $# -eq 0 ];then
  delete_code=1
elif [ $# -eq 1 ];then
  delete_code=2
  delete_node=$1
fi
if [ $delete_code -eq 0 ];then
  echo "Usage : ${0##*/} [ name ]"
  exit 1
fi
if [ $delete_code -eq 2 ];then
  no=-1
  cnt=0
  while [ $cnt -lt $allcnt ]
  do
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    if [ $name = $delete_node ];then
      if [ $cnt -le $ge ];then
        no=0
      elif [ $cnt -le $pe ];then
        no=1
      elif [ $cnt -le $ce ];then
        no=2
      else
        no=3
      fi
      delete_cnt=$cnt
      break
    fi
    cnt=`expr $cnt + 1`
  done
  if [ $no -eq -1 ];then
    echo "### ERROR : INVALID NAME"
    exit 1
  fi
fi

### COMPONENT NAME SPECIFIED ###
if [ $delete_code -eq 2 ];then
  component=("gtm" "gtm_proxy" "coordinator" "datanode")
  node=("gtm" "gtm" "node" "node")
  slan=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $2}'`
  port=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $4}'`
  data=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $5}'`
  pgxc_monitor -Z $node -h $slan -p $port
  if [ $? -eq 0 ];then
    echo "### ERROR : COMPONENT IS RUNNING (slan=$slan, port=$port)"
    exit 1
  fi
  echo "*****  $slan(${component[$no]})  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "rm -r $newdata" || { echo "### ERROR : DELETE (${component[$no]})"; }
  if [ $no -ge 2 ];then
    arch=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $6}'`
    xlog=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $7}'`
    newarch=`ssh $slan "echo $arch"`
    ssh $slan "rm -r $newarch" || { echo "### ERROR : DELETE (${component[$no]})"; }
    newxlog=`ssh $slan "echo $xlog"`
    ssh $slan "rm -r $newxlog" || { echo "### ERROR : DELETE (${component[$no]})"; }
    if [ $ha = "on" ];then
      check=`expr $delete_cnt - $ds`
      check=`expr $check % 2`
      if [ $check -eq 0 ];then
        pair_cnt=`expr $delete_cnt + 1`
      else
        pair_cnt=`expr $delete_cnt - 1`
      fi
      slan_2=`echo ${all[$pair_cnt]} | awk -F'[,]' '{print $2}'`
      echo "*****  $slan_2(${component[$no]})  *****"
      ssh $slan_2 "rm -r $newdata $newarch $newxlog" || { echo "### ERROR : DELETE (${component[$no]})"; }
    fi
  fi
  exit 0
fi

### COMPONENT START CHECK ###
cnt=$gs
while [ $cnt -lt $allcnt ]
do
  slan=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $2}'`
  port=`echo ${all[$delete_cnt]} | awk -F'[,]' '{print $4}'`
  if [ $cnt -le $pe ];then
    pgxc_monitor -Z gtm -h $slan -p $port
  else
    pgxc_monitor -Z node -h $slan -p $port
  fi
  if [ $? -eq 0 ];then
    echo "### ERROR : COMPONENT IS RUNNING (slan=$slan, port=$port)"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

### Coordinator , Datanode ###
cnt=$cs
while [ $cnt -le $de ]
do
  if [ $ha = "on" ];then
    slan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    arch_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
    xlog_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    newdata_1=`ssh $slan_1 "echo $data_1"`
    newarch_1=`ssh $slan_1 "echo $arch_1"`
    newxlog_1=`ssh $slan_1 "echo $xlog_1"`
    cnt=`expr $cnt + 1`
    slan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    arch_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
    xlog_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    newdata_2=`ssh $slan_2 "echo $data_2"`
    newarch_2=`ssh $slan_2 "echo $arch_2"`
    newxlog_2=`ssh $slan_2 "echo $xlog_2"`
    echo "*****  $slan_1  *****"
    ssh $slan_1 "rm -r $newdata_1 $newarch_1 $newxlog_1 $newdata_2 $newarch_2 $newxlog_2" || { echo "### ERROR : DELETE (Coordinator,Datanode)"; }
    echo "*****  $slan_2  *****"
    ssh $slan_2 "rm -r $newdata_1 $newarch_1 $newxlog_1 $newdata_2 $newarch_2 $newxlog_2" || { echo "### ERROR : DELETE (Coordinator,Datanode)"; }
  else
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    arch=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
    xlog=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    echo "*****  $slan  *****"
    newdata=`ssh $slan "echo $data"`
    newarch=`ssh $slan "echo $arch"`
    newxlog=`ssh $slan "echo $xlog"`
    ssh $slan "rm -r $newdata $newarch $newxlog" || { echo "### ERROR : DELETE (Coordinator,Datanode)"; }
  fi
  cnt=`expr $cnt + 1`
done

### GTM , GTM_proxy ###
cnt=$gs
while [ $cnt -le $pe ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "rm -r $newdata" || { echo "### ERROR : DELETE (GTM,GTM_proxy)"; }
  cnt=`expr $cnt + 1`
done

exit 0
