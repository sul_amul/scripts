#!/bin/sh
################################################################
# pgxc_ha_stop.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### DEFINE
timeout=30

### PARAMETER CHECK ###
code=0
d_code=0
declare -i g_num=0
if [ $# -eq 0 ];then
  code=1
elif [ $# -eq 1 ];then
  case $1 in
  --gtm) code=2 ;;
  esac
elif [ $# -eq 2 ];then
  case $1 in
  --gtm) code=3 ;;
  --data) code=4 ;;
  esac
  g_num=$2
elif [ $# -eq 3 ];then
  case $1 in
  --data) code=5 ;;
  esac
  g_num=$2
  case $3 in
  A) d_code=1 ;;
  B) d_code=2 ;;
  *) code=0 ;;
  esac
fi
if [ $code -eq 0 -o $code -eq 3 -a $g_num -ne 1 -a $g_num -ne 2 ];then
  echo "Usage : ${0##*/} [ --gtm [ 1 | 2 ] | --data {n} [ A | B } ] (n : group number)"
  exit 1
fi

### FILE CHECK ###
ha_define_file="./pgxc_ha_define.conf"
if [ ! -f $ha_define_file ];then
  echo "### ERROR : NOT FOUND HA DEFINE FILE (file=$ha_define_file)"
  exit 1
fi
source $ha_define_file

### GROUP NUMBER CHECK ###
if [ $code -eq 4 -o $code -eq 5 ];then
  if [ $g_num -le 0 -o $g_num -gt $group_num ];then
    echo "### ERROR : INVALID GROUP NUMBER"
    exit 1
  fi
fi

### DATA PACEMAKER STOP ###
if [ $code -eq 1 -o $code -eq 4 -o $code -eq 5 ];then
  g_check_1=`expr $g_num \* 2 - 1`
  g_check_2=0
  if [ $code -eq 4 ];then
    g_check_2=`expr $g_check_1 + 1`
  elif [ $code -eq 5 -a $d_code -eq 2 ];then
    g_check_1=`expr $g_check_1 + 1`
  fi
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check_1 -eq $cnt -o $g_check_2 -eq $cnt ];then
      ssh -t ${data_slan[$cnt]} "sudo -b service heartbeat stop > /dev/null" || { echo "### ERROR : PACEMAKER STOP (DATA:${data_slan[$cnt]})"; exit 1; }
    fi
    cnt=`expr $cnt + 1`
  done
  sleep 10
fi

### GTM PACEMAKER STOP ###
if [ $code -eq 1 -o $code -eq 2 ];then
  ssh -t ${gtm_slan[2]} "sudo -b service heartbeat stop > /dev/null" || { echo "### ERROR : PACEMAKER STOP (GTM:${gtm_slan[2]})"; exit 1; }
  ssh -t ${gtm_slan[1]} "sudo -b service heartbeat stop > /dev/null" || { echo "### ERROR : PACEMAKER STOP (GTM:${gtm_slan[1]})"; exit 1; }
elif [ $code -eq 3 ];then
  ssh -t ${gtm_slan[$g_num]} "sudo -b service heartbeat stop > /dev/null" || { echo "### ERROR : PACEMAKER STOP (GTM:${gtm_slan[$g_num]})"; exit 1; }
fi

### PACEMAKER STOP CHECK###
if [ $code -eq 1 -o $code -eq 4 -o $code -eq 5 ];then
  g_check_1=`expr $g_num \* 2 - 1`
  g_check_2=0
  if [ $code -eq 4 ];then
    g_check_2=`expr $g_check_1 + 1`
  elif [ $code -eq 5 -a $d_code -eq 2 ];then
    g_check_1=`expr $g_check_1 + 1`
  fi
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check_1 -eq $cnt -o $g_check_2 -eq $cnt ];then
      t_cnt=0
      while true
      do
        ssh ${data_slan[$cnt]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
        if [ $? -ne 0 ];then
          break
        fi
        if [ $t_cnt -ge $timeout ];then
          echo "### ERROR : TIME OUT"
          exit 1
        fi
        t_cnt=`expr $t_cnt + 1`
        sleep 10
      done
    fi
    cnt=`expr $cnt + 1`
  done
fi

if [ $code -eq 1 -o $code -eq 2 ];then
  t_cnt=0
  while true
  do
    ssh ${gtm_slan[2]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
    if [ $? -ne 0 ];then
      ssh ${gtm_slan[1]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
      if [ $? -ne 0 ];then
        break
      fi
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
elif [ $code -eq 3 ];then
  t_cnt=0
  while true
  do
    ssh ${gtm_slan[$g_num]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
    if [ $? -ne 0 ];then
      break
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
fi
