#!/bin/sh
################################################################
# pgxc_monitor.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

### GTM MONITOR
echo "##### GTM MONITOR #####"
cnt=$gs
while [ $cnt -le $ge ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  pgxc_monitor -Z gtm -h $slan -p $port
  if [ $? -eq 0 ];then
    ret=`ssh $slan "gtm_ctl status -Z gtm -D $data 2> /dev/null"`
    sts=`echo $ret | awk '{print $NF}'`
    echo "$slan : GTM($sts) is running."
  else
    echo "$slan : GTM is not running. *****"
  fi
  cnt=`expr $cnt + 1`
done

### GTM PROXY MONITOR
echo "##### GTM PROXY MONITOR #####"
cnt=$ps
while [ $cnt -le $pe ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
  pgxc_monitor -Z gtm -h $slan -p $port
  if [ $? -eq 0 ];then
    echo "$slan : GTM_proxy is running."
  else
    echo "$slan : GTM_proxy is not running. *****"
  fi
  cnt=`expr $cnt + 1`
done

### COORDINATOR , DATANODE MONITOR
echo "##### COORDINATOR MONITOR #####"
comp="Coordinator"
cnt=$cs
while [ $cnt -le $de ]
do
  if [ $cnt -eq $ds ];then
    echo "##### DATANODE MONITOR #####"
    comp="Datanode"
  fi
  if [ $ha = "on" ];then
    slan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    port_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    cnt=`expr $cnt + 1`
    slan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    port_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    a_slan=($slan_1 $slan_2 $slan_2 $slan_1)
    a_port=($port_1 $port_1 $port_2 $port_2)
    a_data=($data_1 $data_1 $data_2 $data_2)
    for ii in 0 1 2 3
    do
      no=`expr $ii / 2 + 1`
      pgxc_monitor -Z node -h ${a_slan[$ii]} -p ${a_port[$ii]}
      if [ $? -eq 0 ];then
        rcv=`ssh ${a_slan[$ii]} "ls ${a_data[$ii]}/recovery.conf" 2> /dev/null`
        if [ $? -ne 0 ];then
          echo "${a_slan[$ii]} : $comp$no(master) is running."
        else
          echo "${a_slan[$ii]} : $comp$no(slave) is running."
        fi
      else
        echo "${a_slan[$ii]} : $comp$no is not running. *****"
      fi
    done
  else
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    pgxc_monitor -Z node -h $slan -p $port
    if [ $? -eq 0 ];then
      echo "$slan : $comp is running."
    else
      echo "$slan : $comp is not running. *****"
    fi
  fi
  cnt=`expr $cnt + 1`
done
