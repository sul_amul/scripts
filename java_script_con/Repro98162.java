import java.sql.*;
import java.util.*;
import java.io.*;

public class Repro98162 implements Runnable
{
    public void run()
    {
	Connection c = null;
	try
	{
            Class.forName("com.edb.Driver");
	    String url      = "jdbc:edb://localhost:5444/edb";
            String user     = "amul";
            String password = "amul";
            c    = DriverManager.getConnection(url, user, password);
            c.setAutoCommit(true);
	    Random r = new Random(); 

	    PreparedStatement bps = c.prepareStatement("BEGIN");
	    bps.execute();
	    System.out.println("thread started and connected");

	    //PreparedStatement ps = c.prepareStatement("INSERT INTO public.SL_POSTING (POSTING_INSTRUCTION_ID, TYPE, AMOUNT, PRODUCT_OWNER, INSERT_TIMESTAMP ) VALUES (?, 'COMMITTED_POSTING', ?, 'DEFAULT', ?) ON CONFLICT ON CONSTRAINT posting_pkey DO NOTHING");
	    //												                                             1     2            3                4       5      6       7             8      9            10,   11,             12         13  14
	    PreparedStatement ps = c.prepareStatement("INSERT INTO public.SL_POSTING (POSTING_INSTRUCTION_ID, TYPE, ACCOUNT_ID, ACCOUNT_ADDRESS, AMOUNT, ASSET, CREDIT, DENOMINATION, PHASE, PRODUCT_OWNER, ABI, INSERT_TIMESTAMP, EVENT_ID,ID_BATCH) VALUES   (?,  ?, ? , ? ,? ,? ,? ,? ,? ,? ,? ,? ,? , (select pg_backend_pid() from pg_sleep(random())))");

// 2023-10-25 00:33:21.589 CEST [3244786]: u=[isyp_app] db=[isyp0] app=[EnterpriseDB JDBC Driver] c=[10.247.13.47] s=[6538462d.3182f2:4681] tx=[14/38394:37344172]DETAIL:  parameters: $1 = 'ff43d513-30ce-432f-b65c-1ecde912b167', $2 = 'COMMITTED_POSTING', $3 = '1', $4 = 'DEFAULT', $5 = '3.85', $6 = 'COMMERCIAL_BANK_MONEY', $7 = 'f', $8 = 'EUR', $9 = 'POSTING_PHASE_PENDING_OUTGOING', $10 = 'VLTC0', $11 = '03385', $12 = '2023-10-25 00:00:49.647692+02', $13 = '35AUTRE20231025ZE001000002149ZE001', $14 = '5255' 
 

// 2023-10-25 00:33:21.589 CEST [3244788]: u=[isyp_app] db=[isyp0] app=[EnterpriseDB JDBC Driver] c=[10.247.13.47] s=[6538462d.3182f4:7861] tx=[16/29786:37344170]DETAIL:  parameters: $1 = '64da16a0-c110-4ced-9219-777fefde13ab', $2 = 'COMMITTED_POSTING', $3 = '1', $4 = 'DEFAULT', $5 = '41.91', $6 = 'COMMERCIAL_BANK_MONEY', $7 = 't', $8 = 'EUR', $9 = 'POSTING_PHASE_PENDING_INCOMING', $10 = 'VLTC0', $11 = '03385', $12 = '2023-10-25 00:01:09.908802+02', $13 = '35AUTRE20231025ZE001000003790ZE001', $14 = '5255'

	    for(int z = 0 ; z <= 100; z++) {
		try {
		  for(Integer pk = 0; pk < 3000; pk++) {
		    //ps.setString(1, new Integer(r.nextInt(100)).toString());
		    ps.setString(1, pk.toString());
		    ps.setString(2, "COMMITTED_POSTING");
		    ps.setString(3, "1");
		    ps.setString(4, "DEFAULT");
		    ps.setString(5, new Integer(r.nextInt(50)).toString());
		    ps.setString(6, "COMMERCIAL_BANK_MONEY");
		    ps.setBoolean(7, true);
		    ps.setString(8, "EUR");
		    ps.setString(9, "POSTING_PHASE_PENDING_INCOMING");
		    ps.setString(10, "VLTC0");
		    ps.setString(11, "03385");

		    // within 1 day
		    Timestamp rTimestamp;
		    long tStart = Timestamp.valueOf("2023-10-24 23:58:00").getTime();
		    long tEnd = Timestamp.valueOf("2023-10-25 00:02:00").getTime();
		    long diff = tEnd - tStart + 1;
		    rTimestamp = new Timestamp(tStart + (long)(Math.random() * diff));
		    //System.out.println("adding " + rTimestamp);
		    ps.setTimestamp(12, rTimestamp);
		    ps.setString(13, new Integer(r.nextInt(100000)).toString());
		    //ps.setLong(14, Long.parseLong("5255"));

		    ps.addBatch();
		}
	        int[] retBatch = ps.executeBatch();
		//Thread.sleep(100); //increases prop. of some deadlocks on INSERTs
		c.commit();
		System.out.println("bdone in " + Thread.currentThread().getId());
	      } catch (SQLException exp) {
		System.out.println("SQL Exception: " + exp.getMessage());
                System.out.println("SQL State:     " + exp.getSQLState());
                System.out.println("Vendor Error:  " + exp.getErrorCode());
		try {
		    if(c != null) c.rollback();
	        } catch(Exception ex2) {
		}
	      }
	    }
	    c.close();
	}
        catch(ClassNotFoundException e)
	{
            System.out.println("Class Not Found : " + e.getMessage());
        }
	catch(SQLException exp)
        {
	    System.out.println("SQL Exception: " + exp.getMessage());
            System.out.println("SQL State:     " + exp.getSQLState());
            System.out.println("Vendor Error:  " + exp.getErrorCode());
        }
	//catch(InterruptedException e) {
        //}
    }

    public static void main(String[] args)
    {
	try {
            Class.forName("com.edb.Driver");
	    for(int i = 0; i < 30; i++) {
		Thread t = new Thread(new Repro98162());
	        t.start();
		System.out.println("thread starting");
	    }
	    Thread.sleep(10*1000);
	    System.exit(0);
	} catch (Exception e) {
	    System.out.println("Runtime exception: " + e.getMessage());
	}
    }
}
