#!/bin/bash - 
#===============================================================================
#
#          FILE: rec_file.sh
# 
#         USAGE: ./rec_file.sh 
# 
#   DESCRIPTION:   Process a file line by line with PIPED while-read loop.
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 08/08/2014 15:14
#===============================================================================

set -o nounset                              # Treat unset variables as an error


FILENAME=$1
count=0
cat $FILENAME | while read LINE
do
       let count++
       echo "$count $LINE"
	perl /home/amul/work/source/scripts/replace/replace.pl "$LINE"
done
echo -e "\nTotal $count Lines read"

