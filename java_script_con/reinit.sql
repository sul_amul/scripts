
CREATE SCHEMA isyp_sl;
DROP TABLE isyp_sl.sl_posting;

CREATE TABLE isyp_sl.sl_posting (
    posting_instruction_id text NOT NULL,
    type text NOT NULL,
    account_id text NOT NULL,
    account_address text NOT NULL,
    amount text,
    asset text NOT NULL,
    credit boolean,
    denomination text NOT NULL,
    phase text NOT NULL,
    product_owner character varying(30) DEFAULT NULL::character varying,
    abi character varying(30) DEFAULT NULL::character varying,
    insert_timestamp timestamp with time zone NOT NULL,
    tms_insert timestamp with time zone DEFAULT clock_timestamp() NOT NULL,
    event_id text,
    id_batch bigint
) PARTITION BY RANGE (insert_timestamp) INTERVAL ('1 second'::interval)
(
  PARTITION p0 VALUES LESS THAN('01-JAN-2023')
);

--
ALTER TABLE isyp_sl.sl_posting ADD CONSTRAINT posting_pkey PRIMARY KEY (posting_instruction_id, type, account_id, asset, account_address, phase, denomination, insert_timestamp);

