#! /bin/sh

#-------------------------------------------
# Author: Amul Sul
# Objective : Analyse streaming replication
#   The connection of Streaming Replication 
#   comes to be closed during  restoring the
#   dumped data into master server.
# Ref : Question ID:112968.	
#-------------------------------------------


# function to print label 
function header(){
echo "------------------------"
echo $1
echo "------------------------"
sleep 5
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------

# NOTE: Change the name of the DUMPDATA 

#created using  
#pg_dump  -U postgres -p $PGPORT0 -F t -f pgdump_data.tar
DUMPDATA="pgdump_data.tar"

#postgres setting
STD_CNT=2
PGDATA0="/tmp/Master"
PGDATA_STDBY="/tmp/Standby"
PGARC="arc"
PGPORT0=65432
PGPORT_STD=65433
MASTERLOG="Master.log"
unset PGPORT
unset PGDATA

echo "Postgresql is \"`which postgres`\""
killall -9 postgres

rm $MASTERLOG 
rm -rf $PGDATA0 $PGARC
ls -d $PGDATA_STDBY[0-9] | xargs rm -rf
mkdir $PGARC

#--------------------------------
 header " CREATING MASTER   "
#--------------------------------

initdb $PGDATA0

# postgres.conf custom settings
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
wal_level = hot_standby
archive_mode = on
archive_command = 'cp %p ../$PGARC/%f'
checkpoint_segments = 3 
max_wal_senders = 2
wal_keep_segments = 1 
hot_standby = on
max_standby_archive_delay = -1
max_standby_streaming_delay = -1
hot_standby_feedback = on
synchronous_standby_names = '*'
log_min_messages = debug1
log_line_prefix = '[Master] %t'
log_checkpoints = on
log_error_verbosity = verbose
log_lock_waits = on
track_activities = on
track_counts = on
track_functions = all
EOF

# pg_hba.conf for replication
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all			trust
host   replication	all	127.0.0.1/32	trust
host   replication      all     ::1/128		trust
EOF

#--------------------------------
 header " STARTING UP MASTER   "
#--------------------------------

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG 

i=0

while [ $i -lt $STD_CNT ];
do

((i++))
PGDATA_STD="$PGDATA_STDBY$i"
#--------------------------------
 header " BASEBACKUP $PGDATA_STD "
#--------------------------------

pg_basebackup -P -p $PGPORT0 -F p -x -D $PGDATA_STD

# recovery.conf in standby
cat >> $PGDATA_STD/recovery.conf <<EOF
standby_mode = 'yes'
recovery_target_timeline='latest'
primary_conninfo='host=localhost port=$PGPORT0'
restore_command='cp ../$PGARC/%f %p'
EOF

# postgres.conf custom setting for standby
cat >> $PGDATA_STD/postgresql.conf <<EOF
port=$PGPORT_STD
wal_level = hot_standby
archive_mode = on
archive_command = 'cp %p ../$PGARC/%f'
max_wal_senders = 2
#wal_keep_segments = 32 
hot_standby = on
max_standby_archive_delay = -1
max_standby_streaming_delay = -1
hot_standby_feedback = on
log_min_messages = debug1
log_line_prefix = '[Standby] %t'
log_checkpoints = on
log_error_verbosity = verbose
log_lock_waits = on
track_activities = on
track_counts = on
track_functions = all
EOF

#--------------------------------
 header "  STARTING UP $PGDATA_STD  "
#--------------------------------

pg_ctl -D $PGDATA_STD start -w -l "$PGDATA_STDBY$i.log" 

((PGPORT_STD++))
done

# Creating postgres user on master
createuser postgres -s -p $PGPORT0

#--------------------------------
 # header "   RUNNING PG-BENCH    "
#--------------------------------
#pgbench -i -s 350 -U postgres -p $PGPORT0

#--------------------------------
 header "   RUNNING PG-RESTORE "
#--------------------------------

pg_restore -v -d postgres -U postgres \
 -p $PGPORT0 -F t $DUMPDATA

#--------------------------------
 header "       DONE  "
#--------------------------------
