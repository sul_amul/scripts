#!/bin/bash -
#===============================================================================
#
#          FILE: edb-check-quick.sh
#
#         USAGE: ./edb-check-quick.sh
#
#   DESCRIPTION: 1. copy EDBAS to /tmp/regression_tests
#		 2. run ./configure
#                3. make edb-check-quick
#
#        AUTHOR: Amul Sul (sulamul@yahoo.com)
#       CREATED: 11/05/2015 22:46
#===============================================================================

#===============================================================================
#   FUNCTIONS
#===============================================================================

#-------------------------------------------------------------------------------
#     Print output description
#-------------------------------------------------------------------------------
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}	# ----------  end of function label  ----------


#-------------------------------------------------------------------------------
# Config ....
#-------------------------------------------------------------------------------
DIR="/tmp/RM5444"
OLDBIN="$DIR/inst/bin"
NEWBIN="$DIR/inst/bin"

OLDDATA="$DIR/inst/data"
NEWDATA="$DIR/inst/data_new"

NEWPORT=5445
#-------------------------------------------------------------------------------
# Test....
#-------------------------------------------------------------------------------
pushd .
cd $DIR

#$OLDBIN/pg_ctl -D $OLDDATA stop -w -m immediate &> /dev/null
#rm -rf $OLDDATA &> /dev/null
#$OLDBIN/initdb $OLDDATA &> /dev/null

$OLDBIN/pg_ctl -D  $OLDDATA -w start &> /dev/null
#$OLDBIN/psql -d edb -f /tmp/rm42745_test.sql
$OLDBIN/pg_dump -d edb --binary-upgrade > /tmp/old_db.sql
$OLDBIN/pg_ctl -D  $OLDDATA -w stop &> /dev/null


$NEWBIN/pg_ctl -D $NEWDATA stop -w -m immediate &> /dev/null
rm -rf $NEWDATA &> /dev/null
$NEWBIN/initdb $NEWDATA &> /dev/null

cat >> $NEWDATA/postgresql.conf <<EOF
port = 5445
EOF

label "$NEWBIN/pg_upgrade --version"
$NEWBIN/pg_upgrade --version
label "$OLDBIN/psql --version"
$OLDBIN/psql --version
label "$NEWBIN/psql --version"
$NEWBIN/psql --version

label "perfor pg_upgrade"
$NEWBIN/pg_upgrade -b $OLDBIN -B $NEWBIN -d $OLDDATA -D $NEWDATA -p $RM -P $NEWPORT

$NEWBIN/pg_ctl -D  $NEWDATA -w start &> /dev/null
$NEWBIN/pg_dump -d edb -p $NEWPORT --binary-upgrade > /tmp/new_db.sql
$NEWBIN/pg_ctl -D  $NEWDATA -w stop &> /dev/null
popd
