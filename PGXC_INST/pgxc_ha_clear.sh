#!/bin/sh
################################################################
# pgxc_ha_clear.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### FILE CHECK ###
ha_define_file="./pgxc_ha_define.conf"
if [ ! -f $ha_define_file ];then
  echo "### ERROR : NOT FOUND HA DEFINE FILE (file=$ha_define_file)"
  exit 1
fi
source $ha_define_file

### PACEMAKER STOP CHECK ###
ssh ${gtm_slan[1]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
if [ $? -eq 0 ];then
  echo "### ERROR : PACEMAKER IS RUNNING (${gtm_slan[1]})"
  exit 1
fi
ssh ${gtm_slan[2]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
if [ $? -eq 0 ];then
  echo "### ERROR : PACEMAKER IS RUNNING (${gtm_slan[2]})"
  exit 1
fi
cnt=1
while [ $cnt -le $data_num ]
do
  ssh ${data_slan[$cnt]} "ps -ef | grep \"heartbeat: master control process\" | grep -v grep" > /dev/null
  if [ $? -eq 0 ];then
    echo "### ERROR : PACEMAKER IS RUNNING (${data_slan[$cnt]})"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

##### GTM CLEAR #####
ssh -t ${gtm_slan[1]} "sudo rm -r /var/lib/heartbeat/crm/*" > /dev/null
ssh -t ${gtm_slan[2]} "sudo rm -r /var/lib/heartbeat/crm/*" > /dev/null

##### DATA CLEAR #####
cnt=1
while [ $cnt -le $data_num ]
do
  next=`expr $cnt + 1`
  ssh -t ${data_slan[$cnt]} "sudo rm -r /var/lib/heartbeat/crm/*" > /dev/null
  ssh -t ${data_slan[$next]} "sudo rm -r /var/lib/heartbeat/crm/*" > /dev/null
  ssh ${data_slan[$cnt]} "rm -f ${coord_tmp[$cnt]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$cnt]} "rm -f ${coord_tmp[$next]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$next]} "rm -f ${coord_tmp[$cnt]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$next]} "rm -f ${coord_tmp[$next]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$cnt]} "rm -f ${dn_tmp[$cnt]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$cnt]} "rm -f ${dn_tmp[$next]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$next]} "rm -f ${dn_tmp[$cnt]}/PGSQL.lock" > /dev/null
  ssh ${data_slan[$next]} "rm -f ${dn_tmp[$next]}/PGSQL.lock" > /dev/null
  cnt=`expr $cnt + 2`
done
