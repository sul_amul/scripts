import psycopg2
from datetime import datetime

# Update connection string information
host = "127.0.0.1"
dbname = "edb"
user = "amul"
password = "amul"
sslmode = "prefer"
port = 5444

# Construct connection string
conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4} port={5}".format(host, user, dbname, password, sslmode, port)
conn = psycopg2.connect(conn_string)
print("Connection established")

cursor = conn.cursor()

for i in range(1,20):
    cursor.execute("""CREATE TABLE SOURCE_TABLE_{}
    (
      prod_id           text,
      prod_quantity     int,
      sold_month        date,
      primary key (prod_id, sold_month)
    );""".format(i))


for i in range(1,20):
    cursor.execute("""INSERT INTO SOURCE_TABLE_{} 
    select 
      'T{}_'||n, -- WITH DIFFERENT KEY
      1, 
      timestamp '2023-03-01 00:00:00' + random() * '30 days'::interval  
    from generate_series(1, 100000) as n;""".format(i,i))


# Clean up
conn.commit()
cursor.close()
conn.close()
