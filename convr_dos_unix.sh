#!/bin/bash - 
#===============================================================================
#
#          FILE: convr_dos_unix.sh
# 
#         USAGE: ./convr_dos_unix.sh 
# 
#   DESCRIPTION: conver dos to unix.
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 07/03/2014 12:11
#===============================================================================

set -o nounset                              # Treat unset variables as an error

for f in *;
do
    awk '{ sub("\r$", ""); print }' "$f" > temp;
	mv temp "$f";
done
