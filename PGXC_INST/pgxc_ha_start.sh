#!/bin/sh
################################################################
# pgxc_ha_start.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### DEFINE
check2="master-Gtm"
check3="1000"
check4="PRI"
check5="HS:alone"
check6="HS:sync"
check_c1="coordinator1-status"
check_c2="coordinator2-status"
check_d1="datanode1-status"
check_d2="datanode2-status"
timeout=30

### PARAMETER CHECK ###
code=0
d_code=0
declare -i g_num=0
if [ $# -eq 0 ];then
  code=1
elif [ $# -eq 1 ];then
  case $1 in
  --gtm) code=2 ;;
  esac
elif [ $# -eq 2 ];then
  case $1 in
  --gtm) code=3 ;;
  --data) code=4 ;;
  esac
  g_num=$2
elif [ $# -eq 3 ];then
  case $1 in
  --data) code=5 ;;
  esac
  g_num=$2
  case $3 in
  A) d_code=1 ;;
  B) d_code=2 ;;
  *) code=0 ;;
  esac
fi
if [ $code -eq 0 -o $code -eq 3 -a $g_num -ne 1 -a $g_num -ne 2 ];then
  echo "Usage : ${0##*/} [ --gtm [ 1 | 2 ] | --data {n} [ A | B } ] (n : group number)"
  exit 1
fi

### FILE CHECK ###
ha_define_file="./pgxc_ha_define.conf"
if [ ! -f $ha_define_file ];then
  echo "### ERROR : NOT FOUND HA DEFINE FILE (file=$ha_define_file)"
  exit 1
fi
source $ha_define_file

### GROUP NUMBER CHECK ###
if [ $code -eq 4 -o $code -eq 5 ];then
  if [ $g_num -le 0 -o $g_num -gt $group_num ];then
    echo "### ERROR : INVALID GROUP NUMBER"
    exit 1
  fi
fi

##### CLEAR #####
if [ $code -eq 1 -o $code -eq 4 -o $code -eq 5 ];then
  cnt=1
  while [ $cnt -le $data_num ]
  do
    next=`expr $cnt + 1`
    if [ $code -eq 1 -o $code -eq 4 -o $d_code -eq 1 ];then
      ssh ${data_slan[$cnt]} "rm -f ${coord_tmp[$cnt]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$cnt]} "rm -f ${coord_tmp[$next]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$cnt]} "rm -f ${dn_tmp[$cnt]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$cnt]} "rm -f ${dn_tmp[$next]}/PGSQL.lock" > /dev/null
    fi
    if [ $code -eq 1 -o $code -eq 4 -o $d_code -eq 2 ];then
      ssh ${data_slan[$next]} "rm -f ${coord_tmp[$cnt]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$next]} "rm -f ${coord_tmp[$next]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$next]} "rm -f ${dn_tmp[$cnt]}/PGSQL.lock" > /dev/null
      ssh ${data_slan[$next]} "rm -f ${dn_tmp[$next]}/PGSQL.lock" > /dev/null
    fi
    cnt=`expr $cnt + 2`
  done
fi

### GTM PACEMAKER START
if [ $code -eq 1 -o $code -eq 2 ];then
  echo "### GTM MASTER PACEMAKER START ###"
  ssh -t $gtm_master_slan "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (GTM:$gtm_master_slan)"; exit 1; }
  sleep 30
  t_cnt=0
  while true
  do
    ssh -t $gtm_master_slan "sudo crm_mon -A -1 | grep $check2 | grep $check3" > /dev/null
    if [ $? -eq 0 ];then
      break
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
  echo "### GTM SLAVE PACEMAKER START ###"
  ssh -t $gtm_slave_slan "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (GTM:$gtm_slave_slan)"; exit 1; }
elif [ $code -eq 3 ];then
  echo "### GTM(${gtm_slan[$g_num]}) PACEMAKER START ###"
  ssh -t ${gtm_slan[$g_num]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (GTM:${gtm_slan[$g_num]})"; exit 1; }
fi

### DATA PACEMAKER START
if [ $code -eq 1 -o $code -eq 4 ];then
  echo "### DATA GROUP PACEMAKER START ###"
  g_check=`expr $g_num \* 2 - 1`
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check -eq $cnt ];then
      ssh -t ${data_slan[$cnt]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (DATA:${data_slan[$cnt]})"; exit 1; }
      next=`expr $cnt + 1`
      ssh -t ${data_slan[$next]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (DATA:${data_slan[$next]})"; exit 1; }
    fi
    cnt=`expr $cnt + 2`
  done
  sleep 10
elif [ $code -eq 5 ];then
  cnt=`expr $g_num \* 2 - 2 + $d_code`
  echo "### DATA(${data_slan[$cnt]}) PACEMAKER START ###"
  ssh -t ${data_slan[$cnt]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (DATA:${data_slan[$cnt]})"; exit 1; }
  sleep 10
fi

## ARCHIVE SYNCHRONIZE
if [ $code -eq 1 -o $code -eq 4 ];then
  echo "### DATA GROUP ARCHIVE SYNCHRONIZE ###"
  sleep 30
  g_check=`expr $g_num \* 2 - 1`
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check -eq $cnt ];then
## Coordinator1, Datanode1 CHECK
      t_cnt=0
      while true
      do
        ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check5" > /dev/null
          if [ $? -eq 0 ];then
            ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check4" > /dev/null
            if [ $? -eq 0 ];then
              ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check5" > /dev/null
              if [ $? -eq 0 ];then
                ssh ${coord_master_slan[$cnt]} "scp -q ${coord_arch[$cnt]}/* ${coord_slave_slan[$cnt]}:${coord_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Coordinator1:${coord_master_slan[$cnt]})"; exit 1; }
                ssh ${dn_master_slan[$cnt]} "scp -q ${dn_arch[$cnt]}/* ${dn_slave_slan[$cnt]}:${dn_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Datanode1:${dn_master_slan[$cnt]})"; exit 1; }
                break
              fi
            fi
          fi
        fi
        if [ $t_cnt -ge $timeout ];then
          echo "### ERROR : TIME OUT"
          exit 1
        fi
        t_cnt=`expr $t_cnt + 1`
        sleep 10
      done
## Coordinator2, Datanode2 CHECK
      next=`expr $cnt + 1`
      t_cnt=0
      while true
      do
        ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check5" > /dev/null
          if [ $? -eq 0 ];then
            ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check4" > /dev/null
            if [ $? -eq 0 ];then
              ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check5" > /dev/null
              if [ $? -eq 0 ];then
                ssh ${coord_master_slan[$next]} "scp -q ${coord_arch[$next]}/* ${coord_slave_slan[$next]}:${coord_arch[$next]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Coordinator2:${coord_master_slan[$next]})"; exit 1; }
                ssh ${dn_master_slan[$next]} "scp -q ${dn_arch[$next]}/* ${dn_slave_slan[$next]}:${dn_arch[$next]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Datanode2:${dn_master_slan[$next]})"; exit 1; }
                break
              fi
            fi
          fi
        fi
        if [ $t_cnt -ge $timeout ];then
          echo "### ERROR : TIME OUT"
          exit 1
        fi
        t_cnt=`expr $t_cnt + 1`
        sleep 10
      done
    fi
    cnt=`expr $cnt + 2`
  done
elif [ $code -eq 5 ];then
  cnt=`expr $g_num \* 2 - 2 + $d_code`
  echo "### DATA(${data_slan[$cnt]}) ARCHIVE SYNCHRONIZE ###"
  if [ $d_code -eq 1 ];then
    m_cnt=`expr $cnt + 1`
  else
    m_cnt=`expr $cnt - 1`
  fi
  master_code=0
  ssh -t ${data_slan[$m_cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check4" > /dev/null
  if [ $? -eq 0 ];then
    ssh -t ${data_slan[$m_cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check4" > /dev/null
    if [ $? -eq 0 ];then
      ssh -t ${data_slan[$m_cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check4" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$m_cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          sleep 30
          ssh ${data_slan[$m_cnt]} "scp -q ${coord_arch[$m_cnt]}/* ${data_slan[$cnt]}:${coord_arch[$m_cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (${data_slan[$m_cnt]})"; exit 1; }
          ssh ${data_slan[$m_cnt]} "scp -q ${coord_arch[$cnt]}/* ${data_slan[$cnt]}:${coord_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (${data_slan[$m_cnt]})"; exit 1; }
          ssh ${data_slan[$m_cnt]} "scp -q ${dn_arch[$m_cnt]}/* ${data_slan[$cnt]}:${dn_arch[$m_cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (${data_slan[$m_cnt]})"; exit 1; }
          ssh ${data_slan[$m_cnt]} "scp -q ${dn_arch[$cnt]}/* ${data_slan[$cnt]}:${dn_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (${data_slan[$m_cnt]})"; exit 1; }
          master_code=1
        fi
      fi
    fi
  fi
fi
sleep 10

## START UP CHECK
if [ $code -eq 1 -o $code -eq 4 ];then
  echo "### START UP CHECK ###"
  g_check=`expr $g_num \* 2 - 1`
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check -eq $cnt ];then
      t_cnt=0
      while true
      do
        ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check6" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check6" > /dev/null
          if [ $? -eq 0 ];then
            ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check6" > /dev/null
            if [ $? -eq 0 ];then
              ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check6" > /dev/null
              if [ $? -eq 0 ];then
                break
              fi
            fi
          fi
        fi
        if [ $t_cnt -ge $timeout ];then
          echo "### ERROR : TIME OUT"
          exit 1
        fi
        t_cnt=`expr $t_cnt + 1`
        sleep 10
      done
      cnt=`expr $cnt + 2`
    fi
  done
elif [ $code -eq 5 ];then
  cnt=`expr $g_num \* 2 - 2 + $d_code`
  echo "### START UP CHECK ###"
  t_cnt=0
  while true
  do
    if [ $master_code -eq 1 ];then
      ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check6" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check6" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check6" > /dev/null
          if [ $? -eq 0 ];then
            ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check6" > /dev/null
            if [ $? -eq 0 ];then
              break
            fi
          fi
        fi
      fi
    else
      ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check4" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check4" > /dev/null
          if [ $? -eq 0 ];then
            ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check4" > /dev/null
            if [ $? -eq 0 ];then
              break
            fi
          fi
        fi
      fi
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
fi
echo "### FINISH ###"
