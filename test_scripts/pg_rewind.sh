#! /bin/sh

#-------------------------------------------
# Author: Amul Sul
#-------------------------------------------


# function to print label
function header(){
echo "------------------------"
echo $1
echo "------------------------"
#sleep 1
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------
TEST_ROUND=2
KEEP_SIZE='4GB'
PG_BENCH_SCALE=35

#postgres setting
DIR="/tmp/RM$RM"
PGDATA0="$DIR/Master"
PGDATA1="$DIR/Standby"
PGPORT0=65432
PGPORT1=65433
MASTERLOG="$DIR/Master.log"
STANDBYLOG="$DIR/Standby.log"
LOGFILE="$DIR/pg_rewind-$KEEP_SIZE-$PG_BENCH_SCALE-$TEST_ROUND.log"
DB='postgres'

unset PGPORT
unset PGDATA

echo "Postgresql is \"`which postgres`\""
killall -9 postgres

rm $MASTERLOG $STANDBYLOG $LOGFILE
rm -rf $PGDATA0 $PGDATA1

#--------------------------------
 header " CREATING MASTER   "
#--------------------------------

initdb $PGDATA0

# postgres.conf custom settings
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
wal_level = hot_standby
max_wal_senders = 6
hot_standby = on
log_min_messages = debug1
log_line_prefix = '[Master] %t'
log_checkpoints = on
log_error_verbosity = verbose
wal_log_hints = on
wal_keep_size=$KEEP_SIZE
EOF

# pg_hba.conf for replication
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all			trust
host   replication	all	127.0.0.1/32	trust
host   replication      all     ::1/128		trust
EOF

#--------------------------------
 header " STARTING UP MASTER   "
#--------------------------------

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG

#--------------------------------
 header " PGBENCH INIT ON MASTER   "
#--------------------------------

# inflate database a bit
pgbench -p $PGPORT0 -i -s $PG_BENCH_SCALE $DB

psql -p $PGPORT0 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as source_db_size;"

#--------------------------------
 header "     BASEBACKUP  "
#--------------------------------

pg_basebackup -v -P -cfast -p $PGPORT0 -F p -D $PGDATA1
#pg_basebackup -v -P  -p $PGPORT0 -F p -D $PGDATA1

# postgres.conf custom setting for standby
cat >> $PGDATA1/postgresql.conf <<EOF
port=$PGPORT1
log_min_messages = debug1
log_line_prefix = '[Standby] %t'
log_checkpoints = on
log_error_verbosity = verbose


#---------------------------------------------
# recovery setting for standby
#---------------------------------------------
recovery_target_timeline='latest'
primary_conninfo='port=$PGPORT0'
EOF
touch $PGDATA1/standby.signal

#--------------------------------
 header "   STARTING UP STANDBY  "
#--------------------------------

pg_ctl -D $PGDATA1 start -w -l $STANDBYLOG

#--------------------------------
 header "   PROMOTE STANDBY  "
#--------------------------------
echo ""
pg_ctl -D $PGDATA1 promote -l $STANDBYLOG
psql -p $PGPORT1 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_standby_db_size;"

#--------------------------------
 header "   MODIFY BOTH SERVER "
#--------------------------------

psql -p $PGPORT0 -d $DB -c "CREATE TABLE should_not_exit_table (id int);"
psql -p $PGPORT0 -d $DB -c "INSERT INTO should_not_exit_table VALUES (generate_series(1,10000));"

psql -p $PGPORT1 -d $DB -c "CREATE TABLE test_table (id int);"
psql -p $PGPORT1 -d $DB -c "INSERT INTO test_table VALUES (generate_series(1,100000));"
psql -p $PGPORT1 -d $DB -c "DROP TABLE test_table"
psql -p $PGPORT1 -d $DB -c "CREATE TABLE test_table2 (id int);"
psql -p $PGPORT1 -d $DB -c "INSERT INTO test_table2 VALUES (generate_series(1,100000));"

psql -p $PGPORT1 -d $DB -c "DROP TABLE test_table2"
psql -p $PGPORT1 -d $DB -c "CREATE TABLE test_table3 (id int);"
psql -p $PGPORT1 -d $DB -c "INSERT INTO test_table3 VALUES (generate_series(1,100000));"

psql -p $PGPORT1 -d $DB -c "DROP TABLE test_table3"
psql -p $PGPORT1 -d $DB -c "CREATE TABLE test_table4 (id int);"
psql -p $PGPORT1 -d $DB -c "INSERT INTO test_table4 VALUES (generate_series(1,100000));"

psql -p $PGPORT1 -d $DB -c "DROP TABLE test_table4"
psql -p $PGPORT1 -d $DB -c "CREATE TABLE test_table5 (id int);"
psql -p $PGPORT1 -d $DB -c "INSERT INTO test_table5 VALUES (generate_series(1,100000));"

psql -p $PGPORT0 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_master_db_size;"
psql -p $PGPORT1 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_standby_db_size;"

#--------------------------------
 header "   STOP ORIGINAL MASTER"
#--------------------------------

pg_ctl -D $PGDATA0 stop -w -l $MASTERLOG
#pg_ctl -D $PGDATA1 stop -w -l $STANDBYLOG

#--------------------------------
 header "   PGREWIND TARGET i.e. OLD MASTER"
#--------------------------------
echo "Writting pg_rewind log to : $LOGFILE"
echo "===========" >> $LOGFILE
echo "" >> $LOGFILE
psql -p $PGPORT1 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_standby_db_size;" >> $LOGFILE
echo "===========" >> $LOGFILE
echo "" >> $LOGFILE
pg_rewind -R -P --debug --target-pgdata=$PGDATA0 --source-server="host=localhost port=$PGPORT1 dbname=$DB" &>> $LOGFILE

#--------------------------------
 header "   RESTARTS SERVERS "
#--------------------------------
# Restore original postgres.conf custom settings since pg_rewind copies
# configuration  file as well
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
wal_level = hot_standby
max_wal_senders = 6
hot_standby = on
log_min_messages = debug1
log_line_prefix = '[Master] %t'
log_checkpoints = on
log_error_verbosity = verbose
wal_log_hints = on
wal_keep_size=$KEEP_SIZE
EOF

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG
#pg_ctl -D $PGDATA1 start -w -l $STANDBYLOG

psql -p $PGPORT0 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_master_db_size;"
psql -p $PGPORT1 -d $DB -c "SELECT pg_size_pretty(pg_database_size('$DB')) as orig_standby_db_size;"
#--------------------------------
 header "       DONE  "
#--------------------------------
