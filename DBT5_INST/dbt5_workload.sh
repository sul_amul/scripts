#!/bin/sh

#---------------------------------------
# Author: Amul Sul
#
#--------------------------------------

. $PWD/config.sh

export PATH=$DBT5INSTDIR/bin:$PATH


export EGENDIR=$EGENDIR
export DBT5DBNAME=$DBT5DBNAME
export DBT5PGDATA=$DBT5PGDATA
export DBT5TSDIR=$DBT5TSDIR


echo ""
echo "+----------------------------+"
echo "|    Running work load       |"
echo "+----------------------------+"
echo ""
sleep 5

dbt5-run-workload -a pgsql -c 1000 -d 300 -u 4 -n dbt5 -t 5 -w 5 -o /tmp/reuslt$1
#pg_dump dbt5 -s -f $PG_DUMP_EXP_DIR/schema_c$1_s$2.sql
#pg_dump dbt5 -a -f $PG_DUMP_EXP_DIR/data_c$1_s$2.sql
