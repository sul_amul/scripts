#!/bin/sh

#---------------------------------------
# Author: Amul Sul
#
# Running this Script:
# 	1. "sh creat_database.sh <nos.connection> <scale factor> new"
# 		only when you want to 
#		initialise new cluster.
#	 eg.
#	   $ sh creat_database.sh 1000 500 new		
#	
#	2. "sh creat_database.sh <nos.connection> <scale factor> "
#		want to use existing 
#		database cluster.
#	 eg.
#          $ sh creat_database.sh 1000 500
#--------------------------------------

. $PWD/config.sh

if [ $# -lt 2 ]; then
	echo ""
	echo "+----------------------------+"
	echo "| ERROR: Incorrect Arguments |"
	echo "+----------------------------+"
	echo ""
	exit 0
fi

if [ -n "$3" ]; then
	killall -9 postgres
	rm /tmp/.s.PGSQL.5432
	rm -rf $DBT5PGDATA
else	
	psql template1  -c "drop database dbt5"
fi

export PATH=$DBT5INSTDIR/bin:$PATH

echo ""
echo "+----------------------------+"
echo "|   Building Small Databse   |"
echo "+----------------------------+"
echo ""
sleep 5

export EGENDIR=$EGENDIR
export DBT5DBNAME=$DBT5DBNAME
export DBT5PGDATA=$DBT5PGDATA
export DBT5TSDIR=$DBT5TSDIR

dbt5-pgsql-build-db -c $1 -s $2 -w 1

echo ""
echo "+----------------------------+"
echo "|    Extracting DML and DDL  |"
echo "+----------------------------+"
echo ""
sleep 5

pg_dump dbt5 -s -f $PG_DUMP_EXP_DIR/schema_c$1_s$2.sql
pg_dump dbt5 -a -f $PG_DUMP_EXP_DIR/data_c$1_s$2.sql
