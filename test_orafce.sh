#!/bin/bash - 
#===============================================================================
#
#          FILE: test_orafce.sh
# 
#         USAGE: ./test_orafce.sh 
# 
#   DESCRIPTION: Test orafce modification on all supported PG version
#
#   REQUIREMENT: 1. postgres 8.4 to 9.3 install directory
#                2. $O_PATH in .bash_profile to mim required lib and bin path.
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 05/07/2014 10:40
#===============================================================================

#===============================================================================
#   GLOBAL DECLARATIONS
#===============================================================================
set -o nounset                              # Treat unset variables as an error

#-------------------------------------------------------------------------------
#   Need to be change w.r.t. envirment
#-------------------------------------------------------------------------------
INSTALL="/home/amul/Public/pg_install"
DATA="/tmp/data"
LOG="/tmp/work.log"
O_PATH=$PATH


#-------------------------------------------------------------------------------
#  SQLs want to test
#-------------------------------------------------------------------------------
rm /tmp/oracle_test_script.sql 
cat > /tmp/oracle_test_script.sql <<EOF
select to_date('2014-05-08 11:11:59.568931+05:30');
set orafce.nls_date_format="YYYY-MM-DD HH24:MI:SS";
select to_date('2014-05-08 11:11:59.568931+05:30');
EOF
#===============================================================================
#   FUNCTIONS 
#===============================================================================

#-------------------------------------------------------------------------------
#     Print output description 
#-------------------------------------------------------------------------------
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}	# ----------  end of function label  ----------

#-------------------------------------------------------------------------------
#     Create DB clustor and start postgres
#-------------------------------------------------------------------------------
function initdb_and_start(){
	rm -rf $DATA
	initdb -D $DATA &> /dev/null
	pg_ctl -D $DATA start -w -l $LOG
	sleep 5
}

#-------------------------------------------------------------------------------
#     Install orafce 
#-------------------------------------------------------------------------------
function install_and_check(){
	make clean
	make
	make install
	sleep 5
	make installcheck
	#--------------------------------
 	 label " Regression Diff"
	#--------------------------------
	cat regression.diffs
}

#-------------------------------------------------------------------------------
#     Install orafce function using sql  
#-------------------------------------------------------------------------------
function sql_check(){
	psql  postgres -f orafunc.sql
	psql -e postgres -f /tmp/oracle_test_script.sql 
	psql  postgres -f uninstall_orafunc.sql
}

#-------------------------------------------------------------------------------
#     Install orafce function using extension  
#-------------------------------------------------------------------------------
function extension_check(){
	#--------------------------------
	 label " Extension installation"
	#--------------------------------
	psql -e postgres -c 'CREATE EXTENSION orafce'
	psql -e postgres -f /tmp/oracle_test_script.sql
	psql -e postgres -c 'DROP EXTENSION orafce'
}

#-------------------------------------------------------------------------------
#     Run tests  
#-------------------------------------------------------------------------------
function run_test(){

	#--------------------------------
	 label " Initiat DB & Start it"
	#--------------------------------
	initdb_and_start

	#--------------------------------
	 label " Installcheck   "
	#--------------------------------
	install_and_check
	#--------------------------------
 	 label " script installation"
	#--------------------------------
	sql_check

}
#===============================================================================
#   MAIN SCRIPT
#===============================================================================

#--------------------------------
label "PG8.4"
#--------------------------------
killall -15 postgres
export PATH=$O_PATH
export PATH=$INSTALL/pg8.4/bin:$PATH
echo "Postgresql is \"`which postgres`\""

#--------------------------------
# Run Test For PG8.4
#--------------------------------
run_test

#--------------------------------
# Run Test For PG9.0-PG9.3
#--------------------------------

i=0

while [ $i -lt 5 ];
do

	#--------------------------------
	 label "PG9.$i"
	#--------------------------------
	killall -15 postgres
	export PATH=$O_PATH
	export PATH=$INSTALL/pg9."$i"/bin:$PATH
	echo "Postgresql is \"`which postgres`\""

	if [ "$i" -gt 0 ]
	then
		run_test
		extension_check
	else
		run_test
	fi

	((i++))
done

#===============================================================================
#   STATISTICS / CLEANUP
#===============================================================================
#--------------------------------
 label "FINISHED"
#--------------------------------
rm /tmp/oracle_test_script.sql
exit 0


