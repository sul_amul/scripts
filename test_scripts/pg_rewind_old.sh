#!/bin/bash

# Variables
PGDATA_SOURCE="/tmp/RM$RM/source_data"
PGDATA_TARGET="/tmp/RM$RM/target_data"
LOGFILE="/tmp/RM$RM/pg_rewind_test.log"
PORT_SOURCE=5433
PORT_TARGET=5434
DB='postgres'
USER="amul"
PGBENCH_SCALE=5

# Cleanup function
cleanup() {
    echo "Cleaning up..."
    pg_ctl -D "$PGDATA_SOURCE" -m immediate stop > /dev/null 2>&1
    pg_ctl -D "$PGDATA_TARGET" -m immediate stop > /dev/null 2>&1
    rm -rf "$PGDATA_SOURCE" "$PGDATA_TARGET"
}

# Trap to ensure cleanup on exit
trap cleanup EXIT

# Create and initialize clusters
echo "Initializing source and target clusters..."
initdb -D "$PGDATA_SOURCE" > /dev/null
initdb -D "$PGDATA_TARGET" > /dev/null

# Configure clusters for replication
echo "Configuring clusters..."
echo "wal_level = replica" >> "$PGDATA_SOURCE/postgresql.conf"
echo "archive_mode = on" >> "$PGDATA_SOURCE/postgresql.conf"
echo "archive_command = 'cp %p /tmp/RM$RM/archive/%f'" >> "$PGDATA_SOURCE/postgresql.conf"
echo "max_wal_senders = 1" >> "$PGDATA_SOURCE/postgresql.conf"
echo "listen_addresses = '*'" >> "$PGDATA_SOURCE/postgresql.conf"
echo "wal_keep_size = 128MB" >> "$PGDATA_SOURCE/postgresql.conf"
echo "port = $PORT_SOURCE" >> "$PGDATA_SOURCE/postgresql.conf"

echo "port = $PORT_TARGET" >> "$PGDATA_TARGET/postgresql.conf"

# Start clusters
echo "Starting clusters..."
pg_ctl -D "$PGDATA_SOURCE" -l "$LOGFILE" -o "--port=$PORT_SOURCE" start -w
pg_ctl -D "$PGDATA_TARGET" -l "$LOGFILE" -o "--port=$PORT_TARGET" start -w

# inflate database a bit
pgbench -p $PORT_SOURCE -U "$USER" -i -s $PGBENCH_SCALE $DB

# Create replication user
psql -p $PORT_SOURCE -d $DB -U "$USER" -c "CREATE ROLE repuser WITH REPLICATION LOGIN PASSWORD 'password';"

# Create a replication slot
psql -p $PORT_SOURCE -d $DB -U "$USER" -c "SELECT * FROM pg_create_physical_replication_slot('rep_slot');"

# Set up target as a replica
pg_basebackup -D "$PGDATA_TARGET" -h localhost -p $PORT_SOURCE -U repuser --wal-method=stream --slot=rep_slot

# Promote target cluster
echo "Promoting target cluster..."
pg_ctl -D "$PGDATA_TARGET" promote

# Modify source cluster
echo "Modifying source cluster..."
psql -p $PORT_SOURCE -d $DB -U "$USER" -c "CREATE TABLE test_table (id SERIAL PRIMARY KEY, name TEXT);"
psql -p $PORT_SOURCE -d $DB -U "$USER" -c "INSERT INTO test_table (name) VALUES ('pg_rewind test');"

psql -p $PORT_SOURCE -d $DB -U "$USER" -c "SELECT pg_size_pretty(pg_database_size('$DB')) as source_db_size;"
psql -p $PGDATA_TARGET -d $DB -U "$USER" -c "SELECT pg_size_pretty(pg_database_size('$DB')) as target_db_size;"

# Stop source and target clusters
pg_ctl -D "$PGDATA_SOURCE" stop
pg_ctl -D "$PGDATA_TARGET" stop

# Rewind target cluster
echo "Rewinding target cluster..."
pg_rewind -R -P --debug --target-pgdata="$PGDATA_TARGET" --source-server="host=localhost port=$PORT_SOURCE user=$USER dbname=$DB"

# Restart clusters
pg_ctl -D "$PGDATA_SOURCE" -l "$LOGFILE" -o "--port=$PORT_SOURCE" start -w
pg_ctl -D "$PGDATA_TARGET" -l "$LOGFILE" -o "--port=$PORT_TARGET" start -w

# Verify rewind results
echo "Verifying rewind results..."
psql -p $PORT_TARGET -d $DB -U "$USER" -c "SELECT * FROM test_table;"

echo "pg_rewind test completed. Check $LOGFILE for logs."

