#!/bin/sh
#===============================================================================
#
#          FILE: imhash_pgbench.sh
#
#         USAGE: ./imhash_pgbench.sh
#
#   DESCRIPTION: Compare TPS & generated WAL size of btree with imhash index
#
#        AUTHOR: Amul Sul (sulamul@yahoo.com)
#       CREATED: 27/08/2016 15:01
#===============================================================================

#===============================================================================
#   CONFIGURATION
#===============================================================================
RESULT="/home/amul.sul/RM38238/result.out"
NUM_OF_RUNS=3
DB="edb"

# Small run
#PGBENCH_INIT_OPT=" -i -s 10"
#PGBENCH_TEST_OPT=" -M prepared -n -N -T 30"

PGBENCH_INIT_OPT=" -i -s 500"
PGBENCH_TEST_OPT=" -M prepared -n -N -t 1000000 -c 128 -j 128"

BIN="/data/RM38238/pg_inst/bin"
DATA="/data/RM38238/data"

LOG="/data/RM38238/db.log"
LOG2="/data/RM38238/imhash_pgbench1.log"

rm $LOG &> /dev/null;
rm $LOG2 &> /dev/null;
#===============================================================================
#   FUNCTIONS
#===============================================================================
function change_pgconfig()
{
cat >> $DATA/postgresql.conf <<EOF
enable_bitmapscan = off
enable_seqscan = off
max_connections = 200
shared_buffers = 8GB
maintenance_work_mem = 4GB
synchronous_commit = off
checkpoint_timeout = 15min
checkpoint_completion_target = 0.9
log_line_prefix = '%t [%p] '
max_wal_size = 40GB
EOF
}

# Print output description
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}

function init_test_db()
{
	$BIN/pg_ctl -D $DATA stop -m i
	rm -rf $DATA

	#Drop server cached
	drop_caches 3

	$BIN/initdb -D $DATA 2>&1 > $LOG2
	change_pgconfig
	$BIN/pg_ctl -D $DATA start -w -l $LOG 2>&1 >> $LOG2
	$BIN/pgbench $PGBENCH_INIT_OPT -d $DB 2>&1 >> $LOG2
	
	#we are going to create second index on abalance
	$BIN/psql -e $DB -c 'alter table pgbench_accounts drop constraint pgbench_accounts_pkey;'
	$BIN/psql -d $DB -c 'UPDATE pgbench_accounts SET abalance = aid;' 2>&1 >> $LOG2
	$BIN/psql -d $DB -c 'VACUUM ANALYZE pgbench_accounts' 2>&1 >> $LOG2
	$BIN/psql -d $DB -c CHECKPOINT 2>&1 >> $LOG2
	$BIN/pg_ctl -D $DATA stop 2>&1 >> $LOG2

	#Drop server cached
	drop_caches 3

	$BIN/pg_ctl -c -D $DATA start -w -l $LOG 2>&1 >> $LOG2
	# reset stats info for all pgbench tables
	$BIN/psql -d $DB -c 'select pg_stat_reset_single_table_counters(oid) from pg_class where relname like 'pgbench_%';' 2>&1 >> $LOG2
	$BIN/psql -d $DB -c ANALYZE 2>&1 >> $LOG2
}

function init_test_imhash(){

	echo "--------------------------------"
	echo " init DB & Start it"
	echo "--------------------------------"
	init_test_db 

	echo "--------------------------------"
	echo " create imhash extension"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'CREATE EXTENSION imhash'

	echo "--------------------------------"
	echo " drop & recreate PK"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'create index pgbench_accounts_pkey on pgbench_accounts using imhash(aid);'
	$BIN/psql -e $DB -c 'create index pgbench_accounts_abalance_indx on pgbench_accounts using imhash(abalance);'
}

function init_test_btree(){

	echo "--------------------------------"
	echo " init DB & Start it"
	echo "--------------------------------"
	init_test_db

	echo "--------------------------------"
	echo " drop & recreate PK"
	echo "--------------------------------"
	$BIN/psql -e $DB -c 'create index pgbench_accounts_pkey on pgbench_accounts using btree(aid);'
	$BIN/psql -e $DB -c 'create index pgbench_accounts_abalance_indx on pgbench_accounts using btree(abalance);'
}
function run_pgbench()
{
	echo ""
	echo "stats before test:  "
	$BIN/psql $DB -c "select * from  pg_stat_all_tables where relname like 'pgbench_%'"

	echo ""
	printf "WAL lsn before test:  "
	$BIN/psql -t  $DB -c "SELECT pg_current_xlog_location()"

	$BIN/pgbench $DB $PGBENCH_TEST_OPT

	echo ""
	printf "WAL lsn After test:  "
	$BIN/psql -t  $DB -c "SELECT pg_current_xlog_location()"

	echo ""
	echo "stats After test:  "
	$BIN/psql $DB -c "select * from  pg_stat_all_tables where relname like 'pgbench_%'"
	$BIN/psql $DB -c "SELECT total_checkpoints,
				seconds_since_start / total_checkpoints / 60 AS minutes_between_checkpoints
			  FROM (SELECT EXTRACT(EPOCH FROM (now() - pg_postmaster_start_time())) AS seconds_since_start,
					(checkpoints_timed+checkpoints_req) AS total_checkpoints
				FROM pg_stat_bgwriter ) AS sub;"
}

function run_test()
{
	#--------------------------------
	label "--- IMHASH ---"
	#--------------------------------
	i=1

	while [ $i -le $NUM_OF_RUNS ];
	do
		#-----------------------------------
		label "IMHASH PGBENCH-TEST RUN : $i"
		#-----------------------------------
		init_test_imhash 2>&1 >> $LOG2
		XLOG_B=\'$($BIN/psql $DB -t -c "SELECT pg_current_xlog_location()")\'

		# Run pgbench
		run_pgbench

		# Calculated WAL size
		printf "Generated WAL Size:  "
		$BIN/psql -t $DB -c "SELECT
		pg_size_pretty(pg_xlog_location_diff(pg_current_xlog_location(),
											 trim($XLOG_B)::pg_lsn))"
		((i++))
	done
	#--------------------------------
	label "--- BTREE ---"
	#--------------------------------
	i=1

	while [ $i -le $NUM_OF_RUNS ];
	do
  	  	#--------------------------------
 		label "BTREE PGBENCH-TEST RUN : $i" 
		#--------------------------------
		init_test_btree 2>&1 >> $LOG2
		XLOG_B=\'$($BIN/psql $DB -t -c "SELECT pg_current_xlog_location()")\'

		# Run pgbench
		run_pgbench

		# Calculated WAL size
		printf "Generated WAL Size:  "
		$BIN/psql -t $DB -c "SELECT
		pg_size_pretty(pg_xlog_location_diff(pg_current_xlog_location(),
											 trim($XLOG_B)::pg_lsn))"
		((i++))
	done
	#---------------------------------------
	label "Server Configuration"
	#---------------------------------------
	$BIN/psql -d $DB -c "SELECT name, current_setting(name), SOURCE FROM pg_settings
WHERE SOURCE NOT IN ('default', 'override');"
	$BIN/pg_ctl -D $DATA stop -m i 2>&1 >> $LOG2

	#---------------------------------------
	label "FINISHED"
	#---------------------------------------
}

run_test 2>&1 > $RESULT
#tail -15 $DATA/postgresql.conf >> $RESULT
#rm /tmp/tmp_result.out
#run_test 2>&1 | tee /tmp/tmp_result.out
#cat /tmp/tmp_result.out | grep -v '^$\|client\|NOTICE\|aid\|bid\|delta\|tid'  > $RESULT
#---------------------------------------
label "RESULT STORED IN \"$RESULT\""
#---------------------------------------
