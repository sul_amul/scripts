#!/bin/sh
################################################################
# pgxc_exec.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

if [ $# -ne 2 ];then
  echo "Usage : ${0##*/} LIST 'COMMAND'"
  exit 1
fi

list=""
while read i
do
  if [ "$i" != "${i#'#'}" ];then
    continue
  elif [ -z "$i" ];then
    continue
  fi
  xclan=`echo $i | awk -F'[,]' '{print $1}'`
  list="$list $xclan"
done < $1

for i in $list
do
  echo "*****  $i  *****"
  ssh $i $2
done

exit 0
