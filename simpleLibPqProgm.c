/*-------------------------------------------------------------------------
 *  gcc temp.c -L/home/amul/pg_install/install/pg9.1.9N/lib/ -lpq -o testlibpq
 *-------------------------------------------------------------------------
 */
#include <stdio.h> 
#include <stdlib.h>
#include "/home/amul/pg_install/install/pg9.1.9N/include/libpq-fe.h"

int main()
{
    PGconn *conn;
    int i, num_records;
    char name[100];
    conn = PQconnectdb("dbname=postgres");
    /* Check to see how I did */
    if(PQstatus(conn) == CONNECTION_OK)
        printf("Connection succeeded.\n");
    else
    {
	printf("Error : Get Lost\n");
        /* Do something to deal with the error*/
	exit(1);
    }

	PGresult *res;
	res = PQexec(conn, "SELECT relname, reltype FROM pg_class Limit 10");

	if ((!res) || (PQresultStatus(res) != PGRES_TUPLES_OK))
    	{
	        fprintf(stderr, "No Boss, it now working\n");
       		PQclear(res);
		PQfinish(conn);
		exit(1);
    	}
		
    printf("%-40s%-20s\n", "Relname:", "RelType:");
    for(i=0;i<60;i++)
    {
        printf("-");
    }
    printf("\n");

    num_records = PQntuples(res);

    for(i = 0 ; i < num_records ; i++)
    {
        sprintf(name, "%-40s%-20s", PQgetvalue(res, i, 0), PQgetvalue(res, i, 1));
        printf("%s\n", name);
    }

    PQclear(res);

    PQfinish(conn);

    return 0;
}
