#!/bin/bash - 
#===============================================================================
#
#          FILE: edb-schedule.sh
# 
#         USAGE: ./edb-schedule.sh "schedule path" 
# 
#   DESCRIPTION: 1. copy EDBAS to /tmp/regression_tests
#		 2. run ./configure
#                3. make edb-check-quick 
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 11/05/2015 22:46
#===============================================================================

#===============================================================================
#   FUNCTIONS 
#===============================================================================
#-------------------------------------------------------------------------------
#     Print output description 
#-------------------------------------------------------------------------------
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}	# ----------  end of function label  ----------


#-------------------------------------------------------------------------------
# Copying code ....
#-------------------------------------------------------------------------------
label "Copy EDBAS to /tmp/regression_tests"
echo ""
rsync -vr --exclude=.git /home/amul/work/source/EDBAS/ /tmp/regression_tests
pushd .
cd /tmp/regression_tests
#-------------------------------------------------------------------------------
# run ./configure ....
#-------------------------------------------------------------------------------
label "configure EDBAS src"
echo ""
./configure --with-oci="$ORACLE_HOME/rdbms/public" --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend CFLAGS='-g -O0 -Wno-error=switch'
#sconfig_oci
make -j 4
#-------------------------------------------------------------------------------
# Run regression tests
#-------------------------------------------------------------------------------
label "Running quick edb regression tests"
cd src/test/regress/
make schedule SCHEDULE="$1"
popd
