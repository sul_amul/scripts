#!/bin/sh
################################################################
# pgxc_ha_init.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

data_num=`expr $de - $ds + 1`
group_num=`expr $data_num / 2`

### CREATE DEFINE FILE ###
file="./pgxc_ha_define.conf"

echo "### USER SET" > $file
echo "gtm_crm=\"/root/xcha/CRM_GTM.crm\"" >> $file
cnt=1
while [ $cnt -le $group_num ]
do
  echo "data_crm[$cnt]=\"/root/xcha/CRM_DATA.crm\"" >> $file
  cnt=`expr $cnt + 1`
done
cnt=1
while [ $cnt -le $data_num ]
do
  echo "coord_tmp[$cnt]=\"/home/postgres/data/tmpC1\"" >> $file
  cnt=`expr $cnt + 1`
  echo "coord_tmp[$cnt]=\"/home/postgres/data/tmpC2\"" >> $file
  cnt=`expr $cnt + 1`
done
cnt=1
while [ $cnt -le $data_num ]
do
  echo "dn_tmp[$cnt]=\"/home/postgres/data/tmpD1\"" >> $file
  cnt=`expr $cnt + 1`
  echo "dn_tmp[$cnt]=\"/home/postgres/data/tmpD2\"" >> $file
  cnt=`expr $cnt + 1`
done
echo "" >> $file

echo "### GTM S-LAN" >> $file
gtm_slan_1=`echo ${all[$gs]} | awk -F'[,]' '{print $2}'`
gtm_slan_2=`echo ${all[$ge]} | awk -F'[,]' '{print $2}'`
echo "gtm_master_slan=\"$gtm_slan_1\"" >> $file
echo "gtm_slave_slan=\"$gtm_slan_2\"" >> $file
echo "" >> $file

echo "### DATA S-LAN" >> $file
cnt=$cs
num=1
while [ $cnt -le $ce ]
do
  data_slan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  cnt=`expr $cnt + 1`
  data_slan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  echo "coord_master_slan[$num]=\"$data_slan_1\"" >> $file
  echo "coord_slave_slan[$num]=\"$data_slan_2\"" >> $file
  num=`expr $num + 1`
  echo "coord_master_slan[$num]=\"$data_slan_2\"" >> $file
  echo "coord_slave_slan[$num]=\"$data_slan_1\"" >> $file
  cnt=`expr $cnt + 1`
  num=`expr $num + 1`
done
cnt=$ds
num=1
while [ $cnt -le $de ]
do
  data_slan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  cnt=`expr $cnt + 1`
  data_slan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  echo "dn_master_slan[$num]=\"$data_slan_1\"" >> $file
  echo "dn_slave_slan[$num]=\"$data_slan_2\"" >> $file
  num=`expr $num + 1`
  echo "dn_master_slan[$num]=\"$data_slan_2\"" >> $file
  echo "dn_slave_slan[$num]=\"$data_slan_1\"" >> $file
  cnt=`expr $cnt + 1`
  num=`expr $num + 1`
done
echo "" >> $file

echo "### OTHER " >> $file
echo "gtm_slan[1]=\"$gtm_slan_1\"" >> $file
echo "gtm_slan[2]=\"$gtm_slan_2\"" >> $file
cnt=$cs
dnc=$ds
num=1
while [ $cnt -le $ce ]
do
  data_slan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  coord_arch_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
  dn_arch_1=`echo ${all[$dnc]} | awk -F'[,]' '{print $6}'`
  cnt=`expr $cnt + 1`
  dnc=`expr $dnc + 1`
  data_slan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  coord_arch_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
  dn_arch_2=`echo ${all[$dnc]} | awk -F'[,]' '{print $6}'`
  echo "data_slan[$num]=\"$data_slan_1\"" >> $file
  echo "coord_arch[$num]=\"$coord_arch_1\"" >> $file
  echo "dn_arch[$num]=\"$dn_arch_1\"" >> $file
  num=`expr $num + 1`
  echo "data_slan[$num]=\"$data_slan_2\"" >> $file
  echo "coord_arch[$num]=\"$coord_arch_2\"" >> $file
  echo "dn_arch[$num]=\"$dn_arch_2\"" >> $file
  cnt=`expr $cnt + 1`
  dnc=`expr $dnc + 1`
  num=`expr $num + 1`
done
echo "group_num=$group_num" >> $file
echo "data_num=$data_num" >> $file
