#!/bin/sh
################################################################
# pgxc_ha_monitor.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### PARAMETER CHECK ###
code=0
declare -i g_num=0
if [ $# -eq 0 ];then
  code=1
elif [ $# -eq 1 ];then
  case $1 in
  --gtm) code=2 ;;
  esac
elif [ $# -eq 2 ];then
  case $1 in
  --data) code=3 ;;
  esac
  g_num=$2
fi
if [ $code -eq 0 ];then
  echo "Usage : ${0##*/} [ --gtm | --data {n} ] (n : group number)"
  exit 1
fi

### FILE CHECK ###
ha_define_file="./pgxc_ha_define.conf"
if [ ! -f $ha_define_file ];then
  echo "### ERROR : NOT FOUND HA DEFINE FILE (file=$ha_define_file)"
  exit 1
fi
source $ha_define_file

### GROUP NUMBER CHECK ###
if [ $code -eq 3 ];then
  if [ $g_num -le 0 -o $g_num -gt $group_num ];then
    echo "### ERROR : INVALID GROUP NUMBER"
    exit 1
  fi
fi

### GTM MONITOR
if [ $code -eq 1 -o $code -eq 2 ];then
  echo "### GTM MONITOR ($gtm_master_slan, $gtm_slave_slan) ###"
  ssh -t $gtm_master_slan "sudo crm_mon -A -1"
fi

### DATA MONITOR
if [ $code -eq 1 -o $code -eq 3 ];then
  g_check=`expr $g_num \* 2 - 1`
  cnt=1
  while [ $cnt -le $data_num ]
  do
    if [ $code -eq 1 -o $g_check -eq $cnt ];then
      echo "### DATA MONITOR (${dn_master_slan[$cnt]}, ${dn_slave_slan[$cnt]}) ###"
      ssh -t ${dn_master_slan[$cnt]} "sudo crm_mon -A -1"
    fi
    cnt=`expr $cnt + 2`
  done
fi
