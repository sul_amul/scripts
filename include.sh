#!/bin/sh 
#===============================================================================
#
#          FILE: include.sh
# 
# 
#   DESCRIPTION:  
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 08/23/2016 12:19
#===============================================================================
source ~/.bash_profile 
#===============================================================================
#   FUNCTIONS 
#===============================================================================

#-------------------------------------------------------------------------------
#     Print output description 
#-------------------------------------------------------------------------------
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}	# ----------  end of function label  ----------

#-------------------------------------------------------------------------------
#     Change src/Makefile.global.in 
#-------------------------------------------------------------------------------
function change_makefile ()
{
	x='--temp-instance=.\/tmp_check'
	y='--temp-instance=..\/..\/..\/..\/..\/..\/..\/..\/..\/..\/tmp\/tmp_check'
	sed -i -e "s/$x/$y/g" src/Makefile.global.in
}	# ----------  end of function change_makefile  ----------
#-------------------------------------------------------------------------------
#     Undo Changes in src/Makefile.global.in 
#-------------------------------------------------------------------------------
function undo_changes_makefile ()
{
	git checkout src/Makefile.global.in
}	# ----------  end of function undo_changes_makefile  ----------
#-------------------------------------------------------------------------------
#     rung sconfig 
#-------------------------------------------------------------------------------
function run_edbas_configure ()
{
	label "Running configuration of source code"
	echo ""
	#make distclean
	make maintainer-clean
	change_makefile
	sconfig
#	undo_changes_makefile
}	# ----------  end of function edbas-configure  ----------
