#!/bin/bash - 
#===============================================================================
#
#          FILE: test.sh
# 
#         USAGE: ./test.sh 
# 
#   DESCRIPTION:  
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 03/01/2018 11:36
#===============================================================================
psql -d edb -l
for i in `seq 1 3`; do psql -d edb -qc "create role user${i} identified by 'foo'"; done
for j in `seq 1 3`
 do 
 	createdb db$j;
	for i in `seq 1 4`;
		do
			psql -qc "create table foo${i} (id int, name1 text, name2 text)" -d db$j;
			psql -qc "insert into foo${i} select i, md5(i::text), md5(i::text) from generate_series(1,10000) i" -d db$j;
		done;
 done
for i in `seq 1 3`; do for j in `seq 1 3`; do psql -d edb -qc "grant connect on database db$i to user$j"; done; done
psql -d edb -l
pg_dumpall -g > /tmp/globals.dmp
for i in `seq 1 3`; do pg_dump db$i > /tmp/db${i}.dmp; done
for i in `seq 1 3`; do pg_dump --create db$i > /tmp/db${i}_create.dmp; done
rm -rf /tmp/data
initdb -D /tmp/data
echo "port = 5447" >> /tmp/data/postgresql.conf
pg_ctl -w -D /tmp/data/ start
psql -d edb -qp5447 < /tmp/globals.dmp 
psql -d edb -qp5447 < /tmp/db1.dmp 
psql -d edb -qp5447 < /tmp/db2_create.dmp
psql -d edb -l -p5447
createdb -p5447 db3
psql -qp5447 -d db3 < /tmp/db3.dmp

