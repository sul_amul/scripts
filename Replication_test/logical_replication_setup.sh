#!/bin/bash -
rm -rf /tmp/RM5444/inst/data
rm -rf /tmp/RM5445/inst/data

PORT_PUB="5444"
PORT_SUB="5445"

INITDB="/tmp/RM5444/inst/bin/initdb"
PGCTL="/tmp/RM5444/inst/bin/pg_ctl -c "
PSQL="/tmp/RM5444/inst/bin/psql -d edb"

DATA_PUB="/tmp/RM5444/inst/data"
DATA_SUB="/tmp/RM5445/inst/data"


$INITDB -D $DATA_PUB > /dev/null
$INITDB -D $DATA_SUB > /dev/null

$PGCTL -o "-p $PORT_PUB" -D $DATA_PUB start > /dev/null
$PGCTL -o "-p $PORT_SUB" -D $DATA_SUB start > /dev/null

$PSQL -p $PORT_PUB -c 'ALTER SYSTEM SET wal_level TO logical;'
$PSQL -p $PORT_SUB -c 'ALTER SYSTEM SET wal_level TO logical;'

$PGCTL -D $DATA_PUB stop > /dev/null
$PGCTL -o "-p $PORT_PUB" -D $DATA_PUB start

$PGCTL -D $DATA_SUB stop > /dev/null
$PGCTL -o "-p $PORT_SUB" -D $DATA_SUB start

$PSQL -p $PORT_PUB -c 'CREATE PUBLICATION my_publication FOR ALL TABLES;'
$PSQL -p $PORT_PUB -c 'create table foo(i int);'
$PSQL -p $PORT_PUB -c 'insert into foo select n  from generate_series(1,10) n;'

$PSQL -p $PORT_SUB -c "create table foo(i int);"
$PSQL -p $PORT_SUB -c "CREATE SUBSCRIPTION my_subscription CONNECTION 'host=localhost port=5444 dbname=edb' PUBLICATION my_publication;"

# let the data to be stream
sleep 5

$PSQL -p $PORT_SUB -c "select count(i) from foo;"


# crashing query on plublisher
$PSQL -p $PORT_PUB -d edb -c "begin
insert into foo select n from generate_series(11,20) n;
 declare
	pragma autonomous_transaction;
 begin
	insert into foo select n from generate_series(21,30) n;
 end;
end;"

echo "------- After autonomos tranx on subscriber site--------"

# let the data to be stream
sleep 5
$PSQL -p $PORT_SUB -d edb -c "select count(i) from foo;"

#stop annoying logs
killall -9 edb-postgres
