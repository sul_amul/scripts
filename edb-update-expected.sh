#!/bin/sh

awk '$4 == "FAILED" { print $2 } $3 == "FAILED" { print $1 }' /tmp/regression_tests/src/test/regress/regression.out |
while read test; do
	echo cp results/$test.out expected/$test.out
	cp /tmp/regression_tests/src/test/regress/results/$test.out src/test/regress/expected/$test.out
done
