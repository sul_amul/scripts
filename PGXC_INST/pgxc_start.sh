#!/bin/sh
################################################################
# pgxc_start.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

echo "****************************"
echo "*** Start ******************"
echo "****************************"

### FILE NAME ###
gtmpid="gtm.pid"
proxypid="gtm_proxy.pid"
masterpid="postmaster.pid"

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

### PARAMETER CHECK ###
start_code=0
if [ $# -eq 0 ];then
  start_code=1
elif [ $# -eq 1 ];then
  start_code=2
  start_node=$1
elif [ $# -eq 2 ];then
  if [ $1 = "-C" ];then
    start_code=3
    ctl_file=$2
  fi
elif [ $# -eq 3 ];then
  if [ $2 = "-C" ];then
    start_code=4
    start_node=$1
    ctl_file=$3
  elif [ $1 = "-C" ];then
    start_code=4
    start_node=$3
    ctl_file=$2
  fi
fi
if [ $start_code -eq 0 ];then
  echo "Usage : ${0##*/} [ name ] [ -C ctlfile ]"
  exit 1
fi
if [ $start_code -eq 2 -o $start_code -eq 4 ];then
  no=-1
  cnt=0
  while [ $cnt -lt $allcnt ]
  do
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    if [ $name = $start_node ];then
      if [ $cnt -le $ge ];then
        no=0
      elif [ $cnt -le $pe ];then
        no=1
      elif [ $cnt -le $ce ];then
        no=2
      else
        no=3
      fi
      start_cnt=$cnt
      break
    fi
    cnt=`expr $cnt + 1`
  done
  if [ $no -eq -1 ];then
    echo "### ERROR : INVALID NAME"
    exit 1
  fi
  if [ $start_code -eq 4 -a $no -ne 0 ];then
    echo "### ERROR : CTLFILE ONLY GTM"
    exit 1
  fi
fi

### Initial start-up check ###
slan=`echo ${all[$cs]} | awk -F'[,]' '{print $2}'`
data=`echo ${all[$cs]} | awk -F'[,]' '{print $5}'`
newdata=`ssh $slan "echo $data"`
ret=`ssh $slan "pg_controldata $newdata | grep NextXID"`
if [ $? -ne 0 ];then
  echo "### ERROR : INITIAL SETTING YET"
  exit 1
else
  next=`echo $ret | awk -F'[/]' '{print $2}'`
  if [ $gxid -gt $next ];then
    echo "### ERROR : INITIAL STARTUP YET (gxid=$gxid , next=$next)"
    exit 1
  fi
fi

### COMPONENT NAME SPECIFIED ###
if [ $start_code -eq 2 -o $start_code -eq 4 ];then
  x_ctl=("gtm_ctl" "gtm_ctl" "pg_ctl" "pg_ctl")
  component=("gtm" "gtm_proxy" "coordinator" "datanode")
  pidfile=($gtmpid $proxypid $masterpid $masterpid)
  slan=`echo ${all[$start_cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$start_cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan(${component[$no]})  *****"
  newdata=`ssh $slan "echo $data"`
  opt_code=""
  if [ $start_code -eq 4 ];then
    opt_code="-C $ctl_file"
  fi
  ssh $slan "${x_ctl[$no]} -w start -Z ${component[$no]} -D $newdata $opt_code > /dev/null" || { echo "### ERROR : START UP (${component[$no]})"; exit 1; }
  pid=`ssh $slan "head -n 1 $newdata/${pidfile[$no]}"`
  ret=`ssh $slan "ps -p $pid -o pid="`
  if [ -z "$ret" ];then
    echo "### ERROR : START UP (${component[$no]})"
    exit 1
  fi
  exit 0
fi

### GTM ###
echo "***** GTM ******************"
cnt=$gs
while [ $cnt -le $ge ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  opt_code=""
  if [ $cnt -eq $gs -a $start_code -eq 3 ];then
    ssh $slan "gtm_ctl -w start -Z gtm -D $newdata -C $ctl_file > /dev/null" || { echo "### ERROR : START UP (GTM)"; exit 1; }
  else
    ssh $slan "gtm_ctl -w start -Z gtm -D $newdata > /dev/null" || { echo "### ERROR : START UP (GTM)"; exit 1; }
  fi
  pid=`ssh $slan "head -n 1 $newdata/$gtmpid"`
  ret=`ssh $slan "ps -p $pid -o pid="`
  if [ -z "$ret" ];then
    echo "### ERROR : START UP (GTM)"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

### GTM_proxy ###
echo "***** GTM_proxy ************"
cnt=$ps
while [ $cnt -le $pe ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "gtm_ctl -w start -Z gtm_proxy -D $newdata > /dev/null" || { echo "### ERROR : START UP (GTM_proxy)"; exit 1; }
  pid=`ssh $slan "head -n 1 $newdata/$proxypid"`
  ret=`ssh $slan "ps -p $pid -o pid="`
  if [ -z "$ret" ];then
    echo "### ERROR : START UP (GTM_proxy)"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

### Datanode ###
echo "***** Datanode *************"
cnt=$ds
while [ $cnt -le $de ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "pg_ctl -w start -Z datanode -D $newdata > /dev/null" || { echo "### ERROR : START UP (Datanode)"; exit 1; }
  pid=`ssh $slan "head -n 1 $newdata/$masterpid"`
  ret=`ssh $slan "ps -p $pid -o pid="`
  if [ -z "$ret" ];then
    echo "### ERROR : START UP (Datanode)"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

### Coordinator ###
echo "***** Coordinator **********"
cnt=$cs
while [ $cnt -le $ce ]
do
  slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  echo "*****  $slan  *****"
  newdata=`ssh $slan "echo $data"`
  ssh $slan "pg_ctl -w start -Z coordinator -D $newdata > /dev/null" || { echo "### ERROR : START UP (Coordinator)"; exit 1; }
  pid=`ssh $slan "head -n 1 $newdata/$masterpid"`
  ret=`ssh $slan "ps -p $pid -o pid="`
  if [ -z "$ret" ];then
    echo "### ERROR : START UP (Coordinator)"
    exit 1
  fi
  cnt=`expr $cnt + 1`
done

exit 0
