#!/bin/bash - 
#===============================================================================
#
#          FILE: test.sh
# 
#         USAGE: ./test.sh 
# 
#   DESCRIPTION:  
# 
#        AUTHOR: Amul Sul (sulamul@gmail.com) 
#       CREATED: 02/06/2018 16:01
#===============================================================================

# function to print label 
function header(){
echo "------------------------"
echo $1
echo "------------------------"
sleep 5
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------
#postgres.conf setting
PGDATA0="/tmp/Master"
PGDATA1="/tmp/Standby"
PGARC="/tmp/arc"
PGPORT0=5432
PGPORT1=5433
MASTERDATA="/tmp/master.out"
STANDBYDATA="/tmp/standby.out"
MASTERLOG="/tmp/Master.log"
STANDBYLOG="/tmp/Standby.log"
unset PGPORT
unset PGDATA

echo "Postgresql is \"`which postgres`\""
killall -9 postgres

rm $MASTERLOG $STANDBYLOG $MASTERDATA $STANDBYDATA
rm -rf $PGDATA0 $PGDATA1 $PGARC
mkdir $PGARC
#--------------------------------
 header " CREATING MASTER   "
#--------------------------------

initdb $PGDATA0

# postgres.conf
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
archive_mode = on
archive_command = 'cp %p $PGARC/%f'
wal_consistency_checking = 'all'
EOF

# pg_hba.conf for replication
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all			trust
host   replication	all	127.0.0.1/32	trust
host   replication      all     ::1/128		trust
EOF

#--------------------------------
 header " STARTING UP MASTER   "
#--------------------------------

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG

#--------------------------------
 header "  CREATE TEST DATA ON MASTER  "
#--------------------------------
psql -p $PGPORT0 -d postgres -c "CREATE TABLE foo (a int2, b text) partition by list (a);
								 CREATE TABLE foo0 PARTITION OF foo FOR VALUES IN (0);
								 CREATE TABLE foo1 PARTITION OF foo FOR VALUES IN (1);
								 CREATE TABLE foo2 PARTITION OF foo FOR VALUES IN (2);
								 INSERT INTO foo SELECT i%3, 'initial_part_'||  i%3  FROM generate_series(1,1000) i;"
psql -p $PGPORT0 -d postgres -c "CHECKPOINT"

#--------------------------------
 header "     BASEBACKUP  "
#--------------------------------

pg_basebackup -P -p $PGPORT0 -F p -X fetch -D $PGDATA1
# recovery.conf in standby
cat >> $PGDATA1/recovery.conf <<EOF
standby_mode = 'yes'
recovery_target_timeline='latest'
#primary_conninfo='host=localhost port=$PGPORT0'
restore_command='cp $PGARC/%f %p'
EOF
# postgres.conf in standby
cat >> $PGDATA1/postgresql.conf <<EOF
port=$PGPORT1
archive_mode = on
archive_command = 'cp %p $PGARC/%f'
wal_consistency_checking = 'all'
EOF
#--------------------------------
 header "   STARTING UP STANDBY  "
#--------------------------------

pg_ctl -D $PGDATA1 start -w -l $STANDBYLOG

#--------------------------------
 header " PERFORM UPDATE ON PARTITION KEY ON MASTER  "
#--------------------------------
for i in `seq 1 2`
do
	psql -p $PGPORT0 -d postgres -c "UPDATE foo SET a=((random()*100)::int)%3, b=b||'_u' WHERE a=((random()*100)::int)%3;"
done
psql -p $PGPORT0 -d postgres -c "CHECKPOINT"

# switch wal file to ensure archived as soon as possible
psql -p $PGPORT0 -d postgres -c "SELECT pg_switch_wal()"
sleep 5
#--------------------------------
 header "  MASTER WAL STATUS "
#--------------------------------
psql -x -p $PGPORT0 -d postgres -c "SELECT * FROM pg_stat_replication"
psql -p $PGPORT0 -d postgres -c "SELECT pg_is_in_recovery(),pg_current_wal_lsn()"

#--------------------------------
 header "  STANDBY WAL STATUS "
#--------------------------------
psql -p $PGPORT1 -d postgres -c "SELECT pg_is_in_recovery(), pg_last_wal_replay_lsn(), pg_last_xact_replay_timestamp()"

#--------------------------------
 header " COMPARE MASTER & STANDBY PARTITION DATA"
#--------------------------------
psql -p $PGPORT0 -d postgres -c 'SELECT * FROM foo ORDER BY 1,2' -o $MASTERDATA
psql -p $PGPORT1 -d postgres -c 'SELECT * FROM foo ORDER BY 1,2' -o $STANDBYDATA
cmp --silent $MASTERDATA $STANDBYDATA && echo '### SUCCESS: Data Is Identical! ###' || echo '### WARNING: Data Is Different! ###'

#--------------------------------
 header "  MASTER CONFIGURATION"
#--------------------------------
psql -p $PGPORT0 -d postgres -c "SELECT name, current_setting(name), SOURCE FROM pg_settings
WHERE SOURCE NOT IN ('default', 'override');"

#--------------------------------
 header "  STANDBY CONFIGURATION"
#--------------------------------
psql -p $PGPORT1 -d postgres -c "SELECT name, current_setting(name), SOURCE FROM pg_settings
WHERE SOURCE NOT IN ('default', 'override');"
