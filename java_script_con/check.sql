
\d+ public.sl_posting

WITH relpartbounds_dups AS ( SELECT inhparent, C.relpartbound, COUNT ( * ) FROM pg_class C, pg_inherits i WHERE C.oid = i.inhrelid AND pg_get_expr ( C.relpartbound, C.oid ) != 'DEFAULT' GROUP BY inhparent, C.relpartbound HAVING COUNT ( * ) >= 2 ) SELECT relname, relpartname, pg_get_expr ( C.relpartbound, C.oid ) FROM pg_class C, pg_inherits i, relpartbounds_dups WHERE 1 = 1 AND C.oid = i.inhrelid AND C.relpartbound = relpartbounds_dups.relpartbound;

