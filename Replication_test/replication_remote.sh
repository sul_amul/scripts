#! /bin/sh

#-------------------------------------------
# Author: Amul Sul
# Objective : Analyse streaming replication
#   The connection of Streaming Replication 
#   comes to be closed during  restoring the
#   dumped data into master server.
# Ref : Question ID:112968.	
#-------------------------------------------


# function to print label 
function header(){
echo "------------------------"
echo $1
echo "------------------------"
sleep 5
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------

# NOTE: Change the name of the DUMPDATA 

#created using  
#pg_dump  -U postgres -p $PGPORT0 -F t -f pgdump_data.tar
DUMPDATA="pgdump_data.tar"

#postgres setting
PWD=`pwd`
PGDATA0="$PWD/Master"
PGDATA1="$PWD/Standby"
PGARC="$PWD/arc"
PGPORT0=65432
PGPORT1=65433
PGHOST0='192.168.56.101'
PGHOST1='192.168.56.102'
MASTERLOG="$PWD/Master.log"
STANDBYLOG="$PWD/Standby.log"
unset PGPORT
unset PGDATA

ssh $PGHOST1 "unset PGPORT"
ssh $PGHOST1 "unset PGDATA"

#--------------------------------
 header " ON $PGHOST0 MASTER   "
#--------------------------------
echo "Postgresql is \"`which postgres`\""
killall -9 postgres

#--------------------------------
 header " ON $PGHOST1 STANDBY   "
#--------------------------------
ssh $PGHOST1 'echo "Postgresql is \"`which postgres`\""'
ssh $PGHOST1 'killall -9 postgres'

rm $MASTERLOG
rm -rf $PGDATA0 $PGARC
mkdir $PGARC

ssh $PGHOST1 "rm -rf  $PGDATA1 $STANDBYLOG"
ssh $PGHOST1 "mkdir -p $PWD"
#--------------------------------
 header " CREATING MASTER   "
#--------------------------------

initdb $PGDATA0

# postgres.conf custom settings
cat >> $PGDATA0/postgresql.conf <<EOF
listen_addresses = '*'
port=$PGPORT0
wal_level = hot_standby
archive_mode = on
archive_command = 'cp %p $PGARC/%f'
checkpoint_segments = 3 
max_wal_senders = 2
wal_keep_segments = 0 
hot_standby = on
max_standby_archive_delay = -1
max_standby_streaming_delay = -1
hot_standby_feedback = on
synchronous_standby_names = '*'
log_min_messages = debug1
log_line_prefix = '[Master] %t'
log_checkpoints = on
log_error_verbosity = verbose
log_lock_waits = on
track_activities = on
track_counts = on
track_functions = all

#------- some from cust config ---
tcp_keepalives_idle = 60							
tcp_keepalives_interval = 5		
tcp_keepalives_count = 5		
shared_buffers = 64MB			
shared_preload_libraries = 'auto_explain, pg_stat_statements'		
 
log_checkpoints = on
log_connections = on
log_disconnections = on
log_autovacuum_min_duration = 30s
datestyle = 'iso, mdy'
lc_messages = 'C'			
lc_monetary = 'C'			
lc_numeric = 'C'			
lc_time = 'C'	

default_text_search_config = 'pg_catalog.english'
restart_after_crash = off			
custom_variable_classes = 'auto_explain'		
auto_explain.log_min_duration = '5s'
auto_explain.log_analyze = off
auto_explain.log_verbose = off
auto_explain.log_buffers = off
auto_explain.log_format = text
auto_explain.log_nested_statements = off
EOF

# pg_hba.conf for replication
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all			trust
host   replication	all	127.0.0.1/32	trust
host   replication      all     ::1/128		trust
host   replication      all     0.0.0.0/0	trust
host   all	        all     0.0.0.0/0	trust
EOF

#--------------------------------
 header " STARTING UP MASTER   "
#--------------------------------

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG 

#--------------------------------
 header "     BASEBACKUP  "
#--------------------------------

ssh $PGHOST1 "pg_basebackup -P -h $PGHOST0 -p $PGPORT0 -F p -x -D $PGDATA1"

# recovery.conf in standby
ssh $PGHOST1 "
cat >> $PGDATA1/recovery.conf <<EOF
standby_mode = 'yes'
recovery_target_timeline='latest'
primary_conninfo='host=$PGHOST0 port=$PGPORT0'
restore_command='scp $PGHOST0:$PGARC/%f %p'
EOF"

ssh $PGHOST1 "
# postgres.conf custom setting for standby
cat >> $PGDATA1/postgresql.conf <<EOF
listen_addresses = '*'
port=$PGPORT1
wal_level = hot_standby
archive_mode = on
archive_command = 'scp %p $PGHOST0:$PGARC/%f'
max_wal_senders = 2
#wal_keep_segments = 32 
hot_standby = on
max_standby_archive_delay = -1
max_standby_streaming_delay = -1
hot_standby_feedback = on
log_min_messages = debug1
log_line_prefix = '[Standby] %t'
log_checkpoints = on
log_error_verbosity = verbose
log_lock_waits = on
track_activities = on
track_counts = on
track_functions = all

#--- some setting from cust config ---
tcp_keepalives_idle = 60		
tcp_keepalives_interval = 5		
tcp_keepalives_count = 5		
shared_buffers = 64MB			

shared_preload_libraries = 'auto_explain, pg_stat_statements'		
random_page_cost = 2.0			
log_rotation_size = 0		
log_checkpoints = on
log_connections = on
log_disconnections = on
log_autovacuum_min_duration = 30s	
datestyle = 'iso, mdy'
lc_messages = 'C'			
lc_monetary = 'C'			
lc_numeric = 'C'			
lc_time = 'C'				
default_text_search_config = 'pg_catalog.english'
restart_after_crash = off			
custom_variable_classes = 'auto_explain'		
auto_explain.log_min_duration = '5s'
auto_explain.log_analyze = off
auto_explain.log_verbose = off
auto_explain.log_buffers = off
auto_explain.log_format = text
auto_explain.log_nested_statements = off
EOF"

#--------------------------------
 header "   STARTING UP STANDBY  "
#--------------------------------

ssh $PGHOST1 "pg_ctl -D $PGDATA1 start -w -l $STANDBYLOG"

# Creating postgres user
createuser postgres -s -p $PGPORT0

#--------------------------------
 # header "   RUNNING PG-BENCH    "
#--------------------------------
#pgbench -i -s 350 -U postgres -p $PGPORT0

#--------------------------------
 header "   RUNNING PG-RESTORE "
#--------------------------------

pg_restore -v -d postgres -U postgres \
 -p $PGPORT0 -F t $DUMPDATA

#--------------------------------
 header "       DONE  "
#--------------------------------
