#!/bin/bash

if pgrep -x "lsyncd" > /dev/null 2>&1
then
	echo "lsyncd running already"
else
	echo "now starting lsyncd"
	lsyncd --delay 600 /home/amul/work/source/scripts/lsyncd.conf
fi
exit 0
