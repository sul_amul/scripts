#! /bin/sh

#-------------------------------------------
# Author: Amul Sul
#-------------------------------------------


# function to print label 
function header(){
echo "------------------------"
echo $1
echo "------------------------"
sleep 5
}

#--------------------------------
 header "   INITIAL SETTING   "
#--------------------------------

#postgres setting
PGDATA0="/tmp/Master"
PGDATA1="/tmp/Standby"
PGPORT0=65432
PGPORT1=65433
MASTERLOG="/tmp/Master.log"
STANDBYLOG="/tmp/Standby.log"
unset PGPORT
unset PGDATA

echo "Postgresql is \"`which postgres`\""
killall -9 postgres

rm $MASTERLOG $STANDBYLOG
rm -rf $PGDATA0 $PGDATA1

#--------------------------------
 header " CREATING MASTER   "
#--------------------------------

initdb $PGDATA0

# postgres.conf custom settings
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
wal_level = hot_standby
max_wal_senders = 6
hot_standby = on
log_min_messages = debug1
log_line_prefix = '[Master] %t'
log_checkpoints = on
log_error_verbosity = verbose
summarize_wal = on
EOF

# pg_hba.conf for replication
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all			trust
host   replication	all	127.0.0.1/32	trust
host   replication      all     ::1/128		trust
EOF

#--------------------------------
 header " STARTING UP MASTER   "
#--------------------------------

pg_ctl -D $PGDATA0 start -w -l $MASTERLOG

#--------------------------------
 header "     BASEBACKUP  "
#--------------------------------

pg_basebackup -v -P -p $PGPORT0 -F p -D $PGDATA1

# postgres.conf custom setting for standby
cat >> $PGDATA1/postgresql.conf <<EOF
port=$PGPORT1
log_min_messages = debug1
log_line_prefix = '[Standby] %t'
log_checkpoints = on
log_error_verbosity = verbose

max_standby_streaming_delay = 120s 

#---------------------------------------------
# recovery setting for standby
#---------------------------------------------
recovery_target_timeline='latest'
primary_conninfo='port=$PGPORT0'
EOF
touch $PGDATA1/standby.signal

#--------------------------------
 header "   STARTING UP STANDBY  "
#--------------------------------

pg_ctl -D $PGDATA1 start -w -l $STANDBYLOG

#--------------------------------
 header "       DONE  "
#--------------------------------
