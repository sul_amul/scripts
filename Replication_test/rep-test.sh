#!/bin/sh

PGDATA0="Master"
PGDATA1="Standby"
PGARC="arc"
mkdir $PGARC
PGPORT0=65432
PGPORT1=65433
echo "Postgresql is \"`which postgres`\""
pg_ctl stop -D $PGDATA0 -w
pg_ctl stop -D $PGDATA1 -w
rm -rf $PGDATA0 $PGDATA1 $PGARC/*
initdb $PGDATA0 --no-locale
cat >> $PGDATA0/postgresql.conf <<EOF
port=$PGPORT0
max_connections = 300
wal_level = hot_standby
checkpoint_segments = 300
checkpoint_timeout = 1h
archive_mode = on
archive_command = 'cp %p ../$PGARC/%f'
max_wal_senders = 3
hot_standby = on
logging_collector = on
log_line_prefix = '[Master] %t '
log_min_duration_statement = 20s
log_checkpoints = on
log_error_verbosity = verbose
log_lock_waits = on
track_activities = on
track_counts = on
track_functions = all
full_page_writes = off
EOF
cat >> $PGDATA0/pg_hba.conf <<EOF
local  replication      all                     trust
host   replication      all     127.0.0.1/32    trust
host   replication      all     ::1/128         trust
EOF
echo "#### Startup master ####"
pg_ctl -D $PGDATA0 -w  start

echo "#### Sleep for 5 seconds ####"
sleep 5

echo "#### basebackup ####"
pg_basebackup -p $PGPORT0 -F p -X s -D $PGDATA1
cat >> $PGDATA1/recovery.conf <<EOF
standby_mode = on
recovery_target_timeline='latest'
primary_conninfo='host=localhost port=$PGPORT0'
restore_command='cp ../$PGARC/%f %p'
EOF

echo "#### Sleep for 5 seconds ####"
sleep 5

cat >> $PGDATA1/postgresql.conf <<EOF
port=$PGPORT1
log_line_prefix = '[Standby] %t '
EOF

echo "#### Startup standby ####"
pg_ctl -D $PGDATA1 -w start

echo "#### Sleep for 5 seconds ####"
sleep 5

createdb testdb -p $PGPORT0
pgbench -i -s 50 -p $PGPORT0 testdb

echo "#### Sleep for 5 seconds ####"
sleep 5

echo "#### pgbench Start ####"
pgbench -n -c 100 -T 3600 -p $PGPORT0 testdb &
PID=$!

#for i in 1 2 3 4 5 6 7 8
i=1
while :
do
  PGB=`ps -ef|grep pgbench|grep -v grep|awk '{print $2}'`
  if [ "$PGB" = "" ]; then
    echo "#### pgbench stoped ####"
    break;
  else
    echo "#### pgbench running ####"
  fi
  echo "#### Loop $i ####"
  sleep 1000
  echo "#### Shutdown standby ####"
  pg_ctl -D $PGDATA1 -w stop -m f > /dev/null
  echo "#### Startup standby ####"
  pg_ctl -D $PGDATA1 -w start > /dev/null
  i=`expr $i + 1`
done

wait $PID

echo "#### Sleep for 180 seconds ####"
sleep 180
psql -p $PGPORT0 -x -c "select * from pg_stat_replication;" testdb

echo "#### Start Dump Master & Standby ####"
rm -f m.dmp s.dmp
pg_dump -o -p $PGPORT0 testdb > m.dmp
pg_dump -o -p $PGPORT1 testdb > s.dmp
diff m.dmp s.dmp > /dev/null
if [ $? = 0 ];then
  echo "#### Master-Standby Data same ####"
else
  echo "#### Master-Standby Data different  ####"
fi

exit 0

