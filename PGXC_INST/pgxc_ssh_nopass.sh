#!/bin/sh
################################################################
# pgxc_ssh_nopass.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

echo "****************************"
echo "*** SSH NO PASSWORD ********"
echo "****************************"

hostname=`hostname`
ipaddr=`grep $hostname /etc/hosts | awk '{print $1}'`
list=""
while read i
do
  if [ "$i" != "${i#'#'}" ];then
    continue
  elif [ -z "$i" ];then
    continue
  fi
  xclan=`echo $i | awk -F'[,]' '{print $1}'`
  if [ "$xclan" != "$hostname" -a "$xclan" != "$ipaddr" ];then
    list="$list $xclan"
  fi
done < ./all.list

mkdir ~/.ssh
ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ''
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
ctrl_pub=`cat ~/.ssh/id_rsa.pub`
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*

for i in $list
do
  echo "*****  $i  *****"
  ssh $i "mkdir ~/.ssh 2> /dev/null; ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ''; echo $ctrl_pub >> ~/.ssh/authorized_keys; chmod 700 ~/.ssh; chmod 600 ~/.ssh/*"
done

for i in $list
do
  ssh $i 'cat ~/.ssh/id_rsa.pub' >> ~/.ssh/authorized_keys
done

for i in $list
do
  scp ~/.ssh/authorized_keys $i:~/.ssh/.
done

exit 0
