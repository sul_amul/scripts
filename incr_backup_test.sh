#!/bin/bash -

DATA="/tmp/RM5444/inst/data"
BIN="/tmp/RM5444/inst/bin"
BACK_DIR="/tmp/RM5444/backup"
PGDATABASE="postgres"
PGPORT1="5444"

rm -rf $BACK_DIR
$BIN/pgbench -i -s 10 -p $PGPORT1 $PGDATABASE
$BIN/pg_basebackup -P -c fast -D $BACK_DIR/full -p $PGPORT1

$BIN/pgbench -n -P 1 -c 4 -T 10 -p $PGPORT1 $PGDATABASE

$BIN/pg_basebackup -P -c fast -D $BACK_DIR/incr1 -p $PGPORT1 --incremental $BACK_DIR/full/backup_manifest

$BIN/pgbench -n -P 1 -c 4 -T 10 -p $PGPORT1 $PGDATABASE

$BIN/pg_basebackup -P -c fast -D $BACK_DIR/incr2 -p $PGPORT1 --incremental $BACK_DIR/incr1/backup_manifest

$BIN/psql -d $PGDATABASE -c "DROP TABLE IF EXISTS foo; CREATE TABLE FOO(i int);"
$BIN/psql -d $PGDATABASE -c "insert into foo values(generate_series(1,100));"

$BIN/pg_basebackup -P -c fast -D $BACK_DIR/incr3 -p $PGPORT1 --incremental $BACK_DIR/incr2/backup_manifest

$BIN/psql -d $PGDATABASE -c "update foo set i = i/2 where i % 6 = 0;"

$BIN/pg_basebackup -P -c fast -D $BACK_DIR/incr4 -p $PGPORT1 --incremental $BACK_DIR/incr3/backup_manifest

