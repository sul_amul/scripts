#!/bin/sh
################################################################
# pgxc_install.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### Directory Check ###
# $1 : xclan
# $2 : directory name
# $3 : key name
dirname_check()
{
  work=$2
  ret=0
  while [ "$work" != "/" ]
  do
    ssh $1 "test -d $work"
    if [ $? -eq 0 ];then
      break
    fi
    work=`ssh $1 "dirname $work"`
  done
  tmpdir=`ssh $1 "mktemp -d $work/touch.XXXXXX 2> /dev/null"`
  if [ $? -ne 0 ];then
    echo "### ERROR : NO PERMISSION (xclan=$1 , $3=$2)"
    ret=1
  else
    ssh $1 "rmdir $tmpdir"
  fi
  return $ret
}

echo "****************************"
echo "*** Install Start **********"
echo "****************************"

### PARAMETER CHECK ###
install_code=0
sort_code=0
if [ $# -eq 0 ];then
  install_code=1
elif [ $# -eq 1 ];then
  case $1 in
  --check) install_code=2 ;;
  --noexec) install_code=3 ;;
  --deploy) install_code=4 ;;
  --setup) install_code=5 ;;
  --init) install_code=6 ;;
  --startup) install_code=7 ;;
  --status) install_code=8 ;;
  esac
elif [ $# -eq 2 ];then
  case $1 in
  --noexec) install_code=3 ;;
  --status) install_code=8 ;;
  esac
  case $2 in
  --sort) sort_code=1 ;;
  *) install_code=0 ;;
  esac
fi
if [ $install_code -eq 0 ];then
  echo "Usage : ${0##*/} [ --check | --noexec [--sort] | --deploy | --setup | --init | --startup | --status [--sort] ]"
  exit 1
fi

### FILE CHECK ###
readfile="./pgxc_readfile.sh"
if [ ! -f $readfile ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$readfile)"
  exit 1
fi
source $readfile

### FILE NAME ###
gtmfile="gtm.conf"
proxyfile="gtm_proxy.conf"
pgfile="postgresql.conf"
hbafile="pg_hba.conf"
incfile="include.conf"
gtmpid="gtm.pid"
proxypid="gtm_proxy.pid"
masterpid="postmaster.pid"
register="register.node"
xcctlfile="pgxc_ctl.conf"

##################################
### OPTION
###   install_code
###     1 : <ALL>
###     2 : --check
###     3 : --noexec
###     4 : --deploy
###     5 : --setup
###     6 : --init
###     7 : --startup
###     8 : --status
###   sort_code
###     0 : <nosort>
###     1 : --sort
### COMMON DATA
###   binary
###   ha
###   deploy[deploycnt] (1:xclan,2:slan,3:pghome)
### SETUP DATA
###   gxid
###   all[allcnt]                  (1:xclan,2:slan,3:name,4:port,5:data)
###     GTM         : all[gs - ge] (6:pghome,7:conf)
###     GTM_proxy   : all[ps - pe] (Same GTM)
###     Coordinator : all[cs - ce] (6:arch,7:xlog,8:vipxc,9:node,10:n_xclan,11:n_port,12:hba,13:conf,14:pooler,15:preferred)
###     Datanode    : all[ds - de] (6:arch,7:xlog,8:vipxc,9:node,10:n_xclan,11:n_port,12:hba,13:conf,14:primary)

####################
### Deploy check ###
####################
if [ $install_code -eq 1 -o $install_code -eq 2 -o $install_code -eq 4 ];then
  err=0
  cnt=0
  while [ $cnt -lt $deploycnt ]
  do

### SSH CHECK ###
    xclan=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $1}'`
    ssh $xclan "test 1 > /dev/null" 2> /dev/null
    if [ $? -ne 0 ];then
      echo "### ERROR : NOT CONNECTED SSH (xclan=$xclan)"
      exit 1
    fi
    slan=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $2}'`
    if [ $slan != $xclan ];then
      ssh $slan "test 1 > /dev/null" 2> /dev/null
      if [ $? -ne 0 ];then
        echo "### ERROR : NOT CONNECTED SSH (slan=$slan)"
        exit 1
      fi
    fi

### PGHOME CHECK ###
    pghome=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $3}'`
    dirname_check $xclan $pghome "pghome"
    if [ $? -ne 0 ];then
      err=1
    fi

    cnt=`expr $cnt + 1`
  done

  cnt=0
  while [ $cnt -lt $allcnt ]
  do

### SSH CHECK ###
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    ssh $xclan "test 1 > /dev/null" 2> /dev/null
    if [ $? -ne 0 ];then
      echo "### ERROR : NOT CONNECTED SSH (xclan=$xclan)"
      exit 1
    fi
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    if [ $slan != $xclan ];then
      ssh $slan "test 1 > /dev/null" 2> /dev/null
      if [ $? -ne 0 ];then
        echo "### ERROR : NOT CONNECTED SSH (slan=$slan)"
        exit 1
      fi
    fi

### DATA CHECK ###
    data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
    dirname_check $xclan $data "data"
    if [ $? -ne 0 ];then
      err=1
    fi

    if [ $cnt -ge $cs ];then
### ARCH CHECK ###
      arch=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
      dirname_check $xclan $arch "arch"
      if [ $? -ne 0 ];then
        err=1
      fi

### XLOG CHECK ###
      xlog=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
      dirname_check $xclan $xlog "xlog"
      if [ $? -ne 0 ];then
        err=1
      fi
    fi

    cnt=`expr $cnt + 1`
  done
  if [ $err -ne 0 ];then
    exit 1
  fi
fi

### CHECK MODE FINISH ###
if [ $install_code -eq 2 ];then
  echo "****************************"
  echo "*** Install Check OK *******"
  echo "****************************"
  exit 0
fi

###################
### New Dir Set ###
###################
cnt=0
while [ $cnt -lt $deploycnt ]
do
  xclan=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $1}'`
  pghome=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $3}'`
  newpghome[$cnt]=`ssh $xclan "echo $pghome"`
  cnt=`expr $cnt + 1`
done

cnt=0
while [ $cnt -lt $allcnt ]
do
  xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
  data=`echo ${all[$cnt]} | awk -F'[,]' '{print $5}'`
  newdata[$cnt]=`ssh $xclan "echo $data"`
  if [ $cnt -ge $cs ];then
    arch=`echo ${all[$cnt]} | awk -F'[,]' '{print $6}'`
    newarch[$cnt]=`ssh $xclan "echo $arch"`
    xlog=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    newxlog[$cnt]=`ssh $xclan "echo $xlog"`
  fi
  cnt=`expr $cnt + 1`
done

##############
### Deploy ###
##############
if [ $install_code -eq 1 -o $install_code -eq 4 ];then
  echo "****************************"
  echo "*** Deploy *****************"
  echo "****************************"
  cnt=0
  file=`echo $binary | awk -F / '{print $NF}'`
  while [ $cnt -lt $deploycnt ]
  do
    xclan=`echo ${deploy[$cnt]} | awk -F'[,]' '{print $1}'`
    pghome=${newpghome[$cnt]}
    echo "*****  $xclan  *****"
    ssh $xclan "mkdir -p $pghome" || { echo "### ERROR : MKDIR INSTALL (pghome=$pghome)"; exit 1; }
    scp $binary $xclan:$pghome/. > /dev/null || { echo "### ERROR : BINARY COPY"; exit 1; }
    ssh $xclan "cd $pghome; tar xvfz $file > /dev/null" || { echo "### ERROR : BINARY DEPLOY"; exit 1; }
    xchome=`ssh $xclan 'echo $XCHOME'`
    if [ $pghome != "$xchome" ];then
      ssh $xclan "echo 'export XCHOME=$pghome' >> ~/.bashrc" || { echo "### ERROR : SET BASHRC"; exit 1; }
      ssh $xclan "echo 'export PATH=\${XCHOME}/bin:\${PATH}' >> ~/.bashrc" || { echo "### ERROR : SET BASHRC"; exit 1; }
      ssh $xclan "echo 'export LD_LIBRARY_PATH=\${XCHOME}/lib:\${LD_LIBRARY_PATH}' >> ~/.bashrc" || { echo "### ERROR : SET BASHRC"; exit 1; }
    fi
    cnt=`expr $cnt + 1`
  done
fi

#############
### Setup ###
#############
if [ $install_code -eq 1 -o $install_code -eq 5 -o $install_code -eq 6 ];then
  echo "****************************"
  echo "*** Make Directory *********"
  echo "****************************"
  cnt=0
  while [ $cnt -lt $allcnt ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    data=${newdata[$cnt]}
    echo "*****  $xclan  *****"
    ssh $xclan "mkdir -p $data" || { echo "### ERROR : MKDIR DATA ($data)"; exit 1; }
    if [ $cnt -ge $cs ];then
      arch=${newarch[$cnt]}
      ssh $xclan "mkdir -p $arch" || { echo "### ERROR : MKDIR ARCH ($arch)"; exit 1; }
      xlog=${newxlog[$cnt]}
      ssh $xclan "mkdir -p $xlog" || { echo "### ERROR : MKDIR XLOG ($xlog)"; exit 1; }
    fi
    cnt=`expr $cnt + 1`
  done

  echo "****************************"
  echo "*** Initialize DB **********"
  echo "****************************"
  cnt=$gs
  while [ $cnt -le $ge ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    data=${newdata[$cnt]}
    ssh $xclan "initgtm -Z gtm -D $data > /dev/null" || { echo "### ERROR : INITIALIZE (GTM)"; exit 1; }
    cnt=`expr $cnt + 1`
  done

  cnt=$ps
  while [ $cnt -le $pe ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    data=${newdata[$cnt]}
    ssh $xclan "initgtm -Z gtm_proxy -D $data > /dev/null" || { echo "### ERROR : INITIALIZE (GTM_proxy)"; exit 1; }
    cnt=`expr $cnt + 1`
  done

  cnt=$cs
  while [ $cnt -le $de ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    data=${newdata[$cnt]}
    xlog=${newxlog[$cnt]}
    echo "*****  $xclan  *****"
    ssh $xclan "initdb -D $data -X $xlog --nodename $name > /dev/null" || { echo "### ERROR : INITIALIZE DB ($name)"; exit 1; }
    cnt=`expr $cnt + 1`
  done

  echo "****************************"
  echo "*** Configuration File *****"
  echo "****************************"

### GTM ###
  cnt=$gs
  while [ $cnt -le $ge ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    conf=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    file="$data/$gtmfile"
    echo "*****  $xclan  *****"
    if [ $conf ];then
      scp $conf $xclan:$data/$incfile > /dev/null || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"include './$incfile'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
    ssh $xclan "echo \"listen_addresses = '*'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"nodename = '$name'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"port = $port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    if [ $cnt -eq $gs ];then
      master_xclan=$xclan
      master_port=$port
    fi
### GTM Master/Slave information ###
    if [ $gs -ne $ge ];then
      if [ $cnt -eq $gs ];then
        ssh $xclan "echo \"#startup = STANDBY\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
        slave_xclan=`echo ${all[$ge]} | awk -F'[,]' '{print $1}'`
        slave_port=`echo ${all[$ge]} | awk -F'[,]' '{print $4}'`
        ssh $xclan "echo \"active_host = '$slave_xclan'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
        ssh $xclan "echo \"active_port = $slave_port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      else
        ssh $xclan "echo \"startup = STANDBY\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
        ssh $xclan "echo \"active_host = '$master_xclan'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
        ssh $xclan "echo \"active_port = $master_port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      fi
    fi
    cnt=`expr $cnt + 1`
  done

### GTM_proxy ###
  cnt=$ps
  while [ $cnt -le $pe ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    conf=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    file="$data/$proxyfile"
    echo "*****  $xclan  *****"
    if [ $conf ];then
      scp $conf $xclan:$data/$incfile > /dev/null || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"include './$incfile'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
    ssh $xclan "echo \"listen_addresses = '*'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"nodename = '$name'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"port = $port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"gtm_host = '$master_xclan'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"gtm_port = $master_port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    cnt=`expr $cnt + 1`
  done

### Coordinator , Datanode ###
  cnt=$cs
  while [ $cnt -le $de ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    node=`echo ${all[$cnt]} | awk -F'[,]' '{print $9}'`
    n_xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $10}'`
    n_port=`echo ${all[$cnt]} | awk -F'[,]' '{print $11}'`
    hba=`echo ${all[$cnt]} | awk -F'[,]' '{print $12}'`
    conf=`echo ${all[$cnt]} | awk -F'[,]' '{print $13}'`
    file="$data/$pgfile"
    echo "*****  $xclan  *****"
    if [ $conf ];then
      scp $conf $xclan:$data/$incfile > /dev/null || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"include './$incfile'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
    ssh $xclan "echo \"listen_addresses = '*'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    ssh $xclan "echo \"port = $port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    if [ "$node" != "localhost" ];then
      ssh $xclan "echo \"gtm_host = '$n_xclan'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
    ssh $xclan "echo \"gtm_port = $n_port\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    if [ $cnt -lt $ds ];then
      pooler=`echo ${all[$cnt]} | awk -F'[,]' '{print $14}'`
      ssh $xclan "echo \"pooler_port = $pooler\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
### HA information ###
    if [ $ha = "on" ];then
      arch=${newarch[$cnt]}
      ssh $xclan "echo \"wal_level = hot_standby\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"archive_mode = on\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"archive_command = 'cp %p $arch/%f'\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"max_wal_senders = 5\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"wal_keep_segments = 32\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"hot_standby = on\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
      ssh $xclan "echo \"restart_after_crash = off\" >> $file" || { echo "### ERROR : SET CONF ($file)"; exit 1; }
    fi
    file="$data/$hbafile"
    tmpfile=`ssh $xclan "mktemp $data/touch.XXXXXX 2> /dev/null"`
    if [ $? -ne 0 ];then
      echo "### ERROR : SET HBA ($file)"
      exit 1
    else
### To add to the end ###
      scp $hba $xclan:$tmpfile > /dev/null || { echo "### ERROR : SET HBA ($file)"; exit 1; }
      ssh $xclan "cat $tmpfile >> $file" || { echo "### ERROR : SET HBA ($file)"; exit 1; }
      ssh $xclan "rm $tmpfile" || { echo "### ERROR : SET HBA ($file)"; exit 1; }
    fi
    cnt=`expr $cnt + 1`
  done
fi

if [ $install_code -eq 1 -o $install_code -eq 5 -o $install_code -eq 7 ];then
  echo "****************************"
  echo "*** Start Up ***************"
  echo "****************************"

### Initial start-up check ###
  xclan=`echo ${all[$cs]} | awk -F'[,]' '{print $1}'`
  data=${newdata[$cs]}
  ret=`ssh $xclan "pg_controldata $data | grep NextXID"`
  if [ $? -ne 0 ];then
    echo "### ERROR : INITIAL SETTING YET"
    exit 1
  else
    next=`echo $ret | awk -F'[/]' '{print $2}'`
    if [ $gxid -lt $next ];then
      echo "### ERROR : INITIAL STARTUP ALREADY. OR, GXID IS TOO SMALL (gxid=$gxid , next=$next)"
      exit 1
    fi
  fi

### GTM ###
  echo "***** GTM ******************"
  cnt=$gs
  while [ $cnt -le $ge ]
  do
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data=${newdata[$cnt]}
    echo "*****  $slan  *****"
    if [ $cnt -eq 0 ];then
      ssh $slan "gtm_ctl -w start -Z gtm -D $data -o '-x $gxid' > /dev/null" || { echo "### ERROR : START UP (GTM)"; exit 1; }
    else
      ssh $slan "gtm_ctl -w start -Z gtm -D $data > /dev/null" || { echo "### ERROR : START UP (GTM)"; exit 1; }
    fi
    pid=`ssh $slan "head -n 1 $data/$gtmpid"`
    ret=`ssh $slan "ps -p $pid -o pid="`
    if [ -z "$ret" ];then
      echo "### ERROR : START UP (GTM)"
      exit 1
    fi
    cnt=`expr $cnt + 1`
  done

### GTM_proxy ###
  echo "***** GTM_proxy ************"
  cnt=$ps
  while [ $cnt -le $pe ]
  do
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data=${newdata[$cnt]}
    echo "*****  $slan  *****"
    ssh $slan "gtm_ctl -w start -Z gtm_proxy -D $data > /dev/null" || { echo "### ERROR : START UP (GTM_proxy)"; exit 1; }
    pid=`ssh $slan "head -n 1 $data/$proxypid"`
    ret=`ssh $slan "ps -p $pid -o pid="`
    if [ -z "$ret" ];then
      echo "### ERROR : START UP (GTM_proxy)"
      exit 1
    fi
    cnt=`expr $cnt + 1`
  done

### Datanode ###
  echo "***** Datanode *************"
  cnt=$ds
  while [ $cnt -le $de ]
  do
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data=${newdata[$cnt]}
    echo "*****  $slan  *****"
    ssh $slan "pg_ctl -w start -Z datanode -D $data > /dev/null" || { echo "### ERROR : START UP (Datanode)"; exit 1; }
    pid=`ssh $slan "head -n 1 $data/$masterpid"`
    ret=`ssh $slan "ps -p $pid -o pid="`
    if [ -z "$ret" ];then
      echo "### ERROR : START UP (Datanode)"
      exit 1
    fi
    cnt=`expr $cnt + 1`
  done

### Coordinator ###
  echo "***** Coordinator **********"
  cnt=$cs
  while [ $cnt -le $ce ]
  do
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    data=${newdata[$cnt]}
    echo "*****  $slan  *****"
    ssh $slan "pg_ctl -w start -Z coordinator -D $data > /dev/null" || { echo "### ERROR : START UP (Coordinator)"; exit 1; }
    pid=`ssh $slan "head -n 1 $data/$masterpid"`
    ret=`ssh $slan "ps -p $pid -o pid="`
    if [ -z "$ret" ];then
      echo "### ERROR : START UP (Coordinator)"
      exit 1
    fi
    cnt=`expr $cnt + 1`
  done

  echo "****************************"
  echo "*** Node Infomation ********"
  echo "****************************"
  i=$cs
  while [ $i -le $ce ]
  do
    xclan=`echo ${all[$i]} | awk -F'[,]' '{print $1}'`
    port=`echo ${all[$i]} | awk -F'[,]' '{print $4}'`
    preferred=`echo ${all[$i]} | awk -F'[,]' '{print $15}'`
    echo "*****  $xclan  *****"
    j=$cs
    while [ $j -le $ce ]
    do
      cn_vipxc=`echo ${all[$j]} | awk -F'[,]' '{print $8}'`
      if [ -z "$cn_vipxc" ];then
        cn_vipxc=`echo ${all[$j]} | awk -F'[,]' '{print $1}'`
      fi
      cn_name=`echo ${all[$j]} | awk -F'[,]' '{print $3}'`
      cn_port=`echo ${all[$j]} | awk -F'[,]' '{print $4}'`
      if [ $i -ne $j ]; then
        ssh $xclan "psql -p $port postgres -c \"CREATE NODE $cn_name WITH (TYPE='coordinator', PORT=$cn_port, HOST='$cn_vipxc');\"" > /dev/null || { echo "### ERROR : CREATE NODE ($cn_name)"; exit 1; }
      fi
      j=`expr $j + 1`
    done
    j=$ds
    while [ $j -le $de ]
    do
      dn_vipxc=`echo ${all[$j]} | awk -F'[,]' '{print $8}'`
      if [ -z "$dn_vipxc" ];then
        dn_vipxc=`echo ${all[$j]} | awk -F'[,]' '{print $1}'`
      fi
      dn_name=`echo ${all[$j]} | awk -F'[,]' '{print $3}'`
      dn_port=`echo ${all[$j]} | awk -F'[,]' '{print $4}'`
      dn_primary=`echo ${all[$j]} | awk -F'[,]' '{print $14}'`
### PREFERRED CHECK ###
      dn_preferred="FALSE"
      if [ $preferred ];then
        cnt=1
        while true
        do
          work_name=`echo $preferred | awk -v id=$cnt -F'[/]' '{print $id}'`
          if [ -z "$work_name" ];then
            break
          fi
          if [ "$dn_name" = "$work_name" ];then
            dn_preferred="TRUE"
            break
          fi
          cnt=`expr $cnt + 1`
        done
      fi

      ssh $xclan "psql -p $port postgres -c \"CREATE NODE $dn_name WITH (TYPE='datanode', PORT=$dn_port, HOST='$dn_vipxc', PRIMARY=$dn_primary, PREFERRED=$dn_preferred);\"" > /dev/null || { echo "### ERROR : CREATE NODE ($dn_name)"; exit 1; }
      j=`expr $j + 1`
    done
    ssh $xclan "psql -p $port postgres -c \"SELECT pgxc_pool_reload();\"" > /dev/null || { echo "### ERROR : POOL RELOAD"; exit 1; }
    i=`expr $i + 1`
  done

### DATABASE BACKUP ###
  if [ $ha = "on" ];then
    echo "****************************"
    echo "*** Database Backup ********"
    echo "****************************"
    cnt=$cs
    while [ $cnt -le $de ]
    do
      xclan_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
      name_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
      port_1=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
      data_1=${newdata[$cnt]}
      arch_1=${newarch[$cnt]}
      xlog_1=${newxlog[$cnt]}
      cnt=`expr $cnt + 1`
      xclan_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
      name_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
      port_2=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
      data_2=${newdata[$cnt]}
      arch_2=${newarch[$cnt]}
      xlog_2=${newxlog[$cnt]}
      echo "*****  $xclan_1 <---> $xclan_2  *****"
      ssh $xclan_1 "pg_basebackup -h $xclan_2 -p $port_2 -D $data_2 --xlog --checkpoint=fast --progress" || { echo "### ERROR : DATABASE BACKUP (1)"; exit 1; }
      ssh $xclan_2 "pg_basebackup -h $xclan_1 -p $port_1 -D $data_1 --xlog --checkpoint=fast --progress" || { echo "### ERROR : DATABASE BACKUP (2)"; exit 1; }
      ssh $xclan_1 "mv $data_2/pg_xlog $xlog_2; ln -s $xlog_2 $data_2/pg_xlog" || { echo "### ERROR : LINK XLOG (1)"; exit 1; }
      ssh $xclan_2 "mv $data_1/pg_xlog $xlog_1; ln -s $xlog_1 $data_1/pg_xlog" || { echo "### ERROR : LINK XLOG (2)"; exit 1; }
      ssh $xclan_1 "mkdir -p $arch_2" || { echo "### ERROR : MKDIR ARCH (1)"; exit 1; }
      ssh $xclan_2 "mkdir -p $arch_1" || { echo "### ERROR : MKDIR ARCH (2)"; exit 1; }
      cnt=`expr $cnt + 1`
    done
    ./pgxc_stop.sh
    slan=`echo ${all[$gs]} | awk -F'[,]' '{print $2}'`
    data=${newdata[$gs]}
    ssh $slan "gtm_ctl -w start -Z gtm -D $data > /dev/null" || { echo "### ERROR : RE START (GTM)"; exit 1; }
    ssh $slan "gtm_ctl -w stop -Z gtm -D $data > /dev/null" || { echo "### ERROR : RE STOP (GTM)"; exit 1; }

### PGXC_CTL CONF FILE ###
    echo "****************************"
    echo "*** pgxc_ctl.conf **********"
    echo "****************************"
    file="~/pgxc_ctl/$xcctlfile"
    xclan=`echo ${all[$gs]} | awk -F'[,]' '{print $1}'`
    pghome=`echo ${all[$gs]} | awk -F'[,]' '{print $6}'`
    user=`ssh $xclan 'echo $USER'`
    echo "*****  $xclan  *****"
    ssh $xclan "mkdir -p ~/pgxc_ctl" || { echo "### ERROR : MKDIR PGXC_CTL"; exit 1; }
    ssh $xclan "echo \"#!/bin/bash\" > $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"pgxcInstallDir=$pghome\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"pgxcOwner=$user\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"pgxcUser=$user\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"tmpDir=/tmp\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"localTmpDir=/tmp\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"#---- GTM ----------------------------------------------------------------------\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    g_name=`echo ${all[$gs]} | awk -F'[,]' '{print $3}'`
    g_port=`echo ${all[$gs]} | awk -F'[,]' '{print $4}'`
    g_data=`echo ${all[$gs]} | awk -F'[,]' '{print $5}'`
    ssh $xclan "echo \"gtmName=$g_name\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"gtmMasterServer=$master_xclan\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"gtmMasterPort=$g_port\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"gtmMasterDir=$g_data\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"gtmSlave=n\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"#---- GTM Proxy ----------------------------------------------------------------\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"gtmProxy=y\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    p_xclan_list="gtmProxyServers=("
    p_name_list="gtmProxyNames=("
    p_port_list="gtmProxyPorts=("
    p_data_list="gtmProxyDirs=("
    p_cnt=$ps
    while [ $p_cnt -le $pe ]
    do
      p_xclan=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $1}'`
      p_name=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $3}'`
      p_port=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $4}'`
      p_data=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $5}'`
      p_xclan_list=${p_xclan_list}${p_xclan}
      p_name_list=${p_name_list}${p_name}
      p_port_list=${p_port_list}${p_port}
      p_data_list=${p_data_list}${p_data}
      if [ $p_cnt -ne $pe ]; then
        p_xclan_list=${p_xclan_list}" "
        p_name_list=${p_name_list}" "
        p_port_list=${p_port_list}" "
        p_data_list=${p_data_list}" "
      fi
      p_cnt=`expr $p_cnt + 1`
    done
    p_xclan_list=${p_xclan_list}")"
    p_name_list=${p_name_list}")"
    p_port_list=${p_port_list}")"
    p_data_list=${p_data_list}")"
    ssh $xclan "echo \"$p_name_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$p_xclan_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$p_port_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$p_data_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"#---- Coordinators -------------------------------------------------------------\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    c_xclan_list="coordMasterServers=("
    c_name_list="coordNames=("
    c_port_list="coordPorts=("
    c_data_list="coordMasterDirs=("
    c_pooler_list="poolerPorts=("
    cs_xclan_list="coordSlaveServers=("
    cs_data_list="coordSlaveDirs=("
    c_wal_list="coordMaxWALSenders=("
    c_arch_list="coordArchLogDirs=("
    c_wal=5
    c_cnt=$cs
    while [ $c_cnt -le $ce ]
    do
      c_xclan=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $1}'`
      c_name=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $3}'`
      c_port=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $4}'`
      c_data=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $5}'`
      c_arch=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $6}'`
      c_pooler=`echo ${all[$c_cnt]} | awk -F'[,]' '{print $14}'`
      c_xclan_list=${c_xclan_list}${c_xclan}
      c_name_list=${c_name_list}${c_name}
      c_port_list=${c_port_list}${c_port}
      c_data_list=${c_data_list}${c_data}
      cs_data_list=${cs_data_list}${c_data}
      c_pooler_list=${c_pooler_list}${c_pooler}
      c_wal_list=${c_wal_list}${c_wal}
      c_arch_list=${c_arch_list}${c_arch}
      mod=`expr $c_cnt % 2`
      if [ $mod -eq 0 ];then
        cs_xclan=$c_xclan
      else
        cs_xclan_list=${cs_xclan_list}${c_xclan}" "${cs_xclan}
        if [ $c_cnt -ne $ce ]; then
          cs_xclan_list=${cs_xclan_list}" "
        fi
      fi
      if [ $c_cnt -ne $ce ]; then
        c_xclan_list=${c_xclan_list}" "
        c_name_list=${c_name_list}" "
        c_port_list=${c_port_list}" "
        c_data_list=${c_data_list}" "
        c_pooler_list=${c_pooler_list}" "
        cs_data_list=${cs_data_list}" "
        c_wal_list=${c_wal_list}" "
        c_arch_list=${c_arch_list}" "
      fi
      c_cnt=`expr $c_cnt + 1`
    done
    c_xclan_list=${c_xclan_list}")"
    c_name_list=${c_name_list}")"
    c_port_list=${c_port_list}")"
    c_data_list=${c_data_list}")"
    cs_data_list=${cs_data_list}")"
    c_pooler_list=${c_pooler_list}")"
    cs_xclan_list=${cs_xclan_list}")"
    c_wal_list=${c_wal_list}")"
    c_arch_list=${c_arch_list}")"
    ssh $xclan "echo \"$c_name_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_port_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_pooler_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_xclan_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_data_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_wal_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"coordSlave=y\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$cs_xclan_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$cs_data_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$c_arch_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"#---- Datanodes ----------------------------------------------------------------\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    d_xclan_list="datanodeMasterServers=("
    d_name_list="datanodeNames=("
    d_port_list="datanodePorts=("
    d_data_list="datanodeMasterDirs=("
    ds_xclan_list="datanodeSlaveServers=("
    ds_data_list="datanodeSlaveDirs=("
    d_wal_list="datanodeMaxWALSenders=("
    d_arch_list="datanodeArchLogDirs=("
    d_wal=5
    d_cnt=$ds
    while [ $d_cnt -le $de ]
    do
      d_xclan=`echo ${all[$d_cnt]} | awk -F'[,]' '{print $1}'`
      d_name=`echo ${all[$d_cnt]} | awk -F'[,]' '{print $3}'`
      d_port=`echo ${all[$d_cnt]} | awk -F'[,]' '{print $4}'`
      d_data=`echo ${all[$d_cnt]} | awk -F'[,]' '{print $5}'`
      d_arch=`echo ${all[$d_cnt]} | awk -F'[,]' '{print $6}'`
      d_xclan_list=${d_xclan_list}${d_xclan}
      d_name_list=${d_name_list}${d_name}
      d_port_list=${d_port_list}${d_port}
      d_data_list=${d_data_list}${d_data}
      ds_data_list=${ds_data_list}${d_data}
      d_wal_list=${d_wal_list}${d_wal}
      d_arch_list=${d_arch_list}${d_arch}
      mod=`expr $d_cnt % 2`
      if [ $mod -eq 0 ];then
        ds_xclan=$d_xclan
      else
        ds_xclan_list=${ds_xclan_list}${d_xclan}" "${ds_xclan}
        if [ $d_cnt -ne $de ]; then
          ds_xclan_list=${ds_xclan_list}" "
        fi
      fi
      if [ $d_cnt -ne $de ]; then
        d_xclan_list=${d_xclan_list}" "
        d_name_list=${d_name_list}" "
        d_port_list=${d_port_list}" "
        d_data_list=${d_data_list}" "
        ds_data_list=${ds_data_list}" "
        d_wal_list=${d_wal_list}" "
        d_arch_list=${d_arch_list}" "
      fi
      d_cnt=`expr $d_cnt + 1`
    done
    d_xclan_list=${d_xclan_list}")"
    d_name_list=${d_name_list}")"
    d_port_list=${d_port_list}")"
    d_data_list=${d_data_list}")"
    ds_data_list=${ds_data_list}")"
    ds_xclan_list=${ds_xclan_list}")"
    d_wal_list=${d_wal_list}")"
    d_arch_list=${d_arch_list}")"
    ssh $xclan "echo \"$d_name_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$d_port_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$d_xclan_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$d_data_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$d_wal_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"datanodeSlave=y\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$ds_xclan_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$ds_data_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"$d_arch_list\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $xclan "echo \"#=============<< End of future extension demonistration >> =====================\" >> $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
### copy GTM(Slave) & change gtmMasterServer
    cp_xclan=`echo ${all[$ge]} | awk -F'[,]' '{print $1}'`
    echo "*****  $cp_xclan  *****"
    ssh $cp_xclan "mkdir -p ~/pgxc_ctl" || { echo "### ERROR : MKDIR PGXC_CTL"; exit 1; }
    scp $xclan:$file $cp_xclan:$file > /dev/null || { echo "### ERROR : SCP PGXC_CTL CONF"; exit 1; }
    ssh $cp_xclan "sed s/gtmMasterServer=$master_xclan/gtmMasterServer=$slave_xclan/ $file > $file.slave" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
    ssh $cp_xclan "mv $file.slave $file" || { echo "### ERROR : PGXC_CTL CONF"; exit 1; }
### copy GTM_proxy
    cnt=$ps
    while [ $cnt -le $pe ]
    do
      cp_xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
      echo "*****  $cp_xclan  *****"
      ssh $cp_xclan "mkdir -p ~/pgxc_ctl" || { echo "### ERROR : MKDIR PGXC_CTL"; exit 1; }
      scp $xclan:$file $cp_xclan:$file > /dev/null || { echo "### ERROR : SCP PGXC_CTL CONF"; exit 1; }
      cnt=`expr $cnt + 1`
    done
  fi
fi

############
### Sort ###
############
if [ $sort_code -eq 1 ];then
  IFS=$'\n'
  start=(1 $ps $cs $ds)
  end=($ge $pe $ce $de)
### Sorting on a per-component basis ###
  for i in 0 1 2 3
  do
    cnt=${start[$i]}
    new_cnt=0
    while [ $cnt -le ${end[$i]} ]
    do
      new[$new_cnt]=${all[$cnt]}
      cnt=`expr $cnt + 1`
      new_cnt=`expr $new_cnt + 1`
    done
    new_sort=(`echo "${new[*]}" | sort`)
### Results of the sort to overwrite the original array ###
    cnt=${start[$i]}
    new_cnt=0
    while [ $cnt -le ${end[$i]} ]
    do
      all[$cnt]=${new_sort[$new_cnt]}
      cnt=`expr $cnt + 1`
      new_cnt=`expr $new_cnt + 1`
    done
  done
fi

##############
### Status ###
##############
if [ $install_code -eq 1 -o $install_code -eq 8 ];then
  checkfile[2]=$incfile
  cnt=0
  while [ $cnt -lt $allcnt ]
  do
    if [ $cnt -eq $gs ];then
      echo "****************************"
      echo "*** GTM Process ************"
      echo "****************************"
      checkfile[0]=$gtmpid
      checkfile[1]=$gtmfile
    elif [ $cnt -eq $ps ];then
      echo "****************************"
      echo "*** GTM_proxy Process ******"
      echo "****************************"
      checkfile[0]=$proxypid
      checkfile[1]=$proxyfile
    elif [ $cnt -eq $cs ];then
      echo "****************************"
      echo "*** Coordinator Process ****"
      echo "****************************"
      checkfile[0]=$masterpid
      checkfile[1]=$pgfile
    elif [ $cnt -eq $ds ];then
      echo "****************************"
      echo "*** Datanode Process *******"
      echo "****************************"
    fi
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    data=${newdata[$cnt]}
    echo "*****  $xclan  *****"
    for i in 0 1 2
    do
      ssh $xclan "test -f $data/${checkfile[$i]}"
      if [ $? -eq 0 ];then
        if [ $i -eq 0 ];then
          pid=`ssh $xclan "head -n 1 $data/${checkfile[$i]}"`
          ssh $xclan "ps -f -p $pid --no-headers"
        else
          echo "[${checkfile[$i]}]"
          ssh $xclan "cat $data/${checkfile[$i]}" | sed "/^#/d" | sed "/^\t/d" | sed "/^ /d" | sed "/^ *$/d"
        fi
      fi
    done
    cnt=`expr $cnt + 1`
  done

  echo "****************************"
  echo "*** Select pgxc_node *******"
  echo "****************************"
  cnt=$cs
  while [ $cnt -le $ce ]
  do
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    echo "*****  $xclan  *****"
    ssh $xclan "test -f $data/$masterpid"
    if [ $? -eq 0 ];then
      ssh $xclan "psql -p $port postgres -c \"SELECT * FROM pgxc_node;\""
    else
      echo "Coordinator is not started"
    fi
    cnt=`expr $cnt + 1`
  done
fi

##############
### Noexec ###
##############
if [ $install_code -eq 3 ];then
### GTM , GTM_proxy ###
  echo "****************************"
  echo "*** GTM Infomation *********"
  echo "****************************"
  cnt=$gs
  while [ $cnt -le $pe ]
  do
    if [ $cnt -eq $ps ];then
      echo "****************************"
      echo "*** GTM_proxy Infomation ***"
      echo "****************************"
    fi
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    conf=`echo ${all[$cnt]} | awk -F'[,]' '{print $7}'`
    echo "*****  xclan = $xclan  *****"
    echo "[slan] = $slan"
    echo "[name] = $name"
    echo "[port] = $port"
    echo "[data] = $data"
    if [ $cnt -eq $gs ];then
      echo "[startup] = ACT"
      echo "[gxid] = $gxid"
    elif [ $cnt -le $ge ];then
      echo "[startup] = STANDBY"
    fi
    if [ $conf ];then
      echo "[conf] = $conf"
      while read i
      do
        echo "$i"
      done < $conf
    fi
    cnt=`expr $cnt + 1`
  done

### Coordinator , Datanode ###
  echo "****************************"
  echo "*** Coordinator Infomation *"
  echo "****************************"
  cnt=$cs
  while [ $cnt -le $de ]
  do
    if [ $cnt -eq $ds ];then
      echo "****************************"
      echo "*** Datanode Infomation ****"
      echo "****************************"
    fi
    xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $1}'`
    slan=`echo ${all[$cnt]} | awk -F'[,]' '{print $2}'`
    name=`echo ${all[$cnt]} | awk -F'[,]' '{print $3}'`
    port=`echo ${all[$cnt]} | awk -F'[,]' '{print $4}'`
    data=${newdata[$cnt]}
    arch=${newarch[$cnt]}
    xlog=${newxlog[$cnt]}
    vipxc=`echo ${all[$cnt]} | awk -F'[,]' '{print $8}'`
    node=`echo ${all[$cnt]} | awk -F'[,]' '{print $9}'`
    n_xclan=`echo ${all[$cnt]} | awk -F'[,]' '{print $10}'`
    n_port=`echo ${all[$cnt]} | awk -F'[,]' '{print $11}'`
    hba=`echo ${all[$cnt]} | awk -F'[,]' '{print $12}'`
    conf=`echo ${all[$cnt]} | awk -F'[,]' '{print $13}'`
    no_14=`echo ${all[$cnt]} | awk -F'[,]' '{print $14}'`
    no_15=`echo ${all[$cnt]} | awk -F'[,]' '{print $15}'`
    echo "*****  xclan = $xclan  *****"
    echo "[slan] = $slan"
    if [ $vipxc ];then
      echo "[vipxc] = $vipxc"
    fi
    echo "[name] = $name"
    echo "[port] = $port"
    if [ $cnt -lt $ds ];then
      if [ $no_14 ];then
        echo "[pooler] = $no_14"
      fi
    fi
    echo "[data] = $data"
    echo "[arch] = $arch"
    echo "[xlog] = $xlog"
    echo "[node] = $node (xclan=$n_xclan, port=$n_port)"
    echo "[hba]  = $hba"
    while read i
    do
      echo "$i"
    done < $hba
    if [ $conf ];then
      echo "[conf] = $conf"
      while read i
      do
        echo "$i"
      done < $conf
    fi
    if [ $cnt -lt $ds ];then
      if [ $no_15 ];then
        echo "[preferred] = $no_15"
      fi
    else
      if [ $no_14 = "TRUE" ];then
        echo "[primary] = $no_14"
      fi
    fi
    cnt=`expr $cnt + 1`
  done
fi

echo "****************************"
echo "*** Install Finish *********"
echo "****************************"
exit 0
