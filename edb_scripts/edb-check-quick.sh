#!/bin/bash - 
#===============================================================================
#
#          FILE: edb-check-quick.sh
# 
#         USAGE: ./edb-check-quick.sh 
# 
#   DESCRIPTION: 1. copy EDBAS to /tmp/regression_tests
#		 2. run ./configure
#                3. make edb-check-quick 
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 11/05/2015 22:46
#===============================================================================

#===============================================================================
#   FUNCTIONS 
#===============================================================================

#-------------------------------------------------------------------------------
#     Print output description 
#-------------------------------------------------------------------------------
function label ()
{
	COLUMNS=$(tput cols);
	LINE=$(printf "%0.s-" $(seq 1 $COLUMNS));
	echo $LINE;
	printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "$1";
	echo $LINE;
}	# ----------  end of function label  ----------


#-------------------------------------------------------------------------------
# Copying code ....
#-------------------------------------------------------------------------------
label "Copy EDBAS to /tmp/regression_tests"
echo ""
rm -rf /tmp/regression_tests
rsync -vr --exclude=.git /home/amul/work/source/EDBAS/ /tmp/regression_tests
pushd .
cd /tmp/regression_tests/
#-------------------------------------------------------------------------------
# run ./configure ....
#-------------------------------------------------------------------------------
label "configure EDBAS src"
make maintainer-clean
echo ""
./configure --with-openssl --with-zlib --with-tcl --with-perl --with-libxslt --enable-coverage --with-ossp-uuid --with-ldap --with-pam --with-libcurl  --with-pgport=65444 --enable-nls --enable-cassert --enable-debug --enable-depend --prefix=/tmp/edbas_master

#-------------------------------------------------------------------------------
# Run regression tests
#-------------------------------------------------------------------------------
label "Running quick edb regression tests"
make
make check && make edb-check-quick
popd
