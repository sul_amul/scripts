#!/bin/sh 
#===============================================================================
#
#          FILE: edb-check-quick.sh
# 
#         USAGE: ./contrib-make-check.sh  <contrib module name>
# 
#   DESCRIPTION: 1. copy EDBAS to /tmp/regression_tests
#		 		 2. cd contrib/<contrib_name>/
#                3. make edb-check-quick 
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 11/05/2015 22:46
#===============================================================================

#===============================================================================
#   FUNCTIONS 
#===============================================================================
# this path need to chagned
. ~/work/source/scripts/include.sh
#-----------------------------------------------------------------------
#  Check number of command line arguments
#-----------------------------------------------------------------------
[ $# -lt 1 ] && { echo -e "\n\tUsage:  ${0##/*/} <contrib name>\n"; exit 1; }

#-------------------------------------------------------------------------------
# Run regression tests
#-------------------------------------------------------------------------------
label "Running quick regression tests for \"$1\"contrib"
change_makefile
pushd .
cd "contrib/$1/"
make clean
make check
popd
undo_changes_makefile
