#!/bin/sh

#---------------------------------------------------
# Author: Amul Sul
#
# This script can be used only once, 
# want to fresh installed DBT5.
# It will install DBT5 and create
# small test database and extract DML and DDL
#---------------------------------------------------

killall -9 postgres

. $PWD/config.sh

rm -rf $DBT5PGDATA
rm -rf $DBT5TSDIR
rm -rf $DBT5INSTDIR
mkdir $DBT5INSTDIR

echo ""
echo "+----------------------------+"
echo "|     Installing DBT-5       |"
echo "+----------------------------+"
echo ""
sleep 5

cd $CURR_PATH
make clean
cmake CMakeLists.txt -DDBMS=pgsql 
cd $CURR_PATH/egen/prj/ && make -f Makefile.pgsql
cd $CURR_PATH/storedproc/pgsql/c/ && make && make install
cd $CURR_PATH/ && make && make install DESTDIR=$DBT5INSTDIR

export PATH=$DBT5INSTDIR/bin:$PATH

echo ""
echo "+----------------------------+"
echo "|   Building Small Databse   |"
echo "+----------------------------+"
echo ""
sleep 5

export EGENDIR=$EGENDIR
export DBT5DBNAME=$DBT5DBNAME
export DBT5PGDATA=$DBT5PGDATA
export DBT5TSDIR=$DBT5TSDIR

dbt5-pgsql-build-db -c 1000 -s 500 -w 1

echo ""
echo "+----------------------------+"
echo "|    Extracting DML and DDL  |"
echo "+----------------------------+"
echo ""
sleep 5

pg_dump dbt5 -s -f $PG_DUMP_EXP_DIR/schema_c1000_s500.sql
pg_dump dbt5 -a -f $PG_DUMP_EXP_DIR/data_c1000_s500.sql


