#!/bin/sh
################################################################
# pgxc_ha_create.sh
#
# Copyright (c) 2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### DEFINE
check1="Online:"
check2="master-Gtm"
check3="1000"
check4="PRI"
check5="HS:alone"
check6="HS:sync"
check_c1="coordinator1-status"
check_c2="coordinator2-status"
check_d1="datanode1-status"
check_d2="datanode2-status"
timeout=30

### FILE CHECK ###
ha_define_file="./pgxc_ha_define.conf"
if [ ! -f $ha_define_file ];then
  echo "### ERROR : NOT FOUND HA DEFINE FILE (file=$ha_define_file)"
  exit 1
fi
source $ha_define_file

### GTM PACEMAKER START
echo "### GTM MASTER PACEMAKER START ###"
ssh -t ${gtm_slan[1]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (GTM:${gtm_slan[1]})"; exit 1; }
cnt=1
while [ $cnt -le $data_num ]
do
  ssh -t ${data_slan[$cnt]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (DATA:${data_slan[$cnt]})"; exit 1; }
  cnt=`expr $cnt + 1`
done
sleep 60

t_cnt=0
while true
do
  ssh -t ${gtm_slan[1]} "sudo crm_mon -A -1 | grep $check1" > /dev/null
  if [ $? -eq 0 ];then
    break
  fi
  if [ $t_cnt -ge $timeout ];then
    echo "### ERROR : TIME OUT"
    exit 1
  fi
  t_cnt=`expr $t_cnt + 1`
  sleep 10
done

### GTM CRM LOAD
echo "### GTM CRM LOAD ###"
ssh -t ${gtm_slan[1]} "sudo crm configure load update $gtm_crm > /dev/null" || { echo "### ERROR : CRM LOAD (GTM:${gtm_slan[1]})"; exit 1; }
sleep 10
t_cnt=0
while true
do
  ssh -t ${gtm_slan[1]} "sudo crm_mon -A -1 | grep $check2 | grep $check3" > /dev/null
  if [ $? -eq 0 ];then
    break
  fi
  if [ $t_cnt -ge $timeout ];then
    echo "### ERROR : TIME OUT"
    exit 1
  fi
  t_cnt=`expr $t_cnt + 1`
  sleep 10
done

## DATA CRM LOAD
echo "### DATA CRM LOAD ###"
ssh -t ${gtm_slan[2]} "sudo service heartbeat start > /dev/null" || { echo "### ERROR : PACEMAKER START (GTM:${gtm_slan[2]})"; exit 1; }
cnt=1
grp=1
while [ $cnt -le $data_num ]
do
  ssh -t ${data_slan[$cnt]} "sudo crm configure load update ${data_crm[$grp]} > /dev/null" || { echo "### ERROR : CRM LOAD (DATA:${data_slan[$cnt]})"; exit 1; }
  cnt=`expr $cnt + 2`
  grp=`expr $grp + 1`
done
sleep 60

## ARCHIVE SYNCHRONIZE
echo "### ARCHIVE SYNCHRONIZE ###"
cnt=1
while [ $cnt -le $data_num ]
do
  next=`expr $cnt + 1`
  t_cnt=0
  while true
  do
    ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check4" > /dev/null
    if [ $? -eq 0 ];then
      ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check5" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check5" > /dev/null
          if [ $? -eq 0 ];then
            ssh ${data_slan[$cnt]} "scp -q ${coord_arch[$cnt]}/* ${data_slan[$next]}:${coord_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Coordinator1:${data_slan[$cnt]})"; exit 1; }
            ssh ${data_slan[$cnt]} "scp -q ${dn_arch[$cnt]}/* ${data_slan[$next]}:${dn_arch[$cnt]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Datanode1:${data_slan[$cnt]})"; exit 1; }
            break
          fi
        fi
      fi
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done

  t_cnt=0
  while true
  do
    ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check4" > /dev/null
    if [ $? -eq 0 ];then
      ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check5" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check4" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check5" > /dev/null
          if [ $? -eq 0 ];then
            ssh ${data_slan[$next]} "scp -q ${coord_arch[$next]}/* ${data_slan[$cnt]}:${coord_arch[$next]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Coordinator2:${data_slan[$next]})"; exit 1; }
            ssh ${data_slan[$next]} "scp -q ${dn_arch[$next]}/* ${data_slan[$cnt]}:${dn_arch[$next]}/. > /dev/null" || { echo "### ERROR : ARCHIVE SYNCHRONIZE (Datanode2:${data_slan[$next]})"; exit 1; }
            break
          fi
        fi
      fi
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
  cnt=`expr $cnt + 2`
done
sleep 10

## START UP CHECK
echo "### START UP CHECK ###"
cnt=1
while [ $cnt -le $data_num ]
do
  t_cnt=0
  while true
  do
    ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c1 | grep $check6" > /dev/null
    if [ $? -eq 0 ];then
      ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_c2 | grep $check6" > /dev/null
      if [ $? -eq 0 ];then
        ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d1 | grep $check6" > /dev/null
        if [ $? -eq 0 ];then
          ssh -t ${data_slan[$cnt]} "sudo crm_mon -A -1 | grep $check_d2 | grep $check6" > /dev/null
          if [ $? -eq 0 ];then
            break
          fi
        fi
      fi
    fi
    if [ $t_cnt -ge $timeout ];then
      echo "### ERROR : TIME OUT"
      exit 1
    fi
    t_cnt=`expr $t_cnt + 1`
    sleep 10
  done
  cnt=`expr $cnt + 2`
done
echo "### FINISH ###"
