#!/bin/sh
################################################################
# pgxc_readfile.sh
#
# Copyright (c) 2012,2013 NIPPON TELEGRAPH AND TELEPHONE CORPORATION
# Version 2.0.0

### Comment,Blank Check ###
comment_check()
{
  if [ "$1" != "${1#'#'}" ];then
    return 1;
  elif [ -z "$1" ];then
    return 1;
  fi
  return 0;
}

##################################
### COMMON DATA
###   binary
###   ha
###   deploy[deploycnt] (1:xclan,2:slan,3:pghome)
### SETUP DATA
###   gxid
###   all[allcnt]                  (1:xclan,2:slan,3:name,4:port,5:data)
###     GTM         : all[gs - ge] (6:pghome,7:conf)
###     GTM_proxy   : all[ps - pe] (Same GTM)
###     Coordinator : all[cs - ce] (6:arch,7:xlog,8:vipxc,9:node,10:n_xclan,11:n_port,12:hba,13:conf,14:pooler,15:preferred)
###     Datanode    : all[ds - de] (6:arch,7:xlog,8:vipxc,9:node,10:n_xclan,11:n_port,12:hba,13:conf,14:primary)
deploycnt=0
allcnt=0
gs=0
cs=0
ce=0

### DECLARE ###
declare -i gxid=0
declare -i port=0
declare -i pooler=0

### FILE ###
file="./install.conf"
list[0]="./gtm.list"
list[1]="./gtm_proxy.list"
list[2]="./coordinator.list"
list[3]="./datanode.list"
list[4]="./all.list"

### COMPONENT ###
comp[0]="GTM:"
comp[1]="GTM_proxy:"
comp[2]="Coordinator1:"
comp[3]="Datanode1:"
comp[4]="Coordinator2:"
comp[5]="Datanode2:"
comp[6]="Common:"

### DEFAULT ###
def_pghome="/usr/local/pgsql"
def_ha="off"
def_gxid=100000
def_node="localhost"
def_hba="./pg_hba.conf"
def_primary="FALSE"
def_name[0]="gtm"
def_name[1]="proxy"
def_name[2]="coord"
def_name[3]="dn"
def_port[0]=6666
def_port[1]=6680
def_port[2]=5432
def_port[3]=15432
def_port[4]=5433
def_port[5]=15433
def_pooler[2]=6667
def_pooler[4]=6668
def_data[0]="/dbfp/data/gtm"
def_data[1]="/dbfp/data/gtm_proxy"
def_data[2]="/dbfp/data/coordinator1"
def_data[3]="/dbfp/data/datanode1"
def_data[4]="/dbfp/data/coordinator2"
def_data[5]="/dbfp/data/datanode2"
def_arch[2]="/dbfp/arch/coordinator1"
def_arch[3]="/dbfp/arch/datanode1"
def_arch[4]="/dbfp/arch/coordinator2"
def_arch[5]="/dbfp/arch/datanode2"
def_xlog[2]="/dbfp/xlog/coordinator1"
def_xlog[3]="/dbfp/xlog/datanode1"
def_xlog[4]="/dbfp/xlog/coordinator2"
def_xlog[5]="/dbfp/xlog/datanode2"

##################
### File check ###
##################
err=0
if [ ! -f $file ];then
  echo "### ERROR : NOT FOUND INSTALL FILE (file=$file)"
  err=1
fi
for i in 0 1 2 3 4
do
  if [ ! -f ${list[${i}]} ];then
    echo "### ERROR : NOT FOUND INSTALL FILE (file=${list[${i}]})"
    err=1
  fi
done
if [ $err -ne 0 ];then
  exit 1
fi

########################
### Reading set file ###
########################
code=-1
line=0
err=0

while read i
do
  line=`expr $line + 1`

  comment_check $i
  if [ $? -ne 0 ];then
    continue
  fi

### COMPONENT CHECK ###
  key=`echo $i | awk '{print $1}'`
  for j in 0 1 2 3 4 5 6
  do
    if [ "$key" = "${comp[$j]}" ];then
      code=$j
      continue 2
    fi
  done
  if [ $code -eq -1 ];then
    echo "### ERROR : NOT SPECIFIED COMPONENT (file=$file, line=$line) : $i"
    exit 1
  fi

### VALUE CHECK ###
  value=`echo $i | awk '{print $2}'`
  if [ -z "$value" ];then
    echo "### ERROR : NOT SPECIFIED VALUE (file=$file, line=$line) : $i"
    err=1
  fi

### DEPLOY SET ###
  if [ $code -eq 6 ];then
    case $key in
    binary:)
      binary=$value
      ;;
    pghome:)
      cmn_pghome=$value
      ;;
    ha:)
      ha=$value
      ;;
    *)
      echo "### ERROR : INVALID KEY (file=$file, line=$line) : $i"
      err=1
    esac
    continue
  fi

### KEY CHECK & VALUE SET ###
  case $key in
  name:)
    cmn_name[$code]=$value
    ;;
  port:)
    port=0
    port=$value
    if [ $port -le 0 ];then
      echo "### ERROR : NOT POSITIVE INTEGER (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_port[$code]=$port
    ;;
  pooler:)
    if [ $code -ne 2 -a $code -ne 4 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    else
      pooler=0
      pooler=$value
      if [ $pooler -le 0 ];then
        echo "### ERROR : NOT POSITIVE INTEGER (file=$file, line=$line) : $i"
        err=1
      fi
      cmn_pooler[$code]=$pooler
    fi
    ;;
  data:)
    cmn_data[$code]=$value
    ;;
  arch:)
    if [ $code -lt 2 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_arch[$code]=$value
    ;;
  xlog:)
    if [ $code -lt 2 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_xlog[$code]=$value
    ;;
  gxid:)
    if [ $code -ne 0 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    else
      gxid=0
      gxid=$value
      if [ $gxid -le 0 ];then
        echo "### ERROR : NOT POSITIVE INTEGER (file=$file, line=$line) : $i"
        err=1
      fi
    fi
    ;;
  node:)
    if [ $code -ne 2 -a $code -ne 3 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_node[$code]=$value
    ;;
  hba:)
    if [ $code -ne 2 -a $code -ne 3 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    elif [ ! -f $value ];then
      echo "### ERROR : NOT FOUND HBA FILE (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_hba[$code]=$value
    ;;
  conf:)
    if [ $code -ge 4 ];then
      echo "### ERROR : MISMATCH KEY (file=$file, line=$line) : $i"
      err=1
    elif [ ! -f $value ];then
      echo "### ERROR : NOT FOUND CONF FILE (file=$file, line=$line) : $i"
      err=1
    fi
    cmn_conf[$code]=$value
    ;;
  *)
    echo "### ERROR : INVALID KEY (file=$file, line=$line) : $i"
    err=1
  esac
done < $file

#########################
### Common data check ###
#########################
if [ -z "$binary" ];then
  echo "### ERROR : NOT SPECIFIED BINARY (file=$file)"
  err=1
elif [ ! -f $binary ];then
  echo "### ERROR : NOT FOUND BINARY (file=$file) : $binary"
  err=1
fi
if [ -z "$cmn_pghome" ];then
  cmn_pghome=$def_pghome
fi
if [ -z "$ha" ];then
  ha=$def_ha
elif [ $ha != "on" -a $ha != "off" ];then
  echo "### ERROR : INVALID VALUE (file=$file) : $ha"
  err=1
fi

if [ $err -ne 0 ];then
  exit 1
fi

####################
### GTM data set ###
####################
if [ $gxid -eq 0 ];then
  gxid=$def_gxid
fi

########################
### Reading all list ###
########################
err=0
line=0
while read i
do
  line=`expr $line + 1`
  comment_check $i
  if [ $? -ne 0 ];then
    continue
  fi

### XCLAN CHECK ###
  xclan=`echo $i | awk -F'[,]' '{print $1}'`
  if [ -z "$xclan" ];then
    echo "### ERROR : NOT SPECIFIED XCLAN (file=${list[4]} , line=$line) : $i"
    err=1
  fi

### SLAN CHECK ###
  slan=`echo $i | awk -F'[,]' '{print $2}'`
  if [ -z "$slan" ];then
    slan=$xclan
  fi

### PGHOME CHECK ###
  pghome=`echo $i | awk -F'[,]' '{print $3}'`
  if [ -z "$pghome" ];then
    pghome=$cmn_pghome
  fi

### SET DEPLOY DATA ###
  deploy[$deploycnt]="$xclan,$slan,$pghome"
  deploycnt=`expr $deploycnt + 1`
done < ${list[4]}
if [ $deploycnt -eq 0 ];then
  echo "### ERROR : LIST FILE EMPTY (file=${list[4]})"
  err=1
fi

#########################
### Reading list file ###
#########################
for code in 0 1 2 3
do

### SET DEFAULT ###
  d_name=${cmn_name[$code]}
  if [ -z "$d_name" ];then
    d_name=${def_name[$code]}
  fi
  d_port=${cmn_port[$code]}
  if [ -z "$d_port" ];then
    d_port=${def_port[$code]}
  fi
  if [ $code -eq 2 ];then
    d_pooler=${cmn_pooler[$code]}
    if [ -z "$d_pooler" ];then
      d_pooler=${def_pooler[$code]}
    fi
  fi
  d_data=${cmn_data[${code}]}
  if [ -z "$d_data" ];then
    d_data=${def_data[$code]}
  fi
  if [ $code -ge 2 ];then
    d_arch=${cmn_arch[${code}]}
    if [ -z "$d_arch" ];then
      d_arch=${def_arch[$code]}
    fi
    d_xlog=${cmn_xlog[${code}]}
    if [ -z "$d_xlog" ];then
      d_xlog=${def_xlog[$code]}
    fi
    d_node=${cmn_node[$code]}
    if [ -z "$d_node" ];then
      d_node=${def_node}
    fi
    d_hba=${cmn_hba[$code]}
    if [ -z "$d_hba" ];then
      d_hba=${def_hba}
    fi
  fi
  d_conf=${cmn_conf[$code]}

### LIST CHECK START ###
  cnt=0
  line=0
  primary_check=0
  while read i
  do
    line=`expr $line + 1`
    comment_check $i
    if [ $? -ne 0 ];then
      continue
    fi
    cnt=`expr $cnt + 1`

### XCLAN CHECK ###
    row=1
    xclan=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$xclan" ];then
      echo "### ERROR : NOT SPECIFIED XCLAN (file=${list[$code]} , line=$line) : $i"
      err=1
    fi

### SLAN CHECK ###
    row=`expr $row + 1`
    slan=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$slan" ];then
      slan=$xclan
    fi

### VIPXC CHECK ###
    if [ $code -ge 2 ];then
      row=`expr $row + 1`
      vipxc=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ $ha = "on" -a -z "$vipxc" ];then
        echo "### ERROR : NOT SPECIFIED VIPXC (file=${list[$code]} , line=$line) : $i"
        err=1
      fi
    fi

### NAME CHECK ###
    row=`expr $row + 1`
    name=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$name" ];then
      name=$d_name$cnt
      if [ $code -ge 2 -a $ha = "on" ];then
        mod=`expr $cnt % 2`
        if [ $mod -eq 0 ];then
          sub=`expr $code + 2`
          if [ -z "${cmn_name[$sub]}" ];then
            name=${def_name[$code]}$cnt
          else
            name=${cmn_name[$sub]}$cnt
          fi
        fi
      fi
    fi

### PORT CHECK ###
    row=`expr $row + 1`
    work=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$work" ];then
      port=$d_port
      if [ $code -ge 2 -a $ha = "on" ];then
        mod=`expr $cnt % 2`
        if [ $mod -eq 0 ];then
          sub=`expr $code + 2`
          if [ -z "${cmn_port[$sub]}" ];then
            port=${def_port[$sub]}
          else
            port=${cmn_port[$sub]}
          fi
        fi
      fi
    else
      port=0
      port=$work
      if [ $port -le 0 ];then
        echo "### ERROR : NOT POSITIVE INTEGER (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi

### POOLER CHECK ###
    if [ $code -eq 2 ];then
      row=`expr $row + 1`
      work=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$work" ];then
        pooler=$d_pooler
        if [ $ha = "on" ];then
          mod=`expr $cnt % 2`
          if [ $mod -eq 0 ];then
            if [ -z "${cmn_pooler[4]}" ];then
              pooler=${def_pooler[4]}
            else
              pooler=${cmn_pooler[4]}
            fi
          fi
        fi
      else
        pooler=0
        pooler=$work
        if [ $pooler -le 0 ];then
          echo "### ERROR : NOT POSITIVE INTEGER (file=${list[$code]}, line=$line) : $i"
          err=1
        fi
      fi
    fi

### DATA CHECK ###
    row=`expr $row + 1`
    data=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$data" ];then
      data=$d_data
      if [ $code -ge 2 -a $ha = "on" ];then
        mod=`expr $cnt % 2`
        if [ $mod -eq 0 ];then
          sub=`expr $code + 2`
          if [ -z "${cmn_data[$sub]}" ];then
            data=${def_data[$sub]}
          else
            data=${cmn_data[$sub]}
          fi
        fi
      fi
    fi

### ARCH CHECK ###
    if [ $code -ge 2 ];then
      row=`expr $row + 1`
      arch=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$arch" ];then
        arch=$d_arch
        if [ $ha = "on" ];then
          mod=`expr $cnt % 2`
          if [ $mod -eq 0 ];then
            sub=`expr $code + 2`
            if [ -z "${cmn_arch[$sub]}" ];then
              arch=${def_arch[$sub]}
            else
              arch=${cmn_arch[$sub]}
            fi
          fi
        fi
      fi

### XLOG CHECK ###
      row=`expr $row + 1`
      xlog=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$xlog" ];then
        xlog=$d_xlog
        if [ $ha = "on" ];then
          mod=`expr $cnt % 2`
          if [ $mod -eq 0 ];then
            sub=`expr $code + 2`
            if [ -z "${cmn_xlog[$sub]}" ];then
              xlog=${def_xlog[$sub]}
            else
              xlog=${cmn_xlog[$sub]}
            fi
          fi
        fi
      fi

### NODE CHECK ###
      row=`expr $row + 1`
      node=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$node" ];then
        node=$d_node
      fi
      if [ $ha = "on" ];then
        node=$def_node
      fi
### GTM_proxy PORT CHECK ###
      if [ "$node" = $def_node ];then
        n_xclan=$xclan
        p_cnt=$ps
        while [ $p_cnt -le $pe ]
        do
          p_xclan=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $1}'`
          if [ "$n_xclan" = "$p_xclan" ];then
            n_port=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $4}'`
            break
          fi
          p_cnt=`expr $p_cnt + 1`
        done
### GTM_proxy XCLAN & PORT CHECK ###
      else
        p_cnt=$ps
        while [ $p_cnt -le $pe ]
        do
          p_name=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $3}'`
          if [ "$node" = "$p_name" ];then
            n_xclan=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $1}'`
            n_port=`echo ${all[$p_cnt]} | awk -F'[,]' '{print $4}'`
            break
          fi
          p_cnt=`expr $p_cnt + 1`
        done
      fi
      if [ $p_cnt -gt $pe ];then
        echo "### ERROR : NOT FOUND NODE (file=${list[$code]}, line=$line) : $i"
        err=1
      fi

### HBA CHECK ###
      row=`expr $row + 1`
      hba=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$hba" ];then
        hba=$d_hba
      fi
      if [ ! -f $hba ];then
        echo "### ERROR : NOT FOUND HBA FILE (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi

### CONF CHECK ###
    row=`expr $row + 1`
    conf=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    if [ -z "$conf" ];then
      conf=$d_conf
    fi
    if [ $conf ];then
      if [ ! -f $conf ];then
        echo "### ERROR : NOT FOUND CONF FILE (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi

### PRIMARY CHECK ###
    if [ $code -eq 3 ];then
      row=`expr $row + 1`
      primary=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
      if [ -z "$primary" ];then
        primary="FALSE"
      elif [ "$primary" = "TRUE" ];then
        if [ $primary_check -ne 0 ];then
          echo "### ERROR : DUPLICATE PRIMARY (file=${list[$code]}, line=$line) : $i"
          err=1
        fi
        primary_check=1
      elif [ "$primary" != "FALSE" ];then
        echo "### ERROR : INVALID VALUE (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi

### PREFERRED CHECK ###
    if [ $code -eq 2 ];then
      row=`expr $row + 1`
      preferred=`echo $i | awk -v id=$row -F'[,]' '{print $id}'`
    fi

### SET ALL DATA ###
    if [ $code -lt 2 ];then
      check=0
      while [ $check -lt $deploycnt ]
      do
        xclan_check=`echo ${deploy[$check]} | awk -F'[,]' '{print $1}'`
        pghome_check=`echo ${deploy[$check]} | awk -F'[,]' '{print $3}'`
        if [ "$xclan" = "$xclan_check" ];then
          break;
        fi
        check=`expr $check + 1`
      done
      if [ $check -ge $deploycnt ];then
        echo "### ERROR : NOT FOUND XCLAN (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
      all[$allcnt]="$xclan,$slan,$name,$port,$data,$pghome_check,$conf"
    elif [ $code -eq 2 ];then
      all[$allcnt]="$xclan,$slan,$name,$port,$data,$arch,$xlog,$vipxc,$node,$n_xclan,$n_port,$hba,$conf,$pooler,$preferred"
    else
      all[$allcnt]="$xclan,$slan,$name,$port,$data,$arch,$xlog,$vipxc,$node,$n_xclan,$n_port,$hba,$conf,$primary"
    fi

### DUPLICATION CHECK ###
    if [ $code -eq 2 ];then
      if [ $port -eq $pooler ];then
        echo "### ERROR : DUPLICATE POOLER (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi
    if [ $code -ge 2 ];then
      if [ "$data" = "$arch" -o "$data" = "$xlog" -o "$arch" = "$xlog" ];then
        echo "### ERROR : DUPLICATE DIR (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
    fi
    check=0
    while [ $check -lt $allcnt ]
    do
      name_check=`echo ${all[$check]} | awk -F'[,]' '{print $3}'`
      if [ "$name" = "$name_check" ];then
        echo "### ERROR : DUPLICATE NAME (file=${list[$code]}, line=$line) : $i"
        err=1
      fi
      xclan_check=`echo ${all[$check]} | awk -F'[,]' '{print $1}'`
      if [ "$xclan" = "$xclan_check" ];then
### DIR CHECK
        data_check=`echo ${all[$check]} | awk -F'[,]' '{print $5}'`
        if [ "$data" = "$data_check" ];then
          echo "### ERROR : DUPLICATE DIR (file=${list[$code]}, line=$line) : $i"
          err=1
        fi
### MYSELF COORDINATOR OR DATANODE
        if [ $code -ge 2 ];then
          if [ "$arch" = "$data_check" -o "$xlog" = "$data_check" ];then
            echo "### ERROR : DUPLICATE DIR (file=${list[$code]}, line=$line) : $i"
            err=1
          fi
        fi
### CHECKED COORDINATOR OR DATANODE
        if [ $cs -ne 0 -a $check -ge $cs ];then
          arch_check=`echo ${all[$check]} | awk -F'[,]' '{print $6}'`
          xlog_check=`echo ${all[$check]} | awk -F'[,]' '{print $7}'`
          if [ "$data" = "$arch_check" -o "$data" = "$xlog_check" ];then
            echo "### ERROR : DUPLICATE DIR (file=${list[$code]}, line=$line) : $i"
            err=1
          fi
### BOTH COORDINATOR OR DATANODE
          if [ $code -ge 2 ];then
            if [ "$arch" = "$arch_check" -o "$arch" = "$xlog_check" -o "$xlog" = "$arch_check" -o "$xlog" = "$xlog_check" ];then
              echo "### ERROR : DUPLICATE DIR (file=${list[$code]}, line=$line) : $i"
            fi
          fi
        fi
### PORT CHECK
        port_check=`echo ${all[$check]} | awk -F'[,]' '{print $4}'`
        if [ $port -eq $port_check ];then
          echo "### ERROR : DUPLICATE PORT (file=${list[$code]}, line=$line) : $i"
          err=1
        fi
### MYSELF COORDINATOR
        if [ $code -eq 2 ];then
          if [ $pooler -eq $port_check ];then
            echo "### ERROR : DUPLICATE POOLER (file=${list[$code]}, line=$line) : $i"
            err=1
          fi
        fi
### CHECKED COORDINATOR
        if [ $cs -ne 0 -a $check -ge $cs ];then
          if [ $ce -eq 0 -o $check -le $ce ];then
            pooler_check=`echo ${all[$check]} | awk -F'[,]' '{print $14}'`
            if [ $port -eq $pooler_check ];then
              echo "### ERROR : DUPLICATE POOLER (file=${list[$code]}, line=$line) : $i"
              err=1
            fi
### BOTH COORDINATOR
            if [ $code -eq 2 ];then
              if [ $pooler -eq $pooler_check ];then
                echo "### ERROR : DUPLICATE POOLER (file=${list[$code]}, line=$line) : $i"
                err=1
              fi
            fi
          fi
        fi
      fi
      check=`expr $check + 1`
    done
    allcnt=`expr $allcnt + 1`
  done < ${list[$code]}
  if [ $cnt -eq 0 ];then
    echo "### ERROR : LIST FILE EMPTY (file=${list[$code]})"
    err=1
  fi

### SET RANGE ###
  if [ $code -eq 0 ];then
    ge=`expr $allcnt - 1`
    ps=$allcnt
  elif [ $code -eq 1 ];then
    pe=`expr $allcnt - 1`
    cs=$allcnt
  elif [ $code -eq 2 ];then
    ce=`expr $allcnt - 1`
    ds=$allcnt
  else
    de=`expr $allcnt - 1`
  fi
done

### GTM LIST CHECK ###
g_cnt=`expr $ge - $gs + 1`
if [ $g_cnt -gt 2 ];then
  echo "### ERROR : GTM LIST COUNT (count=$g_cnt)"
  err=1
fi

### PREFERRED CHECK ###
d_cnt=`expr $de - $ds + 1`
i=$cs
while [ $i -le $ce ]
do
  preferred=`echo ${all[$i]} | awk -F'[,]' '{print $15}'`
  if [ "$preferred" ];then
    cnt=1
    while true
    do
      work_name=`echo $preferred | awk -v id=$cnt -F'[/]' '{print $id}'`
      if [ -z "$work_name" ];then
        break
      fi
      cnt=`expr $cnt + 1`
    done
    cnt=`expr $cnt - 1`
    if [ $cnt -gt $d_cnt ];then
      echo "### ERROR : PREFERRED COUNT (count=$cnt)"
      err=1
    fi
  fi
  i=`expr $i + 1`
done

### HA CHECK ###
if [ $ha = "on" ];then
### GTM SERVER CHECK ###
  g_cnt=`expr $ge - $gs + 1`
  if [ $g_cnt -ne 2 ];then
    echo "### ERROR : (HA) GTM LIST ONLY 2"
    err=1
  else
    port_1=`echo ${all[$gs]} | awk -F'[,]' '{print $4}'`
    port_2=`echo ${all[$ge]} | awk -F'[,]' '{print $4}'`
    if [ $port_1 -ne $port_2 ];then
      echo "### ERROR : (HA) GTM PORT NOT MATCH"
      err=1
    fi
    data_1=`echo ${all[$gs]} | awk -F'[,]' '{print $5}'`
    data_2=`echo ${all[$ge]} | awk -F'[,]' '{print $5}'`
    if [ "$data_1" != "$data_2" ];then
      echo "### ERROR : (HA) GTM DATA NOT MATCH"
      err=1
    fi
  fi
### DATA SERVER CHECK ###
  p_cnt=`expr $pe - $ps + 1`
  c_cnt=`expr $ce - $cs + 1`
  d_cnt=`expr $de - $ds + 1`
  if [ $p_cnt -ne $c_cnt -o $p_cnt -ne $d_cnt ];then
    echo "### ERROR : (HA) DATA COUNTS NOT MATCH"
    err=1
  else
    even=`expr $p_cnt % 2`
    if [ $even -ne 0 ];then
      echo "### ERROR : (HA) DATA COUNTS NOT EVEN"
      err=1
    fi
  fi
### SLAVE CHECK ###
  i=$cs
  while [ $i -le $de ]
  do
    port=`echo ${all[$i]} | awk -F'[,]' '{print $4}'`
    data=`echo ${all[$i]} | awk -F'[,]' '{print $5}'`
    arch=`echo ${all[$i]} | awk -F'[,]' '{print $6}'`
    xlog=`echo ${all[$i]} | awk -F'[,]' '{print $7}'`
    if [ $i -ge $cs -a $i -le $ce ];then
      pooler=`echo ${all[$i]} | awk -F'[,]' '{print $14}'`
    fi
    pair=`expr $i - $cs`
    pair=`expr $pair % 2`
    if [ $pair -eq 0 ];then
      pair=`expr $i + 1`
    else
      pair=`expr $i - 1`
    fi
    xclan_pair=`echo ${all[$pair]} | awk -F'[,]' '{print $1}'`
    check=0
    while [ $check -lt $allcnt ]
    do
      xclan_check=`echo ${all[$check]} | awk -F'[,]' '{print $1}'`
      if [ "$xclan_pair" = "$xclan_check" ];then
### PORT CHECK
        port_check=`echo ${all[$check]} | awk -F'[,]' '{print $4}'`
        if [ $port -eq $port_check ];then
          echo "### ERROR : (HA) DUPLICATE PORT (port=$port_check)"
          err=1
        fi
        if [ $i -ge $cs -a $i -le $ce ];then
          if [ $pooler -eq $port_check ];then
            echo "### ERROR : (HA) DUPLICATE PORT (pooler=$port_check)"
            err=1
          fi
          if [ $check -ge $cs -a $check -le $ce ];then
            pooler_check=`echo ${all[$check]} | awk -F'[,]' '{print $14}'`
            if [ $pooler -eq $pooler_check ];then
              echo "### ERROR : (HA) DUPLICATE PORT (pooler=$pooler_check)"
              err=1
            fi
          fi
        fi
### DIR CHECK
        data_check=`echo ${all[$check]} | awk -F'[,]' '{print $5}'`
        if [ "$data" = "$data_check" -o "$arch" = "$data_check" -o "$xlog" = "$data_check" ];then
          echo "### ERROR : (HA) DUPLICATE DIR (data=$data_check)"
          err=1
        fi
        if [ $check -ge $cs -a $check -le $de ];then
          arch_check=`echo ${all[$check]} | awk -F'[,]' '{print $6}'`
          if [ "$data" = "$arch_check" -o "$arch" = "$arch_check" -o "$xlog" = "$arch_check" ];then
            echo "### ERROR : (HA) DUPLICATE DIR (arch=$arch_check)"
            err=1
          fi
          xlog_check=`echo ${all[$check]} | awk -F'[,]' '{print $7}'`
          if [ "$data" = "$xlog_check" -o "$arch" = "$xlog_check" -o "$xlog" = "$xlog_check" ];then
            echo "### ERROR : (HA) DUPLICATE DIR (xlog=$xlog_check)"
            err=1
          fi
        fi
      fi
      check=`expr $check + 1`
    done
    i=`expr $i + 1`
  done
fi

### ERROR FINISH ###
if [ $err -ne 0 ];then
  exit 1
fi
