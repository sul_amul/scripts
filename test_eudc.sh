#!/bin/sh
#this file is added for eudc  test
# function to print label
function header(){
	echo "------------------------"
	echo $1
	echo "------------------------"
	sleep 5
}

function initdb_and_start(){
	rm -rf $DATA
	initdb -D $DATA
	pg_ctl -D $DATA start -w -l $LOG
	sleep 5
}

function install_and_check(){
	make clean
	make
	make install
	sleep 5
	make installcheck
}

function sql_check(){
	psql  postgres -f eudc.sql
	psql -e postgres -c 'select * from show_eudc()'
	psql  postgres -f uninstall_eudc.sql
}

function extension_check(){
	#--------------------------------
	 header " Extension installation"
	#--------------------------------
	psql -e postgres -c 'CREATE EXTENSION eudc'
	psql -e postgres -c 'select * from show_eudc()'
	psql -e postgres -c 'CREATE EXTENSION eudc_drop'
}

function run_test(){

	#--------------------------------
	 header " Initiat DB & Start it"
	#--------------------------------
	initdb_and_start

	#--------------------------------
	 header " Installcheck   "
	#--------------------------------
	install_and_check

	#--------------------------------
	 header " script installation"
	#--------------------------------
	sql_check
}

INSTALL="/home/amul/pg_install/install"
DATA="/tmp/data"
LOG="/tmp/work.log"


#--------------------------------
header "PG8.4"
#--------------------------------
killall -15 postgres
export PATH=$O_PATH
export PATH=$INSTALL/pg8.4D/bin:$PATH
echo "Postgresql is \"`which postgres`\""

#--------------------------------
# Run Test For PG8.4
#--------------------------------
run_test


#--------------------------------
# Run Test For PG9.0-PG9.3
#--------------------------------

i=0

while [ $i -lt 4 ];
do

	#--------------------------------
	 header "PG9.$i"
	#--------------------------------
	killall -15 postgres
	export PATH=$O_PATH
	export PATH=$INSTALL/pg9."$i"D/bin:$PATH
	echo "Postgresql is \"`which postgres`\""

	if [ "$i" -gt 0 ]
	then
		run_test
		extension_check
	else
		run_test
	fi

	((i++))
done
#--------------------------------
 header "FINISHED"
#--------------------------------
