#!/bin/sh 
#===============================================================================
#
#          FILE: edb-check-quick.sh
# 
#         USAGE: ./edb-check-quick.sh 
# 
#   DESCRIPTION: 1. copy EDBAS to /tmp/regression_tests
#		 2. run ./configure
#                3. make edb-check-quick 
# 
#        AUTHOR: Amul Sul (sulamul@yahoo.com) 
#       CREATED: 11/05/2015 22:46
#===============================================================================

# this path need to chagned
. ~/work/source/scripts/include.sh

#-------------------------------------------------------------------------------
# Run regression recheck tests
#-------------------------------------------------------------------------------
label "Running quick edb regression tests"
make edb-recheck
popd
