#!/usr/bin/perl

use strict;
use warnings;

for my $file (@ARGV)
{
	pad_one_file($file);
}

sub pad_one_file {
	my ($file) = @_;
	open(FILE, ">>$file") || die "can't open $file: $!";
	my $bytes = -s $file;
	printf "%s: current file size is %d bytes\n", $file, $bytes;
	my $missingbytes = 262144 - $bytes;
	if ($missingbytes <= 0)
	{
		printf "%s: nothing to do\n", $file;
		return;
	}
	my $padbytes = "\x55" x $missingbytes;
	printf "%s: appending %d bytes\n", $file, length($padbytes);
	print FILE $padbytes || die "can't write to $file: $!";
	close(FILE);
}
