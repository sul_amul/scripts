#!/bin/sh
#===============================================================================
#
#          FILE: coverage.sh
#
#         USAGE: ./coverage.sh
#
#   DESCRIPTION:
#
#       CREATED: 10/24/2017 15:21
#
# STEPS:
#
# 1. Commit your patch locally
#
# 2. Configure sources with --enable-coverage switch
#
# 3. Initialize lcov counters etc. using:
# lcov --directory $PWD --capture --output-file mycoverage.info
#
# 4. Run tests
#
# 5. Run the script by passing your patch name. Patch is used to get all the .c
# files touched by the patch for which gcov is run and analyzed per Thomas's
# script.
#
#===============================================================================
#set -x

if [ $# -ne 1 ]; then
  echo "must pass patch name"
  exit 1
fi

patch=$1
src=$PWD

for fn in `grep "diff --git" $patch | cut -d' ' -f 3 | cut -d'/' -f 2- | grep \.c$` ; do

  f=$(basename $fn) ; echo $f ; d=$(dirname $fn) ; cd $d ; gcov $f > cov.log ; cd $src

  echo $fn
  ( grep -E '^ *-: *0:' $fn.gcov | sed 's/^.*$/  /' ; \
    git blame $fn | sed 's/^[\^a-z0-9]* (Amul Sul .*$/! /;s/^[\^a-z0-9]* .*/  /' ) | \
  paste -d '\0' - $fn.gcov | grep -E '^! *#'

done
#if [ $# -ne 1 ]; then
#  echo "ERROR : patches?"
#  exit 1
#fi
#
##patch=$1
##src=$PWD
#src="/tmp/regression_tests"
#
#for fn in `grep "diff --git" $patch | cut -d' ' -f 3 | cut -d'/' -f 2- | grep \.c$` ; do
##for fn in `git log -$1 --stat --format="" --name-only` ; do
#
#  f=$(basename $fn) ; echo $f ; d=$(dirname $fn) ; cd $d ; gcov $f > cov.log ; cd $src
#
#  echo $fn
#  ( grep -E '^ *-: *0:' $fn.gcov | sed 's/^.*$/  /' ; \
#    git blame $fn | sed 's/^[\^a-z0-9]* (Amul Sul .*$/! /;s/^[\^a-z0-9]* .*/  /' ) | \
#  paste -d '\0' - $fn.gcov | grep -E '^! *#'
#
#done
