#!/usr/bin/perl

# perl split function example 1
# purpose:   read the /etc/passwd file, whose columns are separated by ':'
# usage:     perl read-passwd-file.pl

# sample /etc/passwd record:
# nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false

$filename = '/tmp/whatsapp_contacts.text';

open(FILE, $filename) or die "Could not read from $filename, program halting.";
while(<FILE>)
{
  # get rid of the pesky newline character
  chomp;

  # read the fields in the current record into an array
  @fields = split(':', $_);

  # print the first field (the username)
  print "BEGIN:VCARD\nVERSION:3.0\nFN:$fields[0]\nN:$fields[0];;;;\n";
  print "TEL;TYPE=CELL:$fields[1]\nEND:VCARD\n";
}
close FILE;
